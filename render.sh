#!/bin/bash

set -e

REMHOST=root@xon.teichisma.info
LUFFADIR="$HOME/src/luffa"
DEMOSDIR="/home/xonotic/demos/demosnew"
USERDIR="/storage/nick/xdwc-video/userdir"
XONODIR=/storage/nick/Xonotic
OUTPUTDIR=/storage/nick/defrag-autovids

ssh "$REMHOST" -C /home/xonotic/demos/process_demosnew.sh
cd "$(dirname "$DEMOSDIR")"
rsync -a -P "$REMHOST:$DEMOSDIR" .

rm "$USERDIR"/data/dlcache/*.pk3
ln -f /storage/nick/xinfra/xonotic-data/data/base.collection/*.pk3 "$USERDIR/data/dlcache"
ln -f /storage/nick/xinfra/xonotic-data/data/hp-maps.collection/*.pk3 "$USERDIR/data/dlcache"
ln -f /storage/nick/xinfra/xonotic-data/data/rr-maps.collection/*.pk3 "$USERDIR/data/dlcache"

cd "$LUFFADIR"
cargo run --release --bin render_and_upload_video -- -u "$USERDIR" -x "$XONODIR" -o ./video-data/outro.mp4 -t "$OUTPUTDIR"
