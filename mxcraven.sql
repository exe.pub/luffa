-- firsts which stayed unbeaten
select map.name,
       player.nickname_nocolors,
       time_record.pos,
       time_record.time,
       time_record.created_at
from time_record
         inner join map on time_record.map_id = map.id
         inner join player on time_record.player_id = player.id
where time_record.created_at > ${start_date}
  and time_record.created_at < ${end_date}
  and time_record.pos = 1
order by time_record.created_at;

-- all firsts set during the period
select map.name,
       player.nickname_nocolors,
       new_pos,
       new_time,
       results_changeset.created_at
from results_change
         inner join results_changeset on results_change.results_changeset_id = results_changeset.id
         inner join map on results_changeset.map_id = map.id
         inner join player on results_change.player_id = player.id
where new_pos = 1
  and results_changeset.created_at < ${end_date}
  and results_changeset.created_at > ${start_date};

-- number of unbeaten firsts per player
select nickname_nocolors,
       count(*)
from (select map.name,
             player.nickname_nocolors,
             new_pos,
             new_time,
             results_changeset.created_at
      from results_change
               inner join results_changeset on results_change.results_changeset_id = results_changeset.id
               inner join map on results_changeset.map_id = map.id
               inner join player on results_change.player_id = player.id
      where new_pos = 1
        and results_changeset.created_at < ${end_date}
        and results_changeset.created_at > ${start_date}) as subq
group by nickname_nocolors;
