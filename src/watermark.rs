use anyhow::Error;
use dpcolors::DPString;
use image::{DynamicImage, Rgba};
use rust_decimal::Decimal;
use rusttype::{point, Font, Scale};
use unicode_normalization::UnicodeNormalization;

pub fn format_time(time: Decimal) -> String {
    let minutes;
    let seconds;
    let sixty = Decimal::new(60, 0);
    if time > sixty {
        let m = (time / sixty).floor();
        seconds = time - m * sixty;
        minutes = Some(m)
    } else {
        minutes = None;
        seconds = time
    }
    if let Some(m) = minutes {
        format!("{m:.0}:{seconds:05.2}s")
    } else {
        format!("{seconds:05.2}s")
    }
}

pub struct Watermark<'a> {
    pub map: &'a str,
    pub nickname: DPString,
    pub time: Decimal,
    pub rating: Decimal,
}

pub const WATERMARK_WIDTH: u32 = 960;
pub const WATERMARK_HEIGHT: u32 = 135;
pub const WATERMARK_OFFSET_LEFT: u32 = 0;
pub const WATERMARK_OFFSET_TOP: u32 = 1080 - WATERMARK_HEIGHT;

impl<'a> Watermark<'a> {
    pub fn gen_image(&self) -> Result<DynamicImage, Error> {
        let font_data = include_bytes!("../fonts/Xolonium-Regular.ttf");
        let font = Font::try_from_bytes(font_data as &[u8]).expect("Error constructing Font");
        let scale1 = Scale::uniform(48.0);
        // let scale2 = Scale::uniform(36.0);
        let mut res = DynamicImage::new_rgba8(WATERMARK_WIDTH, WATERMARK_HEIGHT);
        let img = res.as_mut_rgba8().unwrap();
        let base_pixel = Rgba([0, 0, 0, 0]);
        for i in 0..WATERMARK_WIDTH {
            for j in 0..WATERMARK_HEIGHT {
                img.put_pixel(i, j, base_pixel)
            }
        }
        let color = (234, 234, 234);
        let v_metrics1 = font.v_metrics(scale1);
        // let v_metrics2 = font.v_metrics(scale2);
        let map_name: String = self.map.chars().take(15).collect();
        let map_glyphs: Vec<_> = font
            .layout(&map_name, scale1, point(15.0, 15.0 + v_metrics1.ascent))
            .collect();
        let player_glyphs: Vec<_> = font
            .layout(
                &self.nickname.to_none().nfc().collect::<String>(),
                scale1,
                point(15.0, 15.0 + v_metrics1.ascent),
            )
            .collect();
        let time_glyphs: Vec<_> = font
            .layout(
                &format_time(self.time),
                scale1,
                point(15.0, 15.0 + v_metrics1.ascent),
            )
            .collect();

        for glyph in map_glyphs {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                glyph.draw(|x, y, v| {
                    img.put_pixel(
                        x + bounding_box.min.x as u32,
                        y + bounding_box.min.y as u32,
                        Rgba([color.0, color.1, color.2, (v * 255.0) as u8]),
                    )
                });
            }
        }

        for glyph in player_glyphs {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                glyph.draw(|x, y, v| {
                    img.put_pixel(
                        x + bounding_box.min.x as u32,
                        y + 63 + bounding_box.min.y as u32,
                        Rgba([color.0, color.1, color.2, (v * 255.0) as u8]),
                    )
                });
            }
        }

        for glyph in time_glyphs {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                glyph.draw(|x, y, v| {
                    img.put_pixel(
                        x + 533 + bounding_box.min.x as u32,
                        y + 30 + bounding_box.min.y as u32,
                        Rgba([color.0, color.1, color.2, (v * 255.0) as u8]),
                    )
                });
            }
        }

        Ok(res)
    }
}
