use chrono::prelude::*;

pub fn f64_to_decimal(f: f64, prec: u32) -> rust_decimal::Decimal {
    let mul = 10_u32.pow(prec);
    let mut dec = rust_decimal::Decimal::from((f * (f64::from(mul))) as i64);
    dec.set_scale(prec).unwrap();
    dec
}

pub fn utcnow() -> NaiveDateTime {
    Utc::now().naive_utc()
}
