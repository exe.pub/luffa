pub mod admin;
pub mod controllers;
pub mod error;
pub mod models;
pub mod schema;
mod templates;
pub mod util;
pub mod watermark;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_sync_db_pools;

extern crate diesel;

use rocket::{Build, Rocket};
// use rocket_db_pools::{sqlx, Connection, Database};
use serde::Deserialize;

#[database("db")]
pub struct DbConn(diesel::PgConnection);

// #[derive(Database)]
// #[database("sqlx")]
// struct Db(sqlx::PgPool);

use rocket::fairing::AdHoc;
use rocket::fs::FileServer;

#[derive(Debug, Deserialize)]
pub struct RocketConfig {
    pub default_map_algo: Option<i32>,
    pub default_sum_algo: Option<i32>,
}

// #[launch]
pub fn create_rocket() -> Rocket<Build> {
    rocket::build()
        .attach(DbConn::fairing())
        .attach(AdHoc::config::<RocketConfig>())
        .mount("/", FileServer::from("static/"))
}
