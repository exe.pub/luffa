use super::maps::format_map_tags;
use super::*;
use crate::models;
use crate::models::history::ResultChange;
use maud::{html, Markup};
use std::collections::HashMap;

pub fn map_results(
    map: &models::Map,
    server: &models::Server,
    ratings: &[models::Rating],
    speed_record: Option<&(models::SpeedRecord, models::Player)>,
    time_records: &[models::PlayerTimeRecord],
    points: &HashMap<(i32, i32), Decimal>,
    images: &[models::MapImage],
    changesets: &[(
        models::Map,
        models::Server,
        models::ResultsChangeset,
        models::history::ResultChangeset,
    )],
    players: &HashMap<i32, models::Player>,
    videos: &[(models::MapVideo, models::Player)],
    max_cp: usize,
) -> Markup {
    base_page(
        &format!("{}: XDF: Relaxed Running", map.name),
        html! {
            div class="row" {
                div class="col-lg-7 col-md-9 col-sm-12 order-2 order-sm-1" {
                    h3 { (map.name)
                          @if map.rating_votes > 0 {
                              abbr class="ml-2" title="map rating (rating/number of votes)" {
                                  code {
                                      (format!("{:+}", map.rating_sum)) "/" (map.rating_votes)
                                  }
                              }
                          }
                    }
                    div class="mt-2 mb-2" {
                        (format_map_tags(map))
                    }
                    div {
                        p {
                            "Available on server: " code class="xolonium" {
                                (server.name)
                            }
                        }
                    }
                    table class="table table-bordered" {
                        thead {
                            tr {
                                th {"Speed record by"}
                                th {"Speed"}
                                th {"Set on"}
                            }
                        }
                        tbody {
                            @if let Some((sr, p)) = speed_record {
                                tr {
                                    td {(format_player_link(p, true))}
                                    td {(format_speed(sr.speed))}
                                    td {(ago(&sr.created_at))}
                                }
                            }
                        }
                    }
                    ul class="nav nav-tabs" role="tablist" {
                        li class="nav-item" {
                            a id="stats1" class="nav-link stats-link active" {
                                "Reltime/Rating"
                            }
                        }
                        li class="nav-item stats-link" {
                            a id="stats2" class="nav-link stats-link" {
                                "Speeds/Strafe%"
                            }
                        }
                        li class="nav-item" {
                            a id="stats3" class="nav-link stats-link" {
                                "Checkpoints"
                            }
                        }
                        li class="nav-item" {
                            a id="stats4" class="nav-link stats-link" {
                                "Checkpoints Delta"
                            }
                        }
                    }
                    div class="table-responsive" {
                        table class="table table-bordered table-striped" {
                            thead {
                                tr {
                                    th {"Pos"}
                                    th {"Player"}
                                    th {"Time"}
                                    th class="stat1" {"Reltime"}
                                    th class="stat2 hidden" {"Start speed"}
                                    th class="stat2 hidden" {"Average speed"}
                                    th class="stat2 hidden" {"Top speed"}
                                    th class="stat2 hidden" {"Strafe %"}
                                    th class="stat1" {"Set on"}
                                    @for rating in ratings {
                                        th class="stat1" { (rating.name) }
                                    }
                                    @for cp_num in 1..max_cp+1 {
                                        th class="stat3 hidden" { (format!("#{}", cp_num)) }
                                    }
                                    @for cp_num in 1..max_cp+1 {
                                        th class="stat4 hidden" { (format!("#{}", cp_num)) }
                                    }
                                }
                            }
                            tbody {
                                @for record in time_records.iter() {
                                    tr {
                                        td {(record.time_record.pos)}
                                        td {(format_player_link(&record.player, true))}
                                        td {(format_time(record.time_record.time))}
                                        td class="stat1" {code class="text-nowrap xolonium" {(format!("{:.3}", record.time_record.reltime))} }
                                        td class="stat2 hidden" {code class="text-nowrap xolonium" {
                                            @if let Some(v) = record.time_record.start_speed {
                                                (format!("{:.0} qu/s", v))
                                            }
                                            @else {
                                                "-"
                                            }
                                        }}
                                        td class="stat2 hidden" {code class="text-nowrap xolonium" {
                                            @if let Some(v) = record.time_record.average_speed {
                                                (format!("{:.0} qu/s", v))
                                            }
                                            @else {
                                                "-"
                                            }
                                        }}
                                        td class="stat2 hidden" {code class="text-nowrap xolonium" {
                                            @if let Some(v) = record.time_record.top_speed {
                                                (format!("{:.0} qu/s", v))
                                            }
                                            @else {
                                                "-"
                                            }
                                        }}
                                        td class="stat2 hidden" {code class="text-nowrap xolonium" {
                                            @if let Some(v) = record.time_record.strafe_percentage {
                                                (format!("{:.0}%", v * 100.))
                                            }
                                            @else {
                                                "-"
                                            }
                                        }}
                                        td class="stat1" {(ago(&record.time_record.checkpoint_house_timestamp.unwrap_or(record.time_record.created_at)))}
                                        @for rating in ratings {
                                            td class="stat1" {
                                                @if let Some(score) = points.get(&(rating.id, record.time_record.id)) {
                                                    (format_dec(*score, 0))
                                                }
                                            }
                                        }
                                        @for cp_num in 0..max_cp {
                                            td class="stat3 hidden" {
                                                @if let Some(cps) = &record.time_record.checkpoints {
                                                    @if let Some(t) = cps[cp_num] {
                                                        (format_time(t))
                                                    }
                                                    @else {
                                                        "-"
                                                    }
                                                }
                                                @else {
                                                    "-"
                                                }
                                            }
                                        }
                                        @let mut prev = dec!(0);
                                        @for cp_num in 0..max_cp {
                                            td class="stat4 hidden" {
                                                @if let Some(cps) = &record.time_record.checkpoints {
                                                    @if let Some(t) = cps[cp_num] {
                                                        ({
                                                            let old_prev = prev;
                                                            prev = t;
                                                            format_time(t - old_prev)
                                                        })
                                                    }
                                                    @else {
                                                        "-"
                                                    }
                                                }
                                                @else {
                                                    "-"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                div class="col-lg-5 col-md-3 col-sm-12 order-1 order-sm-2" {
                    @for (v, _p) in videos.iter().take(1) {
                        div {
                            (embed_youtube(v.get_video_code().unwrap()))
                        }
                    }
                    @if videos.is_empty() {
                        @for i in images {
                            div {
                                img style="max-width: 512px" class="mb-5 img-fluid" src=(i.url) alt=(map.name);
                            }
                        }
                    }
                    h3 class="mt-3" { "Recent games" }
                    @for (_m, _server, obj, c) in changesets {

                        h4 { (ago(&obj.created_at)) }

                        @if let Some(sr_change) = &c.speed_record_change {
                            p {
                                "New speed record by "
                                    (format_player_link(&players[&sr_change.new_player_id], true))
                                    ": "
                                    (format_speed(sr_change.new_speed))
                            }
                        }
                        table class="table w-100" {
                            @for change in c.changes.iter() {
                                @let player = &players[&change.player_id];
                                tr {
                                    @match change.change {
                                        ResultChange::New(np, nt) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="text-right" style="width: 10%" {
                                                (format_pos(np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(nt)) }
                                        }
                                        ResultChange::Improvement(np, nt, _op, ot) | ResultChange::Degradation(np, nt, _op, ot) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="code text-right" style="width: 10%" {
                                                (format_pos(np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(nt))
                                                                           " ( " (format_time(ot)) " )" }
                                        }
                                        ResultChange::Deleted(_op, ot) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="code text-right" style="width: 10%" {
                                                abbr title="deleted time" {
                                                    i class="text-danger fas fa-times" {}
                                                }
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(ot))}
                                        }
                                    }
                                }
                            }
                        }
                        hr {}
                    }
                }
            }
        },
        html! {
            script type="text/javascript" src="/static/js/map.js" {};
        },
    )
}
