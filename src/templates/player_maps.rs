use super::*;
use crate::models;
use maud::{html, Markup};

pub fn player_maps_page(
    player: &models::Player,
    maps: &[models::PlayerTimeRecord],
    ordering: models::PlayerMapsOrdering,
) -> Markup {
    base_page(
        &format!("{}: XDF: Relaxed Running", player.nickname_nocolors),
        html! {
            div class="row" {
                div class="col-12" {
                    h2 class="mb-5" {
                        (format_player_link(player, false))
                    }
                    (player_map_table(maps, Some(ordering)))
                }
            }
        },
        html! {},
    )
}
