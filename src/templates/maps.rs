use super::*;
use crate::models;
use maud::{html, Markup};

fn format_map_img(images: &[models::MapImage]) -> Markup {
    let maybe_img = images.get(0);
    if let Some(img) = maybe_img {
        html! {
            img class="img-fluid" style="max-height: 6em;" src=(img.url);
        }
    } else {
        html! {}
    }
}

macro_rules! map_tag_url {
    ($map:expr, $tag:ident) => {{
        if $map.$tag {
            let tag = stringify!($tag);
            html! {
                img class="mr-2" title=(tag) style="max-width: 24px" src=(format!("/static/img/map-tags/{}.png", tag));
            }
        } else {
            html! {}
        }
    }};
}

pub fn format_map_tags(map: &models::Map) -> Markup {
    html! {
        (map_tag_url!(map, haste));
        (map_tag_url!(map, slick));
        // (map_tag_url!(map, vq3_compat));
        (map_tag_url!(map, nexrun));
        (map_tag_url!(map, sg));
        (map_tag_url!(map, mg));
        (map_tag_url!(map, rocket));
        (map_tag_url!(map, mortar));
        (map_tag_url!(map, crylink));
        (map_tag_url!(map, hagar));
        (map_tag_url!(map, hook));
        (map_tag_url!(map, fireball));
        (map_tag_url!(map, nex));
        (map_tag_url!(map, electro));
        (map_tag_url!(map, blaster));
        (map_tag_url!(map, mine_layer));
    }
}

fn format_records(map: &models::Map, records: &[models::PlayerTimeRecord]) -> Markup {
    if let Some(_) = records.get(0) {
        // let score = &rec.time_record.score;
        html! {
            div {
                @for (pos, i) in records.iter().enumerate() {
                    p class="mb-0" {
                        (format_pos(pos as i16 + 1));
                        ": ";
                        (format_player_link(&i.player, true));
                        " - ";
                        (format_time(i.time_record.time));
                        " - ";
                        { code class="xolonium text-nowrap" { (format_dec(i.time_record.reltime, 3)) } }
                    };
                }
            }
            div class="mt-1" style="font-size: 100%" {
                "Total records: ";
                { code class="xolonium text-nowrap" { (map.max_pos) } }
            }
        }
    } else {
        html! {}
    }
}

fn pages(filter_name: Option<&str>, page_number: i64, page_size: i64, total_pages: i64) -> Markup {
    html! {
        @if page_number > 0 {
            a href=(format!("?filter.name={}&filter.page_number={}&filter.page_size={}", filter_name.unwrap_or(""), page_number - 1, page_size)) {"⇇ Prev page "}
        }
        span {(format!("Page {} of {}", page_number + 1, total_pages))}
        @if page_number + 1 < total_pages {
            a href=(format!("?filter.name={}&filter.page_number={}&filter.page_size={}", filter_name.unwrap_or(""), page_number + 1, page_size)) {" Next page ⇉"}
        }
    }
}

pub fn maps_page(
    maps: &[models::MapWithMetadata],
    filter_name: Option<&str>,
    page_number: i64,
    page_size: i64,
    total_pages: i64,
) -> Markup {
    base_page(
        "Maps: XDF: Relaxed Running",
        html! {
            div class="row" {
                div class="col-12" {
                    h2 {"Maps"}
                    form class="form-inline" method="GET" {
                        input class="d-none" name="filter.page_size" value=(page_size);
                        input class="d-none" name="filter.page_number" value="0";
                        input class="form-control" name="filter.name" value=(filter_name.unwrap_or(""));
                        button class="btn btn-primary" {"search by name"}
                    }
                    (pages(filter_name, page_number, page_size, total_pages));
                    div class="table-responsive" {
                        table class="table table-bordered table-striped" {
                            col width="256";
                            tr {
                                th {"Pic"}
                                th {"Map"}
                                th {"Leaders"}
                            }
                            @for map in maps {
                                tr {
                                    td {(format_map_img(&map.images))}
                                    td class="pt-1 pl-1" {
                                        h5 {a href=(uri!(map(map_name=&map.map.name))) { (map.map.name) }}
                                        div class="mt-2" {
                                            (format_map_tags(&map.map))
                                        }
                                    }
                                    td class="pt-1 pl-1" {
                                        (format_records(&map.map, &map.records))
                                    }
                                }
                            }
                        }
                    }
                    (pages(filter_name, page_number, page_size, total_pages));
                }
            }
        },
        html! {},
    )
}
