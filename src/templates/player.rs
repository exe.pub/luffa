use super::*;
use crate::models;
use crate::models::history::ResultChange;
use crate::models::OrderDirection;
use crate::models::PlayerMapsField;
use maud::{html, Markup, PreEscaped};

fn heatmap_data(player: &models::Player, profile: &models::PlayerProfile) -> Markup {
    let mut data = format!("window.player_id = {}; window.heatmap_data = [", player.id);
    let mut first = true;
    for (x, y, map_name) in models::heatmap_data(&profile.time_records) {
        if !first {
            data.push(',');
        } else {
            first = false;
        }
        data.push_str(&format!(
            "{{x:{}, y: {}, value: 1, map: '{}' }}",
            x, y, map_name
        ));
    }
    data.push_str("];");
    html! {
        (PreEscaped(data))
    }
}

pub fn player_page(
    player: &models::Player,
    profile: &models::PlayerProfile,
    ratings: &[models::Rating],
) -> Markup {
    base_page(
        &format!("{}: XDF: Relaxed Running", player.nickname_nocolors),
        html! {
            div class="row" {
                div class="col-md-6 col-sm-12" {
                    h2 {(format_player(player, true))}
                }
                div class="col-lg-3 col-md-5 col-sm-12" {
                    table class="table table-bordered" {
                        tr {
                            th {"Rating"}
                            th {"Maps played"}
                            th {"Position"}
                            th {"Score"}
                        }
                        @for (r, rt) in profile.ratings.iter() {
                            tr {
                                td {code class="xolonium" {(r.name)}}
                                td {code class="xolonium" {(rt.map_count)}}
                                td {code class="xolonium" {(rt.pos) "/" (r.max_pos)}}
                                td {code class="xolonium" { (format_dec(rt.score, 0))}}
                            }
                        }
                        @if profile.videos_number > 0 {
                            tr {
                                th {"Videos"}
                                td {
                                    a href=(uri!(player_videos(player_id = player.id))) {(profile.videos_number)}
                                }
                            }
                        }
                    }
                }
                div class="col-12" {
                    /* h3 {"Results visualization"} */
                    ul class="nav nav-tabs" role="tablist" {
                        li class="nav-item" {
                            a class="nav-link active" data-toggle="tab" href="#heatmap-tab" {
                                "Heatmap"
                            }
                        }
                        li class="nav-item" {
                            a class="nav-link" data-toggle="tab" href="#rating-history-tab" {
                                "Rating History"
                            }
                        }
                    }
                    div class="tab-content" {
                        div class="tab-pane fade show active" id="heatmap-tab" {
                            div class="table-responsive heatmap-container" {
                                div id="relpos-description-container" {
                                    span id="relpos-desc-1" class="relpos-description" {"← first"}
                                    span id="relpos-desc-2" class="relpos-description" {"rel. position"}
                                    span id="relpos-desc-3" class="relpos-description" {"last →"}
                                }
                                div class="heatmap-axis-description-container" {
                                    span id="better-than-others" class="heatmap-axis-description" { "← better than others" };
                                    span id="reltime-description" class="heatmap-axis-description" {"rel. time"}
                                    span id="worse-than-the-best" class="heatmap-axis-description" { "worse than the best →" };
                                }
                                div id="heatmap" {}
                            }
                            div class="heatmap-hint" {
                            }
                            div class="heatmap-legend" {
                                pre {}
                            }
                        }
                        div class="tab-pane fade" id="rating-history-tab" {
                            div class="chart-container" style="position: relative; height:40vh;" {
                                canvas id="rating-history-chart" {}
                            }
                        }
                    }
                }
            }
            div class="row" {
                div class="col-md-6 col-sm-12" {
                    h3 {"Recent Activity"}
                    div class="table-responsive" {
                        table class="table w-100" {
                            @for (m, rcset, c) in &profile.recent_results {
                                tr {
                                    @match c {
                                        ResultChange::New(np, nt) => {
                                            td style="width: 60%" {
                                                a href=(uri!(map(map_name=&m.name))) { (m.name) }
                                            }
                                            td {(ago(&rcset.created_at))}
                                            td class="text-right" style="width: 10%" {
                                                (format_pos(*np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(*nt)) }
                                        }
                                        ResultChange::Improvement(np, nt, _op, ot) | ResultChange::Degradation(np, nt, _op, ot) => {
                                            td style="width: 60%" {
                                                a href=(uri!(map(map_name=&m.name))) { (m.name) }
                                            }
                                            td {(ago(&rcset.created_at))}
                                            td class="code text-right" style="width: 10%" {
                                                (format_pos(*np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(*nt))
                                                                           " ( " (format_time(*ot)) " )" }
                                        }
                                        ResultChange::Deleted(_op, ot) => {
                                            td style="width: 60%" {
                                                a href=(uri!(map(map_name=&m.name))) { (m.name) }
                                            }
                                            td {(ago(&rcset.created_at))}
                                            td class="code text-right" style="width: 10%" {
                                                abbr title="deleted time" {
                                                    i class="text-danger fas fa-times" {}
                                                }
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(*ot))}
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                div class="col-md-6 col-sm-12" {
                    h3 {"Best Maps";
                        a class="subheader float-right"
                        href=(uri!(player_maps(player_id = player.id, order_field = _, order_dir= _))) {"All maps"}
                    }
                    ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" {
                        @for (i, rating) in ratings.iter().enumerate() {
                            @let aclass = if i == 0 {"nav-link active"} else {"nav-link"};
                            li class="nav-item mr-2" role="presentation" {
                                a class=(aclass)
                                    id=(format!("rating-{}-pill", rating.id))
                                    data-toggle="pill"
                                    href=(format!("#rating-{}", rating.id))
                                    role="tab"
                                    aria-controls=(format!("rating-{}", rating.id))
                                    aria-selected=(if i==0 {"true"} else {"false"}) {(rating.name)}
                            }
                        }
                    }
                    div class="tab-content" id="pills-tabContent" {
                        @for (i, rating) in ratings.iter().enumerate() {
                            @let dclass = if i == 0 {"tab-pane fade show active"} else {"tab-pane fade"};
                            div class=(dclass)
                                id=(format!("rating-{}", rating.id))
                                role="tabpanel"
                                aria-labelledby=(format!("rating-{}-pill", rating.id)) {
                                    div class="table-responsive" {
                                        table class="table" {
                                            @for (ptr, score) in profile.best_maps[i].1.iter() {
                                                tr {
                                                    td {a href=(uri!(map(map_name=&ptr.map.name))) {(ptr.map.name)} }
                                                    td {(ago(&ptr.time_record.created_at))}
                                                    td {(format_time(ptr.time_record.time))}

                                                    td {code class="xolonium text-nowrap" {(ptr.time_record.pos) "/" (ptr.map.max_pos)} }
                                                    td {code class="xolonium text-nowrap" {(format_dec(ptr.time_record.reltime, 3))} }
                                                    td {code class="xolonium text-nowrap" {(format_dec(*score, 0))} }
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        },
        html! {
            script type="text/javascript" src="/static/js/heatmap.min.js" {};
            script {
                (heatmap_data(player, profile))
            }
            script type="text/javascript" src="/static/js/Chart.bundle.min.js" {};
            script type="text/javascript" src="/static/js/player.js" {};
        },
    )
}
