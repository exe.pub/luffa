use super::*;
use crate::models;
use crate::models::history::ResultChange;
use maud::{html, Markup};
use std::collections::HashMap;

pub fn index_page(
    players: &HashMap<i32, models::Player>,
    changesets: &[(
        models::Map,
        models::Server,
        models::ResultsChangeset,
        models::history::ResultChangeset,
    )],
    maps: &[(models::MapUpdate, models::Map, models::Server)],
) -> Markup {
    base_page(
        "XDF: Relaxed Running",
        html! {
            div class="row" {
                div class="col-lg-7 col-md-12" {
                    h3 {"Recent activity"};
                    @for (m, server, obj, c) in changesets {
                        h4 class="mt-3" {
                            a href=(uri!(map(map_name=&m.name))) { (m.name) }
                            span class="float-right" style="font-size: 70%" {
                                abbr title="best time at the moment of the game" {
                                    (format_time(obj.best_time))
                                } " | ";
                                abbr title="total records at the moment of the game" {
                                    code class="xolonium" {
                                        (obj.max_pos)
                                    }
                                } " | ";
                                @if m.rating_votes > 0 {
                                    abbr title="map rating (rating/number of votes)" {
                                        code {
                                            (format!("{:+}", m.rating_sum)) "/" (m.rating_votes)
                                        } }
                                    " | "
                                }
                                abbr title="on which server the game was played" {
                                    code {(server.name) }
                                }
                                " | ";
                                (ago(&obj.created_at))
                            }
                        }
                        @if let Some(sr_change) = &c.speed_record_change {
                            p {
                                "New speed record by "
                                    (format_player_link(&players[&sr_change.new_player_id], true))
                                    ": "
                                    (format_speed(sr_change.new_speed))
                            }
                        }
                        table class="table w-100" {
                            @for change in c.changes.iter() {
                                @let player = &players[&change.player_id];
                                tr {
                                    @match change.change {
                                        ResultChange::New(np, nt) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="text-right" style="width: 10%" {
                                                (format_pos(np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(nt)) }
                                        }
                                        ResultChange::Improvement(np, nt, _op, ot) | ResultChange::Degradation(np, nt, _op, ot) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="code text-right" style="width: 10%" {
                                                (format_pos(np))
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(nt))
                                                                           " ( " (format_time(ot)) " )" }
                                        }
                                        ResultChange::Deleted(_op, ot) => {
                                            td style="width: 60%" {
                                                (format_player_link(player, true))
                                            }
                                            td class="code text-right" style="width: 10%" {
                                                abbr title="deleted time" {
                                                    i class="text-danger fas fa-times" {}
                                                }
                                            }
                                            td class="pl-2 text-nowrap" { (format_time(ot))}
                                        }
                                    }
                                }
                            }
                        }
                        hr {}
                    }
                }
                div class="col-lg-5 col-md-12" {
                    h3 {"Map updates"}
                    div class="table-responsive" {
                        @for (u, m, s) in maps {
                            p {
                                @if u.deleted {
                                    i class="text-danger fas fa-times" {}
                                }
                                @else {
                                    i class="text-success fas fa-check" {}
                                }
                                " ";
                                (ago(&u.created_at)) ": "
                                    a href=(uri!(map(map_name=&m.name))) {(m.name)}
                                @if u.deleted {
                                    " removed from server "
                                }
                                @else {
                                    " added to server "
                                }
                                {(s.name)}
                            }
                        }
                    }
                }
            }

        },
        html! {},
    )
}
