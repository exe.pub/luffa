use super::*;
use crate::models;
use maud::{html, Markup};
use num_traits::cast::ToPrimitive;

fn versus_form(player1: Option<&models::Player>, player2: Option<&models::Player>) -> Markup {
    html! {
        form action="/versus" {
            @if let Some(p) = player1 {
                input type="hidden" name="player1" id="player1" value=(p.id) {};
            }
            @else {
                input type="hidden" name="player1" id="player1" {};
            }
            @if let Some(p) = player2 {
                input type="hidden" name="player2" id="player2" value=(p.id) {};
            }
            @else {
                input type="hidden" name="player2" id="player2" {};
            }
            div class="row" {
                div class="col-lg-3 col-md-5 col-sm-10" {
                    div class="form-group" {
                        input type="text"
                            class="player-select-widget form-control typeahead"
                            data-target-field="#player1"
                            autocomplete="off" {}
                    }
                }
                div class="col-lg-3 col-md-5 col-sm-10" {
                    div class="form-group" {
                        input type="text"
                            class="player-select-widget form-control typeahead"
                            data-target-field="#player2"
                            autocomplete="off" {}
                    }
                }
                div class="col-2" {
                    button class="btn btn-primary" {"Compare"}
                }
            }
        }
    }
}

pub fn versus_page() -> Markup {
    base_page(
        "Compare Players: XDF: Relaxed Running",
        html! {
            h2 {"Compare Players"}
            (versus_form(None, None))
        },
        javascripts(html! {
            script type="text/javascript" src="/static/js/bootstrap3-typeahead.min.js" {};
            script type="text/javascript" src="/static/js/admin.js" {};
        }),
    )
}

pub fn versus_same_player(p: models::Player) -> Markup {
    base_page(
        "Compare Players: XDF: Relaxed Running",
        html! {
            h2 {"Compare Players"}
            (versus_form(Some(&p), Some(&p)));
            p {
                (format_player_link(&p, false)) " and " (format_player_link(&p, false))
                    " have exactly the same results. Surprised?"
            }
        },
        javascripts(html! {
            script type="text/javascript" src="/static/js/bootstrap3-typeahead.min.js" {};
            script type="text/javascript" src="/static/js/admin.js" {};
        }),
    )
}

fn diff_gradient(t1: &models::TimeRecord, t2: &models::TimeRecord) -> String {
    let base: f64 = 59.0; // 3b
    if t1.time > t2.time {
        let reltime = ((&t1.time - &t2.time) / &t2.time).to_f64().unwrap();
        let red = if reltime > 0.2 {
            0xff
        } else {
            (base + reltime * (255.0 - base) / 0.2).round() as u8
        };
        format!("#{:X}3b3b", red)
    } else {
        let reltime = ((&t2.time - &t1.time) / &t2.time).to_f64().unwrap();
        let green = if reltime > 0.2 {
            0xff
        } else {
            (base + reltime * (255.0 - base) / 0.2).round() as u8
        };
        format!("#3b{:X}3b", green)
    }
}

pub fn versus_table(
    player1: &models::Player,
    player2: &models::Player,
    comparison: &[models::PlayerComparison],
) -> Markup {
    let hundred = dec!(100);
    base_page(
        "Compare Players: XDF: Relaxed Running",
        html! {
            h2 {"Compare Players"}
            (versus_form(Some(player1), Some(player2)));
            table class="table-bordered" {
                tr {
                    th colspan="2" {
                        (format_player_link(player1, false))
                    }
                    th class="text-center" {"Map"}
                    th class="text-right" colspan="2" {
                        (format_player_link(player2, false))
                    }
                }
                @for row in comparison {
                    tr {
                        td {
                            (row.left.pos) "/" (row.map.max_pos)
                        }
                        td {
                            (format_time(row.left.time))
                        }
                        td class="text-center"
                            style=(format!("background-color: {}", diff_gradient(&row.left, &row.right))) {
                                a style="color: #fff" href=(uri!(map(map_name=&row.map.name))) { (row.map.name) };
                                br;
                                (format_dec(
                                    (row.right.time - row.left.time) * hundred
                                        / std::cmp::min(row.right.time, row.left.time), 3)) "%"
                            }
                        td {
                            (format_time(row.right.time))
                        }
                        td {
                            (row.right.pos) "/" (row.map.max_pos)
                        }
                    }
                }
            }
        },
        html! {
            script type="text/javascript" src="/static/js/bootstrap3-typeahead.min.js" {};
            script type="text/javascript" src="/static/js/admin.js" {};
        },
    )
}
