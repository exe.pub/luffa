use super::*;
use crate::models;
use maud::{html, Markup};
use std::collections::HashMap;

pub fn rating_display_page(
    rating: &models::Rating,
    ratings: &[models::Rating],
    points: &[(models::Player, models::RatingTotal)],
    history: &HashMap<models::Player, Vec<models::RatingHistory>>,
) -> Markup {
    base_page(
        "Player Rating - XDF: Relaxed Running",
        html! {
            div class="row" {
                div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12" {
                    @for r in ratings {
                        @if r.id == rating.id {
                            a class="btn btn-primary mr-2" href=(uri!(rating_specific(rating_id=r.id))) {(r.name)}
                        }
                        @else {
                            a class="btn btn-secondary mr-2" href=(uri!(rating_specific(rating_id=r.id))) {(r.name)}
                        }
                    }
                    h2 class="mt-3" {"Player Rating: " (rating.name) }
                    div class="table-responsive" {
                        table class="table table-bordered table-striped table-hover" {
                            thead {
                                tr {
                                    th {"Pos"}
                                    th {"Player"}
                                    th {abbr title="Number of maps where player has set a time" {"Maps"} }
                                    th class="table-primary" {"Score"}
                                    th {abbr title="Number of points / ranks gained or lost over last 24 hours" {"+1d"}}
                                    th {abbr title="Number of points / ranks gained or lost over last 7 days" {"+7d"}}
                                    th {abbr title="Number of points / ranks gained or lost over last 30 days" {"+30d"}}
                                }
                            }
                            tbody {
                                @for (p, rt) in points.iter() {
                                    tr {
                                        td {( rt.pos )}
                                        td {(format_player_link(p, true))}
                                        td {(rt.map_count)}
                                        td class="table-primary" {
                                            (format_dec(rt.score, 1))
                                        }
                                        @if let Some(h) = history.get(p) {
                                            @for i in h {
                                                @let (delta_s, delta_p) = i.compare(rt);
                                                td {(format_delta_dec(delta_s)) "/" (format_delta_i(delta_p))}
                                            }
                                            @for _ in 0..(3-h.len()) {
                                                td {"-"}
                                            }
                                        }
                                        @else {td {"-"}; td {"-"}; td {"-"}}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        html! {},
    )
}
