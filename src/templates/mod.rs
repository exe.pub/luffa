use crate::controllers::*;
use crate::models;
use chrono::prelude::*;
use dpcolors::DPString;
use maud::{html, Markup, PreEscaped, DOCTYPE};
use rocket::http::uri::{fmt::Query, fmt::UriDisplay};
use rocket::uri;
use rust_decimal::{Decimal, RoundingStrategy};
use rust_decimal_macros::*;

pub mod admin_players;
mod index;
mod map;
mod maps;
mod player;
mod player_maps;
mod player_videos;
mod rating;
mod versus;

pub const SITE_TITLE: &str = "XDF: Relaxed Running";

pub use index::index_page;
pub use map::map_results;
pub use maps::maps_page;
pub use player::player_page;
pub use player_maps::player_maps_page;
pub use player_videos::player_videos;
pub use rating::rating_display_page;
pub use versus::{versus_page, versus_same_player, versus_table};

fn head(title: &str) -> Markup {
    html! {
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no";
            title { (title) }
            link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css";
            link rel="stylesheet" type="text/css" href="/static/css/font-awesome.css";
            link rel="stylesheet" type="text/css" href="/static/css/flag-icon.min.css";
            link rel="stylesheet" type="text/css" href="/static/css/style.css";
            link rel="stylesheet" type="text/css" href="/static/css/Chart.min.css";
            link rel="apple-touch-icon" sizes="57x57" href="/static/favs/apple-icon-57x57.png";
            link rel="apple-touch-icon" sizes="60x60" href="/static/favs/apple-icon-60x60.png";
            link rel="apple-touch-icon" sizes="72x72" href="/static/favs/apple-icon-72x72.png";
            link rel="apple-touch-icon" sizes="76x76" href="/static/favs/apple-icon-76x76.png";
            link rel="apple-touch-icon" sizes="114x114" href="/static/favs/apple-icon-114x114.png";
            link rel="apple-touch-icon" sizes="120x120" href="/static/favs/apple-icon-120x120.png";
            link rel="apple-touch-icon" sizes="144x144" href="/static/favs/apple-icon-144x144.png";
            link rel="apple-touch-icon" sizes="152x152" href="/static/favs/apple-icon-152x152.png";
            link rel="apple-touch-icon" sizes="180x180" href="/static/favs/apple-icon-180x180.png";
            link rel="icon" type="image/png" sizes="192x192"  href="/static/favs/android-icon-192x192.png";
            link rel="icon" type="image/png" sizes="32x32" href="/static/favs/favicon-32x32.png";
            link rel="icon" type="image/png" sizes="96x96" href="/static/favs/favicon-96x96.png";
            link rel="icon" type="image/png" sizes="16x16" href="/static/favs/favicon-16x16.png";
            link rel="manifest" href="/static/favs/manifest.json";
            meta name="msapplication-TileColor" content="#ffffff";
            meta name="msapplication-TileImage" content="/static/favs/ms-icon-144x144.png";
            meta name="theme-color" content="#ffffff";
        }
    }
}

fn javascripts(extra_js: Markup) -> Markup {
    html! {
        script type="text/javascript" src="/static/js/jquery.min.js" {};
        script type="text/javascript" src="/static/js/bootstrap.bundle.min.js" {};
        (extra_js)
    }
}

fn footer() -> Markup {
    html! {
        hr;
        footer class="container mb-3" {
            div class="row" {
                div class="col-12 text-center" {
                    "Made with some help of the "
                        a href="https://rust-lang.org" {"Static Typechecking Power"};
                    br;
                    "Also special thanks to " a href="https://rocket.rs/" {"rocket"};
                    " and "
                    a href="https://diesel.rs/" {"diesel"};
                }
            }
        }
    }
}

fn header() -> Markup {
    html! {
        nav class="navbar navbar-expand-lg navbar-light bg-light mb-4" {
            a class="navbar-brand" href=(uri!(index)) { (SITE_TITLE) };
            button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation" { span class="navbar-toggler-icon" {} };
            div class="collapse navbar-collapse" id="mainNavbar" {
                ul class="navbar-nav" {
                    li class="nav-item" {
                        a class="nav-link" href=(uri!(maps)) {"Maps"};
                    }
                    li class="nav-item" {
                        a class="nav-link" href=(uri!(rating)) {"Player Rating"};
                    }
                }
            }
        }
    }
}

fn base_admin_page(body: Markup) -> Markup {
    html! {
        (DOCTYPE);
        html {
            (head("Luffa Admin"));
            body class="xolonium" {
                div class="container-fluid" {
                    (body)
                }
                (footer())
            }
            (javascripts(html! {
                script type="text/javascript" src="/static/js/bootstrap3-typeahead.min.js" {};
                script type="text/javascript" src="/static/js/admin.js" {};
            }));
        }
    }
}

fn base_page(title: &str, body: Markup, extra_js: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            (head(title));
            body class="xolonium" {
                (header());
                div class="container-fluid" {
                    (body);
                }
                (footer())
            }
            (javascripts(extra_js));
        }
    }
}

// fn duration(d: &chrono::Duration) -> Markup {
//     let mut f = timeago::Formatter::new();
//     f.ago("");
//     html! {
//         span class="duration" {
//             (f.convert(d.to_std().unwrap()))
//         }
//     }
// }

fn ago(d: &chrono::NaiveDateTime) -> Markup {
    let n = Utc::now().naive_utc();
    let duration = n - *d;
    let f = timeago::Formatter::new();
    html! {
        abbr class="duration text-nowrap" title=(d) {
            (f.convert(duration.to_std().unwrap()))
        }
    }
}

const PLAYER_MAX_NICK: usize = 30;

pub fn format_dpstring(s: &str, truncate: bool) -> Markup {
    let parsed = DPString::parse(s);
    let h = if truncate {
        parsed.to_html_truncated(PLAYER_MAX_NICK)
    } else {
        parsed.to_html()
    };
    html! {
        (PreEscaped(h))
    }
}

fn format_player(player: &models::Player, truncate: bool) -> Markup {
    let dp_string = DPString::parse(&player.nickname);
    let html_nick = if truncate {
        dp_string.to_html_truncated(PLAYER_MAX_NICK)
    } else {
        dp_string.to_html()
    };
    html! {
        span class="text-nowrap player-nick" { (PreEscaped(html_nick)) }
    }
    // html! {
    //     span title=(&player.country) class=(format!("mr-2 flag-icon flag-icon-{}", &player.country)) {
    //         ""
    //     }
    //     (nickname)
    // }
}

fn format_player_link(player: &models::Player, truncate: bool) -> Markup {
    html! {
        a class="player-link" href={(uri!(player(player_id=player.id)))} {(format_player(player, truncate))}
    }
}

fn format_speed(speed: Decimal) -> Markup {
    let s = speed.round_dp_with_strategy(0, RoundingStrategy::MidpointAwayFromZero);
    html! {
        code class="text-nowrap xolonium" {
            (s) "qu/s"
        }
    }
}

fn format_time(time: Decimal) -> Markup {
    let sixty_x_sixty = dec!(3600.00);
    let sixty = dec!(60.00);
    let hours = (time / sixty_x_sixty).floor();
    let minutes = ((time - hours * sixty_x_sixty) / sixty).floor();
    let mut seconds = time - hours * sixty_x_sixty - minutes * sixty;
    seconds.rescale(2);
    let seconds = format!("{:05}", seconds);
    html! {
        code class="text-nowrap xolonium" {
            @if hours == dec!(0) {
                @if minutes == dec!(0) {
                    (seconds) "s"
                }
                @else {
                    (minutes);
                    strong { ":" }
                    (seconds) "s"
                }
            }
            @else {
                (hours);
                strong { ":" }
                (format!("{:02}", minutes));
                strong { ":" }
                (seconds) "s"
            }
        }
    }
}

// fn activity_table(
//     activity: &[(models::News, models::NewsContent)],
//     include_self: bool,
//     include_beaten: bool,
// ) -> Markup {
//     html! {
//         div class="table-responsive" {
//             table class="table table-bordered" {
//                 thead {
//                     tr {
//                         th {"When"}
//                         th {"Map"}
//                         th {}
//                         @if include_self {
//                             th {"Player"}
//                         }
//                         th {"Position"}
//                         th {"Result"}
//                         @if include_beaten {
//                             th {"Beaten player"}
//                             th {"Beaten result"}
//                         }
//                     }
//                 }
//                 tbody {
//                     @for (n, c) in activity {
//                         tr {
//                             td { (ago(&n.created_at))}
//                             @match c {
//                                 models::NewsContent::TimeRecord {rec, player, map, beaten_player, pos, max_pos} => {
//                                     td {
//                                         a href=(uri!(map: map_name=&map.name)) {(map.name)}
//                                     }
//                                     td {
//                                         @match pos {
//                                             1 => i class="fas fa-medal gold-medal" title="first" {},
//                                             2 => i class="fas fa-medal silver-medal" title="second" {},
//                                             3 => i class="fas fa-medal bronze-medal" title="third" {},
//                                             _ => i class="fas fa-stopwatch" title="time record" {}
//                                         }
//                                     }
//                                     @if include_self {
//                                         td {(format_player_link(&player, true))}
//                                     }
//                                     td { code class="xolonium" {(pos) "/" (max_pos)} }
//                                     td {(format_time(&rec.rec))}
//                                     @if include_beaten {
//                                         @if let Some(ref bp) = beaten_player {
//                                             @if let Some(ref bt) = rec.beaten_rec {
//                                                 td {(format_player_link(bp, true))}
//                                                 td {(format_time(bt))}
//                                             }
//                                             @ else {
//                                                 td {} td {}
//                                             }
//                                         }
//                                         @else {
//                                             td {} td {}
//                                         }
//                                     }
//                                 },
//                                 models::NewsContent::SpeedRecord {rec, player, map, beaten_player} => {
//                                     td {
//                                         a href=(uri!(map: map_name=&map.name)) {(map.name)}
//                                     }

//                                     td {
//                                         i class="fas fa-running gold-medal" title="speed record" {}
//                                     }
//                                     @if include_self {
//                                         td {(format_player_link(&player, true))}
//                                     }
//                                     td {}
//                                     td {(format_speed(&rec.rec))}
//                                     @if include_beaten {
//                                         @if let Some(ref bp) = beaten_player {
//                                             @if let Some(ref bt) = rec.beaten_rec {
//                                                 td {(format_player_link(bp, true))}
//                                                 td {(format_speed(bt))}
//                                             }
//                                             @else {
//                                                 td {} td {}
//                                             }
//                                         }
//                                         @else {
//                                             td {} td {}
//                                         }
//                                     }
//                                 },
//                                 _ => ""
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//     }
// }

fn player_map_table_header(
    label: Markup,
    field: models::PlayerMapsField,
    ordering: Option<models::PlayerMapsOrdering>,
    default_direction: models::OrderDirection,
) -> Markup {
    if let Some(mut o) = ordering {
        if o.maps_field == field {
            // link to order in opposite direction
            o.order_direction = o.order_direction.opposite();
            let query = format!("?{}", &o as &dyn UriDisplay<Query>);
            html! {
                th { a href=(query) {(label)} }
            }
        } else {
            let o = models::PlayerMapsOrdering{maps_field: field, order_direction: default_direction};
            let query = format!("?{}", &o as &dyn UriDisplay<Query>);
            html! {
                th { a href=(query) {(label)} }
            }
        }
    } else {
        html! {
            th { (label) }
        }
    }
}

pub fn player_map_table(
    maps: &[models::PlayerTimeRecord],
    ordering: Option<models::PlayerMapsOrdering>,
) -> Markup {
    use models::OrderDirection::*;
    use models::PlayerMapsField::*;
    html! {
        div class="table-responsive" {
            table class="table table-bordered table-striped" {
                thead {
                    tr {
                        th {}
                        (player_map_table_header(html! {"Map"}, MapName, ordering, Asc));
                        (player_map_table_header(html! {"Date"}, Timestamp, ordering, Asc));
                        (player_map_table_header(html! {"Time"}, Time, ordering, Asc));
                        (player_map_table_header(html! {"Pos"}, Pos, ordering, Asc));
                        (player_map_table_header(
                            html! {abbr title="Time relative to the fastest competitor" { "Reltime" }},
                            Reltime, ordering, Asc));
                        (player_map_table_header(html! {"Score"}, Timestamp, ordering, Desc));
                    }
                }
                tbody {
                    @for (i, ptr) in maps.iter().enumerate() {
                        tr {
                            td {( (i+1) )}
                            td {a href=(uri!(map(map_name=&ptr.map.name))) {(ptr.map.name)} }
                            td {(ago(&ptr.time_record.created_at))}
                            td {(format_time(ptr.time_record.time))}

                            td {code class="xolonium text-nowrap" {(ptr.time_record.pos) "/" (ptr.map.max_pos)} }
                            td {code class="xolonium text-nowrap" {(format_dec(ptr.time_record.reltime, 3))} }
                            td {code class="xolonium text-nowrap" {} }
                        }
                    }
                }
            }
        }
    }
}

fn jsquote(s: &str) -> String {
    s.replace("\"", "\\\"").replace("\\", "\\\\")
}

fn format_dec(d: Decimal, prec: u32) -> String {
    let rounded =
        d.round_dp_with_strategy(prec, rust_decimal::RoundingStrategy::MidpointAwayFromZero);
    rounded.to_string()
}

fn format_delta_dec(delta: Decimal) -> Markup {
    /* TODO: make this more beautiful */
    let z = dec!(0);
    let bound = dec!(0.05);
    if delta.abs() < bound {
        html! {
            span class="" {"-"}
        }
    } else if delta > z {
        html! {
            span class="text-success" {"+" (format_dec(delta, 1))}
        }
    } else {
        html! {
            span class="text-danger" {(format_dec(delta, 1))}
        }
    }
}

fn format_delta_i(delta: i32) -> Markup {
    if delta == 0 {
        html! {
            span class="" {"-"}
        }
    } else if delta > 0 {
        html! {span class="text-success" {"+" (delta)}}
    } else {
        html! {span class="text-danger" {(delta)}}
    }
}

fn format_pos(pos: i16) -> Markup {
    html! {
        span {
            @match pos {
                1 => i class="fas fa-medal gold-medal" title="first" {} " ",
                2 => i class="fas fa-medal silver-medal" title="second" {} " ",
                3 => i class="fas fa-medal bronze-medal" title="third" {} " ",
                _ => {}
            }
            code class="xolonium" {
                (pos)
            }
        }
    }
}

fn embed_youtube(code: &str) -> Markup {
    html! {
        iframe width="100%" height="420" src=(format!("https://www.youtube.com/embed/{}", code)) frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="1" {}
    }
}

fn embed_youtube_high(code: &str) -> Markup {
    html! {
        iframe width="100%" height="640" src=(format!("https://www.youtube.com/embed/{}", code)) frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="1" {}
    }
}
