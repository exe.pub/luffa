use super::*;
use crate::models;
use maud::{html, Markup};

pub fn player_videos(
    player: &models::Player,
    videos: &[(models::MapVideo, models::Map)],
) -> Markup {
    base_page(
        &format!("{}: XDF: Relaxed Running", player.nickname_nocolors),
        html! {
            div class="row" {
                div class="col-md-12 col-lg-6" {
                    h2 class="mb-2" {
                        (format_player_link(player, false))
                    }
                    @for (v, m) in videos {
                        div class="mb-4" {
                            h4 {(m.name)}
                            (embed_youtube_high(v.get_video_code().unwrap()))
                        }
                    }
                }
            }
        },
        html! {},
    )
}
