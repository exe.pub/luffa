use super::*;
use crate::admin::*;
use crate::models;
use maud::{html, Markup};

pub fn player_index_page(
    players: &[(
        models::Player,
        Vec<models::PlayerKey>,
        Vec<models::PlayerNickname>,
    )],
) -> Markup {
    base_admin_page(html! {
        div class="row" {
            div class="col-12" {
                h2 {"Players"}
                table class="table table-bordered table-striped" {
                    thead {
                        tr {
                            th {"Player ID"}
                            th {"nicks"}
                            th {"keys"}
                        }
                    }
                    tbody {
                        @for (p, keys, nicks) in players {
                            tr {
                                td {
                                    a href=(uri!(player_manage(player_id=p.id))) {(p.id)}
                                }
                                td {
                                    ul class="list-unstyled" {
                                        @for n in nicks {
                                            li {
                                                a href=(uri!(player_manage(player_id=p.id))) {
                                                    (format_dpstring(&n.nickname, true))
                                                }
                                            }
                                        }
                                    }
                                }
                                td {
                                    ul class="list-unstyled" {
                                        @for k in keys {
                                            li {(k.crypto_idfp)}
                                        }
                                    }
                                }

                            }

                        }
                    }
                }

            }
        }
    })
}

pub fn player_page(player: &models::PlayerAdminProfile) -> Markup {
    base_admin_page(html! {
        script {
            (format!("window.currentPlayerId = {}", player.player.id))
        }
        div class="row mb-3" {
            div class="col-12" {
                h2 {(format_player(&player.player, true))}
                a href=(uri!(player_index)) {"Back to player list"}
            }
        }
        div class="row mb-3" {
            div class="col-lg-4 col-md-6 col-sm-12" {
                form action="/admin/player/merge" {
                    div class="form-group" {
                        input type="hidden" name="first_player_id" value=(player.player.id) {};
                        input type="hidden" name="second_player_id" id="merge_id_field" {};
                        input type="text"
                            class="player-select-widget form-control typeahead"
                            data-target-field="#merge_id_field"
                            autocomplete="off" {}
                    }
                    button class="btn btn-primary" {"Merge"}
                }
            }
        }
        div class="row" {
            div class="col-md-6 col-sm-12" {
                div class="table-responsive" {
                    table class="table table-bordered" {
                        @for i in &player.nicks {
                            tr {
                                td {a href="#"
                                    onclick=(format!("setClipboard(\"{}\")", jsquote(&i.nickname)))
                                    {(format_dpstring(&i.nickname, false))} }
                                td {(i.last_used_at)}
                            }
                        }
                    }
                }
                span class="small" {"hint: click nickname to copy its raw form (with xonotic color codes) to clipboard"}
            }
            div class="col-md-6 col-sm-12" {
                div class="table-responsive" {
                    table class="table table-bordered" {
                        @for i in &player.keys {
                            tr {
                                td {(i.crypto_idfp)}
                                td {(i.last_used_at)}
                            }
                        }
                    }
                }
            }
        }
    })
}

fn merge_description(player: &models::PlayerAdminProfile) -> Markup {
    html! {
        div class="col-md-6 col-sm-12" {
            h4 {(format_player(&player.player, true))}
            h5 {"Keys"}
            table class="table table-bordered" {
                @for i in &player.keys {
                    tr {
                        td {(i.crypto_idfp)}
                    }
                }
            }
            h5 {"Nicks"}
            table class="table table-bordered" {
                @for i in &player.nicks {
                    tr {
                        td {(format_dpstring(&i.nickname, true))}
                    }
                }
            }
        }
    }
}

pub fn merge_page(
    first: &models::PlayerAdminProfile,
    second: &models::PlayerAdminProfile,
) -> Markup {
    base_admin_page(html! {
        div class="row" {
            div class="col-12" {
                h2 {"Merge"}
                div class="alert alert-danger" {"Are you sure you want to merge the following players? Note, that merging can't be easily rolled back!"}
            }
            (merge_description(first));
            (merge_description(second));
            div class="col-12 text-center" {
                form method="post" action="/admin/player/merge" {
                    input type="hidden" name="first_player_id" value=(first.player.id) {}
                    input type="hidden" name="second_player_id" value=(second.player.id) {}
                    button class="btn btn-lg btn-danger" {"I understand everything and want to merge"}
                }
            }
        }
    })
}
