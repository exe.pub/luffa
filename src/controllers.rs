use super::error::LuffaError;
use super::models;
use super::models::{OrderDirection, PlayerMapsField, PlayerMapsOrdering};
use super::templates;
use super::DbConn;
use chrono::prelude::*;
use maud::Markup;
use rocket::form::{self, Errors, FromFormField, ValueField};
use rocket::get;
use rocket::serde::json::Json;
use std::collections::HashMap;

const PLAYER_PROFILE_MAX_ROWS: i64 = 20;

#[get("/")]
pub async fn index(conn: DbConn) -> Result<Markup, LuffaError> {
    conn.run(move |conn| {
        let players = models::Player::load_all_players_brief(conn)?;
        let changesets = models::ResultsChangeset::get_news_feed(conn, 0, None)?;
        let maps = models::MapUpdate::get_news_feed(conn)?;
        Ok(templates::index_page(&players, &changesets, &maps))
    })
    .await
}

#[get("/map/<map_name>")]
pub async fn map(conn: DbConn, map_name: String) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_map = models::Map::get_by_name_with_server(conn, &map_name)?;
        if let Some((m, s)) = maybe_map {
            let speed_record = models::SpeedRecord::get_for_map(conn, m.id)?;
            let time_records = models::TimeRecord::get_for_map(conn, &m)?;
            let ratings = models::Rating::get_for_server(conn, s.id)?;
            let points = models::Rating::get_points_for_time_records(
                conn,
                &time_records
                    .iter()
                    .map(|ptr| ptr.time_record.id)
                    .collect::<Vec<_>>(),
            )?;
            let images = models::MapImage::get_for_map(conn, &m)?;
            let changesets = models::ResultsChangeset::get_news_feed(conn, 0, Some(m.id))?;
            let players = models::Player::load_all_players_brief(conn)?;
            let videos = models::MapVideo::get_for_map(conn, &m)?;
            let mut max_cp: isize = -1;
            for rec in time_records.iter() {
                if let Some(cps) = &rec.time_record.checkpoints {
                    for (ix, i) in cps.iter().enumerate() {
                        if i.is_some() {
                            max_cp = ix as isize;
                        }
                    }
                }
            }
            Ok(Some(templates::map_results(
                &m,
                &s,
                &ratings,
                speed_record.as_ref(),
                &time_records,
                &points,
                &images,
                &changesets,
                &players,
                &videos,
                (max_cp + 1) as usize,
            )))
        } else {
            Ok(None)
        }
    })
    .await
}

fn rating_inner(
    conn: &mut diesel::PgConnection,
    rating: models::Rating,
    history: HashMap<models::Player, Vec<models::RatingHistory>>,
) -> Result<Markup, LuffaError> {
    let points = models::Rating::get_points(conn, rating.id)?;
    let ratings = models::Rating::get_all(conn)?;
    Ok(templates::rating_display_page(
        &rating, &ratings, &points, &history,
    ))
}

#[get("/rating")]
pub async fn rating(conn: DbConn) -> Result<Markup, LuffaError> {
    conn.run(|conn| {
        let rating = models::Rating::get_by_id(conn, 2)?;
        let history = models::RatingHistory::get_short(conn, rating.id)?;
        rating_inner(conn, rating, history)
    })
    .await
}

#[get("/rating/<rating_id>")]
pub async fn rating_specific(conn: DbConn, rating_id: i32) -> Result<Markup, LuffaError> {
    conn.run(move |conn| {
        let rating = models::Rating::get_by_id(conn, rating_id)?;
        let history = models::RatingHistory::get_short(conn, rating.id)?;
        rating_inner(conn, rating, history)
    })
    .await
}

#[get("/rating-history.json?<player_ids>&<from>&<to>")]
pub async fn rating_history_data(
    conn: DbConn,
    player_ids: Wrapper<Vec<i32>>,
    from: Option<Wrapper<NaiveDateTime>>,
    to: Option<Wrapper<NaiveDateTime>>,
) -> Result<Option<Json<models::RatingHistoryChartData>>, LuffaError> {
    conn.run(|conn| {
        if let Some(data) = models::RatingHistory::get_chart_data(
            conn,
            &player_ids.into_inner(),
            from.map(|x| x.into_inner()),
            to.map(|x| x.into_inner()),
        )? {
            Ok(Some(Json(data)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[get("/player/<player_id>")]
pub async fn player(conn: DbConn, player_id: i32) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_player = models::Player::get_by_id(conn, player_id)?;
        if let Some(player) = maybe_player {
            let ratings = models::Rating::get_all(conn)?;
            let profile = player.get_profile(conn, PLAYER_PROFILE_MAX_ROWS, &ratings)?;
            Ok(Some(templates::player_page(&player, &profile, &ratings)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[get("/player/<player_id>/videos")]
pub async fn player_videos(conn: DbConn, player_id: i32) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_player = models::Player::get_by_id(conn, player_id)?;
        if let Some(player) = maybe_player {
            let videos = models::MapVideo::get_for_player(conn, player_id)?;
            Ok(Some(templates::player_videos(&player, &videos)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[get("/player/<player_id>/maps?<order_field>&<order_dir>")]
pub async fn player_maps(
    conn: DbConn,
    player_id: i32,
    order_field: Option<PlayerMapsField>,
    order_dir: Option<OrderDirection>,
) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_player = models::Player::get_by_id(conn, player_id)?;
        let ord = match (order_field, order_dir) {
            (Some(f), Some(d)) => PlayerMapsOrdering {
                maps_field: f,
                order_direction: d,
            },
            _ => PlayerMapsOrdering {
                maps_field: PlayerMapsField::Timestamp,
                order_direction: OrderDirection::Desc,
            },
        };
        if let Some(player) = maybe_player {
            let maps = player.get_maps(conn, Some(ord))?;
            Ok(Some(templates::player_maps_page(&player, &maps, ord)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[derive(Debug, PartialEq, FromForm)]
pub struct MapFilter<'r> {
    name: Option<&'r str>,
    page_size: i64,
    page_number: i64,
}

impl<'r> Default for MapFilter<'r> {
    fn default() -> Self {
        Self {
            page_size: 20,
            name: None,
            page_number: 0,
        }
    }
}

#[get("/maps")]
pub async fn maps(conn: DbConn) -> Result<Markup, LuffaError> {
    maps_with_filter(conn, Default::default()).await
}

#[get("/maps?<filter>")]
pub async fn maps_with_filter(conn: DbConn, filter: MapFilter<'_>) -> Result<Markup, LuffaError> {
    let filter_name = filter.name.map(String::from);
    conn.run(move |conn| {
        let total_maps = models::Map::total_maps(conn, filter_name.as_deref())?;
        let maps = models::Map::get_all(
            conn,
            filter_name.as_deref(),
            filter.page_number,
            filter.page_size,
        )?;
        Ok(templates::maps_page(
            &maps,
            filter_name.as_deref(),
            filter.page_number,
            filter.page_size,
            (total_maps + filter.page_size - 1) / filter.page_size,
        ))
    })
    .await
}

// #[get("/beta/maps")]
// pub async fn maps_beta(conn: DbConn) -> Result<Markup, LuffaError> {
//     conn.run(move |conn| {
//         let maps = models::Map::get_all(conn)?;
//         Ok(templates::beta::maps_page(&maps))
//     })
//     .await
// }

#[get("/versus")]
pub fn versus(_conn: DbConn) -> Markup {
    templates::versus_page()
}

#[get("/versus?<player1>&<player2>")]
pub async fn versus_do(
    conn: DbConn,
    player1: i32,
    player2: i32,
) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_player1 = models::Player::get_by_id(conn, player1)?;
        let maybe_player2 = models::Player::get_by_id(conn, player2)?;
        if let (Some(player1), Some(player2)) = (maybe_player1, maybe_player2) {
            if player1.id == player2.id {
                Ok(Some(templates::versus_same_player(player1)))
            } else {
                let comparison = player1.compare(conn, &player2)?;
                Ok(Some(templates::versus_table(
                    &player1,
                    &player2,
                    &comparison,
                )))
            }
        } else {
            Ok(None)
        }
    })
    .await
}

pub struct Wrapper<T>(T);

impl<T> Wrapper<T> {
    fn into_inner(self) -> T {
        self.0
    }
}

impl<'v> FromFormField<'v> for Wrapper<Vec<i32>> {
    fn from_value(form_value: ValueField<'v>) -> form::Result<Wrapper<Vec<i32>>> {
        let val: &str = form_value.value;
        let mut res = Vec::new();
        for i in val.split(',') {
            let maybe_id: Result<i32, _> = i.trim().parse();
            match maybe_id {
                Ok(id) => res.push(id),
                Err(_) => return Err(Errors::new()),
            }
        }
        Ok(Wrapper(res))
    }
}

impl<'v> FromFormField<'v> for Wrapper<NaiveDateTime> {
    fn from_value(form_value: ValueField<'v>) -> form::Result<Wrapper<NaiveDateTime>> {
        let val: &str = form_value.value;
        match NaiveDateTime::parse_from_str(val, "%Y-%m-%d %H:%M:%S") {
            Ok(parsed) => Ok(Wrapper(parsed)),
            Err(_) => Err(Errors::new()),
        }
    }
}

impl<'v> FromFormField<'v> for OrderDirection {
    fn from_value(form_value: ValueField<'v>) -> form::Result<OrderDirection> {
        let val: &str = form_value.value;
        Ok(match val {
            "asc" => OrderDirection::Asc,
            "desc" => OrderDirection::Desc,
            _ => return Err(Errors::new()),
        })
    }
}

impl<'v> FromFormField<'v> for PlayerMapsField {
    fn from_value(form_value: ValueField<'v>) -> form::Result<PlayerMapsField> {
        use PlayerMapsField::*;
        let val: &str = form_value.value;
        Ok(match val {
            "map_name" => MapName,
            "reltime" => Reltime,
            "created_at" => Timestamp,
            // "score" => Score,
            "pos" => Pos,
            "time" => Time,
            _ => return Err(Errors::new()),
        })
    }
}
