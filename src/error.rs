use thiserror::Error;

use rocket::http::ContentType;
use rocket::http::Status;
use rocket::request::Request;
use rocket::response::{self, Responder, Response};

#[derive(Error, Debug)]
pub enum LuffaError {
    #[error("failure: {0}")]
    Failure(#[from] anyhow::Error),
    #[error("DB error {0}")]
    DbError(#[from] diesel::result::Error),
    #[error("parse error")]
    ParseError,
}

impl<'r> Responder<'r, 'static> for LuffaError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let string = format!("ERROR: {}", self);
        Ok(Response::build_from(string.respond_to(req)?)
            .header(ContentType::new("text", "plain"))
            .status(Status::InternalServerError)
            .finalize())
    }
}
