// @generated automatically by Diesel CLI.

diesel::table! {
    admin_log (id) {
        id -> Int4,
        created_at -> Timestamp,
        #[max_length = 64]
        op -> Varchar,
        user_id -> Int4,
        description -> Text,
    }
}

diesel::table! {
    demo_file (id) {
        id -> Int4,
        map_id -> Int4,
        player_id -> Int4,
        time -> Numeric,
        #[max_length = 512]
        demo_file_path -> Varchar,
    }
}

diesel::table! {
    heatmap_history (id) {
        id -> Int4,
        player_id -> Int4,
        created_at -> Timestamp,
        data -> Jsonb,
    }
}

diesel::table! {
    map (id) {
        id -> Int4,
        #[max_length = 256]
        name -> Varchar,
        haste -> Bool,
        slick -> Bool,
        vq3_compat -> Bool,
        nexrun -> Bool,
        rocket -> Bool,
        mortar -> Bool,
        crylink -> Bool,
        hagar -> Bool,
        hook -> Bool,
        fireball -> Bool,
        nex -> Bool,
        electro -> Bool,
        blaster -> Bool,
        mine_layer -> Bool,
        sg -> Bool,
        mg -> Bool,
        max_pos -> Int2,
        server_id -> Nullable<Int4>,
        rating_sum -> Int4,
        rating_votes -> Int4,
    }
}

diesel::table! {
    map_image (id) {
        id -> Int4,
        map_id -> Int4,
        #[sql_name = "type"]
        #[max_length = 16]
        type_ -> Varchar,
        #[max_length = 1024]
        url -> Varchar,
    }
}

diesel::table! {
    map_update (id) {
        id -> Int4,
        map_id -> Int4,
        server_id -> Int4,
        deleted -> Bool,
        created_at -> Timestamp,
    }
}

diesel::table! {
    map_video (id) {
        id -> Int4,
        map_id -> Int4,
        player_id -> Int4,
        time -> Numeric,
        record_created_at -> Timestamp,
        video_created_at -> Timestamp,
        render_success -> Bool,
        #[max_length = 64]
        youtube_code -> Nullable<Varchar>,
        is_hidden -> Bool,
    }
}

diesel::table! {
    player (id) {
        id -> Int4,
        stats_id -> Nullable<Int4>,
        #[max_length = 256]
        nickname -> Varchar,
        #[max_length = 256]
        nickname_nocolors -> Varchar,
        created_at -> Timestamp,
    }
}

diesel::table! {
    player_key (id) {
        id -> Int4,
        player_id -> Int4,
        #[max_length = 256]
        crypto_idfp -> Varchar,
        created_at -> Timestamp,
        last_used_at -> Timestamp,
    }
}

diesel::table! {
    player_nickname (id) {
        id -> Int4,
        player_id -> Int4,
        #[max_length = 256]
        nickname -> Varchar,
        #[max_length = 256]
        nickname_nocolors -> Varchar,
        created_at -> Timestamp,
        last_used_at -> Timestamp,
    }
}

diesel::table! {
    rating (id) {
        id -> Int4,
        #[max_length = 64]
        name -> Varchar,
        servers -> Array<Nullable<Int4>>,
        max_pos -> Int4,
    }
}

diesel::table! {
    rating_history (rating_id, player_id, created_at) {
        player_id -> Int4,
        created_at -> Timestamp,
        score -> Numeric,
        map_count -> Int4,
        pos -> Int4,
        max_pos -> Int4,
        maps -> Nullable<Jsonb>,
        rating_id -> Int4,
    }
}

diesel::table! {
    rating_map (rating_id, time_record_id) {
        rating_id -> Int4,
        time_record_id -> Int4,
        score -> Numeric,
        last_updated -> Timestamp,
    }
}

diesel::table! {
    rating_total (rating_id, player_id) {
        rating_id -> Int4,
        player_id -> Int4,
        score -> Numeric,
        map_count -> Int4,
        pos -> Int4,
    }
}

diesel::table! {
    results_change (id) {
        id -> Int4,
        results_changeset_id -> Int4,
        player_id -> Int4,
        new_time -> Nullable<Numeric>,
        old_time -> Nullable<Numeric>,
        new_pos -> Nullable<Int2>,
        old_pos -> Nullable<Int2>,
    }
}

diesel::table! {
    results_changeset (id) {
        id -> Int4,
        map_id -> Int4,
        created_at -> Timestamp,
        max_pos -> Int2,
        best_time -> Numeric,
        server_id -> Int4,
    }
}

diesel::table! {
    server (id) {
        id -> Int4,
        #[max_length = 256]
        name -> Varchar,
        ip_address -> Inet,
        #[max_length = 256]
        rcon_password -> Varchar,
        port -> Int4,
        active -> Bool,
        #[max_length = 512]
        server_db_path -> Varchar,
    }
}

diesel::table! {
    speed_record (id) {
        id -> Int4,
        player_id -> Int4,
        speed -> Numeric,
        created_at -> Timestamp,
        map_id -> Int4,
    }
}

diesel::table! {
    speed_record_change (id) {
        id -> Int4,
        results_changeset_id -> Int4,
        new_player_id -> Int4,
        old_player_id -> Nullable<Int4>,
        new_speed -> Numeric,
        old_speed -> Nullable<Numeric>,
    }
}

diesel::table! {
    time_record (id) {
        id -> Int4,
        player_id -> Int4,
        time -> Numeric,
        created_at -> Timestamp,
        map_id -> Int4,
        pos -> Int2,
        reltime -> Numeric,
        checkpoints -> Nullable<Array<Nullable<Numeric>>>,
        #[max_length = 256]
        nickname -> Nullable<Varchar>,
        #[max_length = 256]
        nickname_nocolors -> Nullable<Varchar>,
        player_key_id -> Nullable<Int4>,
        server_id -> Nullable<Int4>,
        ip_address -> Nullable<Inet>,
        top_speed -> Nullable<Float8>,
        average_speed -> Nullable<Float8>,
        start_speed -> Nullable<Float8>,
        strafe_percentage -> Nullable<Float8>,
        checkpoint_house_synced_at -> Nullable<Timestamp>,
        checkpoint_house_timestamp -> Nullable<Timestamp>,
    }
}

diesel::table! {
    user (id) {
        id -> Int4,
        #[max_length = 128]
        username -> Varchar,
        #[max_length = 512]
        email -> Nullable<Varchar>,
        player_id -> Int4,
        is_admin -> Bool,
    }
}

diesel::joinable!(admin_log -> user (user_id));
diesel::joinable!(demo_file -> map (map_id));
diesel::joinable!(demo_file -> player (player_id));
diesel::joinable!(heatmap_history -> player (player_id));
diesel::joinable!(map -> server (server_id));
diesel::joinable!(map_image -> map (map_id));
diesel::joinable!(map_update -> map (map_id));
diesel::joinable!(map_update -> server (server_id));
diesel::joinable!(map_video -> map (map_id));
diesel::joinable!(map_video -> player (player_id));
diesel::joinable!(player_key -> player (player_id));
diesel::joinable!(player_nickname -> player (player_id));
diesel::joinable!(rating_history -> player (player_id));
diesel::joinable!(rating_history -> rating (rating_id));
diesel::joinable!(rating_map -> rating (rating_id));
diesel::joinable!(rating_map -> time_record (time_record_id));
diesel::joinable!(rating_total -> player (player_id));
diesel::joinable!(rating_total -> rating (rating_id));
diesel::joinable!(results_change -> player (player_id));
diesel::joinable!(results_change -> results_changeset (results_changeset_id));
diesel::joinable!(results_changeset -> map (map_id));
diesel::joinable!(results_changeset -> server (server_id));
diesel::joinable!(speed_record -> map (map_id));
diesel::joinable!(speed_record -> player (player_id));
diesel::joinable!(speed_record_change -> results_changeset (results_changeset_id));
diesel::joinable!(time_record -> map (map_id));
diesel::joinable!(time_record -> player (player_id));
diesel::joinable!(time_record -> player_key (player_key_id));
diesel::joinable!(user -> player (player_id));

diesel::allow_tables_to_appear_in_same_query!(
    admin_log,
    demo_file,
    heatmap_history,
    map,
    map_image,
    map_update,
    map_video,
    player,
    player_key,
    player_nickname,
    rating,
    rating_history,
    rating_map,
    rating_total,
    results_change,
    results_changeset,
    server,
    speed_record,
    speed_record_change,
    time_record,
    user,
);
