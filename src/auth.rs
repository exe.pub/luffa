use models::*;
use rocket::request::{FromRequest, Outcome};

impl<'a, 'r> FromRequest<'a, 'r> for User {}
