use super::error::LuffaError;
use super::models;
use super::templates;
use super::DbConn;
use maud::Markup;
use rocket::form::{Form, FromForm};
use rocket::get;
use rocket::response::Redirect;
use rocket::serde::json::Json;
use serde::Serialize;

#[derive(Serialize)]
pub struct PlayerAutocomplete {
    id: i32,
    nickname: String,
    nickname_nocolors: String,
    keys: Vec<String>,
    nicks: Vec<String>,
}

#[derive(FromForm)]
pub struct ExcludeIds(Vec<i32>);

// impl<'v> FromFormField<'v> for ExcludeIds {
//     fn from_value(form_value: form::ValueField<'v>) -> form::Result<ExcludeIds> {
//         let s: &str = &form_value.percent_decode_lossy();
//         let mut res = Vec::new();
//         for i in s.split(',') {
//             if let Ok(id) = i.parse() {
//                 res.push(id);
//             }
//         }
//         Ok(ExcludeIds(res))
//     }
// }

#[get("/players.json?<exclude_ids>")]
pub async fn players_json(
    conn: DbConn,
    exclude_ids: Option<ExcludeIds>,
) -> Result<Json<Vec<PlayerAutocomplete>>, LuffaError> {
    conn.run(move |conn| {
        let players = models::Player::admin_list(conn, None)?;
        let res = players
            .into_iter()
            .filter(|(p, _, _)| {
                if let Some(e_ids) = &exclude_ids {
                    !e_ids.0.contains(&p.id)
                } else {
                    true
                }
            })
            .map(|(p, ks, ns)| PlayerAutocomplete {
                id: p.id,
                nickname: templates::format_dpstring(&p.nickname, true).into_string(),
                nickname_nocolors: p.nickname_nocolors,
                keys: ks.into_iter().map(|k| k.crypto_idfp).collect(),
                nicks: ns
                    .into_iter()
                    .map(|n| templates::format_dpstring(&n.nickname, true).into_string())
                    .collect(),
            })
            .collect();
        Ok(Json(res))
    })
    .await
}

#[get("/admin")]
pub async fn player_index(conn: DbConn) -> Result<Markup, LuffaError> {
    conn.run(|conn| {
        let players = models::Player::admin_list(conn, None)?;
        Ok(templates::admin_players::player_index_page(&players))
    })
    .await
}

#[get("/admin/player/<player_id>")]
pub async fn player_manage(conn: DbConn, player_id: i32) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_player = models::Player::get_admin_profile(conn, player_id)?;
        if let Some(player) = maybe_player {
            Ok(Some(templates::admin_players::player_page(&player)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[get("/admin/player/merge?<first_player_id>&<second_player_id>")]
pub async fn player_merge(
    conn: DbConn,
    first_player_id: i32,
    second_player_id: i32,
) -> Result<Option<Markup>, LuffaError> {
    conn.run(move |conn| {
        let maybe_first = models::Player::get_admin_profile(conn, first_player_id)?;
        let maybe_second = models::Player::get_admin_profile(conn, second_player_id)?;
        if let (Some(first), Some(second)) = (maybe_first, maybe_second) {
            Ok(Some(templates::admin_players::merge_page(&first, &second)))
        } else {
            Ok(None)
        }
    })
    .await
}

#[derive(FromForm)]
pub struct MergeForm {
    first_player_id: i32,
    second_player_id: i32,
}

#[post("/admin/player/merge", data = "<data>")]
pub async fn player_merge_post(
    conn: DbConn,
    data: Form<MergeForm>,
) -> Result<Option<Redirect>, LuffaError> {
    conn.run(move |conn| {
        let maybe_first = models::Player::get_by_id(conn, data.first_player_id)?;
        let maybe_second = models::Player::get_by_id(conn, data.second_player_id)?;
        if let (Some(first), Some(second)) = (maybe_first, maybe_second) {
            first.merge_tran(conn, &second)?;
            Ok(Some(Redirect::to(uri!(player_index))))
        } else {
            Ok(None)
        }
    })
    .await
}
