use anyhow::{bail, Error};
use luffa::{create_rocket, models, DbConn};
use std::collections::HashMap;
use xonotic_db::server_db::{CryptoIDFP, Map, SpeedRecord, TimeRecord};
use xonotic_db::ServerDB;

fn process_dump(conn: &mut diesel::PgConnection) -> Result<(), Error> {
    let mut db = ServerDB::empty();
    let maps = models::Map::get_all(conn, None, 10000000, 0)?;
    let speed_recs = models::SpeedRecord::load_all_records(conn)?;
    let mut players: HashMap<i32, (String, String)> = HashMap::new();
    for (p, keys, nicks) in models::Player::admin_list(conn, None)? {
        players.insert(
            p.id,
            (keys[0].crypto_idfp.clone(), nicks[0].nickname.clone()),
        );
    }
    for m in maps {
        let map = Map(m.map.name.clone());
        if let Some(speed_rec) = speed_recs.get(&m.map.id) {
            let (key, nick) = players.get(&speed_rec.player_id).unwrap();
            db.insert_speed_record(
                nick,
                map.clone(),
                SpeedRecord {
                    crypto_idfp: CryptoIDFP(key.clone()),
                    speed: speed_rec.speed,
                },
            );
        }
        let time_records = models::TimeRecord::get_for_map(conn, &m.map)?;
        for tr in time_records {
            let (key, nick) = players.get(&tr.player.id).unwrap();
            db.insert_record(
                nick,
                map.clone(),
                TimeRecord {
                    pos: tr.time_record.pos as u16,
                    time: tr.time_record.time,
                    crypto_idfp: CryptoIDFP(key.clone()),
                },
            );
        }
    }
    db.save(format!(
        "server-db-dump-{:?}.db",
        std::time::SystemTime::now()
    ))?;
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket);
    let conn = match maybe_conn.await {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(|conn| {
        process_dump(conn).map_err(|e| {
            eprintln!("Error importing database: {e:?}");
            // eprintln!("{:?}", e.backtrace());
            e
        })
    })
    .await
    .unwrap();
    Ok(())
}
