use anyhow::{bail, Error};
use luffa::models;
use luffa::{create_rocket, DbConn};
use pk3rs::{GameType, MapInfo, PK3};
use std::collections::HashMap;
use std::fs::read_to_string;
use std::path::PathBuf;

use structopt::StructOpt;
use walkdir::WalkDir;

#[derive(StructOpt, Debug)]
#[structopt()]
/// imports map metadata (tags and levelshots) from pk3 + mapinfo collection
struct Opt {
    #[structopt(short = "p", long = "pk3-dir", parse(from_os_str))]
    pk3_dir: PathBuf,

    #[structopt(short = "m", long = "mapinfo-dir", parse(from_os_str))]
    mapinfo_dir: Option<PathBuf>,
}

fn main_inner(opt: &Opt, conn: &mut diesel::PgConnection) -> Result<(), Error> {
    let mut mapinfo: HashMap<String, MapInfo> = HashMap::new();
    if let Some(ref md) = opt.mapinfo_dir {
        for i in WalkDir::new(md) {
            let entry = i?;
            let path = entry.path();
            if let (Some(stem), Some(ext)) = (path.file_stem(), path.extension()) {
                if ext == "mapinfo" {
                    let map_name = stem.to_string_lossy().into_owned();
                    let data = read_to_string(path)?;
                    let mi = MapInfo::parse(&data)?;
                    mapinfo.insert(map_name, mi);
                }
            }
        }
    }
    for i in WalkDir::new(&opt.pk3_dir) {
        let entry = i?;
        let path = entry.path();
        if let Some(ext) = path.extension() {
            if ext == "pk3" {
                let pk3 = PK3::from_path(&path)?;
                match pk3.into_maplist() {
                    Ok(maplist) => {
                        for mut map in maplist {
                            if let Some(m) = models::Map::get_by_name(conn, &map.name)? {
                                if let Some(mi) = mapinfo.remove(&map.name) {
                                    map.override_mapinfo(mi)
                                }
                                let tags = map.get_tags(GameType::CTS);
                                let maybe_levelshot = map.get_levelshot();
                                m.set_tags(conn, tags)?;
                                if let Some(levelshot) = maybe_levelshot {
                                    println!("found levelshot for map {}", map.name);
                                    m.set_levelshot(conn, levelshot, false)?;
                                }
                            }
                        }
                    }
                    Err(e) => {
                        println!(
                            "Warning: broken pk3 file {}: {:?}",
                            entry.path().display(),
                            e
                        );
                    }
                }
            }
        }
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    if !opt.pk3_dir.is_dir() {
        bail!("Error: pk3 directory doesn't exist or isn't a directory")
    }
    if let Some(ref md) = opt.mapinfo_dir {
        if !md.is_dir() {
            bail!("Error: mapinfo directory doesn't exist or ")
        }
    }
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(move |conn| {
        main_inner(&opt, conn).unwrap();
    })
    .await;
    Ok(())
}
