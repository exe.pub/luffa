use anyhow::{bail, Error};
use luffa::models;
use luffa::{create_rocket, DbConn};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt()]
/// imports map metadata (tags and levelshots) from pk3 + mapinfo collection
struct Opt {
    #[structopt(short = "s", long = "is-speed-change")]
    is_speed_change: bool,
    #[structopt(short = "c", long = "change-id")]
    change_id: i32
}

fn main_inner(opt: &Opt, conn: &mut diesel::PgConnection) -> Result<(), Error> {
    if opt.is_speed_change {
        models::ResultsChangeset::revert_speed_change(conn, opt.change_id)?;
    } else {
        models::ResultsChangeset::revert_change(conn, opt.change_id)?;
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(move |conn| {
        main_inner(&opt, conn).unwrap();
    })
    .await;
    Ok(())
}
