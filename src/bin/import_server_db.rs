use anyhow::{bail, Error};
use diesel::connection::Connection;
use luffa::{create_rocket, models, DbConn};
use rust_decimal::Decimal;
use std::collections::{HashMap, HashSet};
use std::path::Path;
use xonotic_db::{server_db::CryptoIDFP, ServerDB};

// #[derive(Debug)]
// enum LockStatus {
//     Free,
//     Locked,
// }

// fn get_lock_status(server: &models::Server) -> Result<LockStatus, Error> {
//     let lock_path = PathBuf::from(server.server_db_path.clone() + ".lock");
//     let buf = read_to_string(&lock_path)?;
//     if buf == "LOCKED\n" {
//         Ok(LockStatus::Locked)
//     } else if buf == "UNLOCKED\n" {
//         Ok(LockStatus::Free)
//     } else {
//         bail!("Invalid lock file")
//     }
// }

fn get_db_records(
    conn: &mut diesel::PgConnection,
    maps: &HashMap<String, models::Map>,
    players: &mut HashMap<String, (models::Player, HashSet<String>, HashSet<String>)>,
    server_db: &ServerDB,
) -> Result<
    (
        HashMap<String, models::history::Results>,
        HashMap<(i32, Decimal), CryptoIDFP>, // (player_id, speed) -> crypto_idfp
        HashMap<(i32, Decimal), CryptoIDFP>, // (player_id, time) -> crypto_idfp
    ),
    Error,
> {
    let mut res: HashMap<String, models::history::Results> = HashMap::new();
    let mut speed_to_crypto_idfp = HashMap::new();
    let mut time_to_crypto_idfp = HashMap::new();
    for (map, db_time_records) in server_db.time_records.iter() {
        if maps.contains_key(&map.0) {
            let speed_record = server_db.speed_records.get(map);
            let speed_record = if let Some(sr) = speed_record {
                let nickname = server_db.players.get(&sr.crypto_idfp).map(|x| x.0.as_str());
                let (player, _keys, _nicks) =
                    models::Player::get_or_create(conn, players, &sr.crypto_idfp.0, nickname)?;
                if player.id != 7629 {
                    println!("IGNORING ph speed record cause he is a cheater");
                    speed_to_crypto_idfp.insert((player.id, sr.speed), sr.crypto_idfp.clone());
                    Some((player.id, sr.speed))
                } else {
                    None
                }
            } else {
                None
            };
            let mut time_records = Vec::new();
            let mut players_on_this_map = HashSet::new(); // avoid merged duplicates
            for time_record in db_time_records.iter().filter_map(|x| x.as_ref()) {
                let nickname = server_db
                    .players
                    .get(&time_record.crypto_idfp)
                    .map(|x| x.0.as_str());
                let (player, _keys, _nicks) = models::Player::get_or_create(
                    conn,
                    players,
                    &time_record.crypto_idfp.0,
                    nickname,
                )?;
                if player.id == 7629 {
                    println!("IGNORING ph time record cause he is a cheater");
                    continue;
                }
                if !players_on_this_map.contains(&player.id) {
                    time_records.push((player.id, 0, time_record.time));
                    time_to_crypto_idfp.insert(
                        (player.id, time_record.time),
                        time_record.crypto_idfp.clone(),
                    );
                    players_on_this_map.insert(player.id);
                }
            }
            let mut results = models::history::Results {
                speed_record,
                time_records,
            };
            results.recalc_positions();
            res.insert(map.0.clone(), results);
        }
    }
    Ok((res, speed_to_crypto_idfp, time_to_crypto_idfp))
}

fn read_server_db(path: &Path) -> Result<ServerDB, Error> {
    for _i in 0..10 {
        match ServerDB::new(path) {
            Ok(db) => {
                return Ok(db);
            }
            Err(_) => {
                std::thread::sleep(std::time::Duration::from_secs(1));
            }
        }
    }
    bail!("Could not read server db at {}", path.display())
}

fn process_server(
    conn: &mut diesel::PgConnection,
    server: models::Server,
) -> Result<Vec<i32>, Error> {
    let mut res = Vec::new();
    let mut all_players = models::Player::load_all_players(conn)?;
    let mut db_files = Vec::new();
    for maybe_entry in std::fs::read_dir(dbg!(&server.server_db_path))? {
        let entry = maybe_entry?;
        let file_type = entry.file_type()?;
        if file_type.is_file() {
            db_files.push(dbg!(entry.path()));
        }
    }
    dbg!(&db_files);
    let mut db = read_server_db(&db_files[0])?;
    let mut other_dbs = Vec::new();
    for i in db_files.iter().skip(1) {
        other_dbs.push(read_server_db(i)?);
    }
    db.merge_with_other_dbs(&other_dbs);
    let mut active_maps = models::Map::get_full_for_server(conn, server.id)?;
    let (website_records, mut map_to_time_record) =
        models::TimeRecord::load_all_records_into_results(conn)?;
    let (mut db_records, speed_to_crypto_idfp, time_to_crypto_idfp) =
        get_db_records(conn, &active_maps, &mut all_players, &db)?;

    for (i, m) in active_maps.drain() {
        if let Some(db_recs) = db_records.remove(&i) {
            let changes = db_recs.compute_changes(website_records.get(&i));
            if !changes.is_empty() {
                res.push(m.id);
                let best_time = db_recs.time_records[0].2;
                let current_time_records = map_to_time_record.remove(&i).unwrap_or_default();
                let updated_map = models::TimeRecord::save_results(
                    conn,
                    &m,
                    &db_recs,
                    current_time_records,
                    &db,
                    &speed_to_crypto_idfp,
                    &time_to_crypto_idfp,
                )?;
                models::ResultsChangeset::save_changeset(
                    conn,
                    server.id,
                    &updated_map,
                    &changes,
                    best_time,
                )?;
            }
        }
    }
    Ok(res)
}

fn process_import(conn: &mut diesel::PgConnection) -> Result<(), Error> {
    let servers = models::Server::get_active(conn)?;
    let ratings = models::Rating::get_all(conn)?;
    let mut include_maps = models::Rating::get_outdated_maps(conn)?;
    for server in servers {
        let changed_maps = process_server(conn, server)?;
        include_maps.extend_from_slice(&changed_maps);
        dbg!(&include_maps);
    }
    for rating in ratings.iter() {
        rating.update(conn, &include_maps)?;
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    // let tb = rayon::ThreadPoolBuilder::new();
    // tb.num_threads(2).build_global()?;
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = maybe_conn.expect("Could not acquire DB connection");
    conn.run(|conn| {
        conn.transaction(|c| {
            process_import(c).map_err(|e| {
                eprintln!("Error importing database: {:?}", e);
                // eprintln!("{:?}", e.backtrace());
                e
            })
        })
    })
    .await?;
    Ok(())
}
