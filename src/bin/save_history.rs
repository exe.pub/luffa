use anyhow::{bail, Error};
use diesel::connection::Connection;
use luffa::models;
use luffa::{create_rocket, DbConn};

fn process_history(conn: &mut diesel::PgConnection) -> Result<(), Error> {
    models::HeatmapHistory::save(conn)?;
    models::RatingHistory::save(conn)?;
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(|conn| {
        conn.transaction(|c| {
            process_history(c).map_err(|e| {
                println!("Error importing database");
                // println!("{:?}", e.backtrace());
                e
            })
        })
    })
    .await?;
    Ok(())
}
