use anyhow::{anyhow, bail, Error};
use lazy_static::lazy_static;
use luffa::{create_rocket, models, DbConn};
use ofiles::opath;
use regex::bytes::Regex;
use rust_decimal::{prelude::FromStr, Decimal, RoundingStrategy};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use structopt::StructOpt;
use walkdir::WalkDir;

#[derive(StructOpt, Debug)]
struct Opt {
    source_directory: PathBuf,
    target_directory: PathBuf,
    trash_directory: PathBuf,
}

#[derive(Debug)]
struct DemoInfo {
    map_name: String,
    crypto_idfp: String,
    marker_time: f64,
    time: Decimal,
    time_f64: f64,
}

impl DemoInfo {
    fn timecode_start(&self) -> f64 {
        self.marker_time - 3.0
    }

    fn timecode_end(&self) -> f64 {
        self.marker_time + self.time_f64 + 1.0
    }
}

fn extract_map_name(file_name: &str) -> Option<String> {
    if let Some((left, _)) = file_name.rsplit_once('_') {
        if let Some((left, _)) = left.rsplit_once('_') {
            if let Some((_, right)) = left.split_once('_') {
                if let Some((_, right)) = right.split_once('_') {
                    return Some(String::from(right));
                }
            }
        }
    }
    None
}

fn get_demo_info(path: &Path) -> Result<Option<(Vec<u8>, DemoInfo)>, Error> {
    lazy_static! {
        static ref FIRST_PLACE_RE: Regex =
            Regex::new("CTS RECORD SET (\\S+) (\\S+) (\\S+) 1 (.*?)(\n|\0)").unwrap();
    }
    let raw_data = std::fs::read(path)?;
    let demo_file_name = path
        .file_name()
        .ok_or_else(|| anyhow!("demo without file name!!!!"))?;
    if let Some(map_name) = extract_map_name(&demo_file_name.to_string_lossy()) {
        println!("{map_name}");
        let mut result: Option<DemoInfo> = None;
        for caps in FIRST_PLACE_RE.captures_iter(&raw_data) {
            println!(
                "{}",
                String::from_utf8_lossy(caps.get(0).unwrap().as_bytes())
            );
            let crypto_idfp = String::from_utf8_lossy(caps.get(4).unwrap().as_bytes()).to_string();
            if crypto_idfp == "ANONYMOUS" {
                return Ok(None);
            }
            let marker_time_str = String::from_utf8_lossy(caps.get(2).unwrap().as_bytes());
            let marker_time: f64 = marker_time_str.parse()?;
            let time_str = String::from_utf8_lossy(caps.get(3).unwrap().as_bytes());
            let time = Decimal::from_str(&time_str)?;
            let time = time.round_dp_with_strategy(2, RoundingStrategy::MidpointAwayFromZero);
            let time_f64: f64 = time_str.parse()?;
            if let Some(mut di) = result.take() {
                if di.time > time {
                    di.time = time;
                    di.time_f64 = time_f64;
                    di.marker_time = marker_time;
                    result.replace(di);
                }
            } else {
                result.replace(DemoInfo {
                    crypto_idfp,
                    map_name: map_name.clone(),
                    marker_time,
                    time,
                    time_f64,
                });
            }
        }
        Ok(result.map(|x| (raw_data, x)))
    } else {
        bail!("could not find map name in demo: {}", path.display());
    }
}

// fn embed_capture_markers(demo_data: &[u8], info: &DemoInfo) -> Result<Vec<u8>, Error> {
//     Ok(Vec::new())
// }

fn compress_xz(input_path: &Path, output_path: &Path) -> Result<(), Error> {
    let data = std::fs::read(input_path)?;
    let compressed_data = lzma::compress(&data, 6)?;
    std::fs::write(output_path, compressed_data)?;
    Ok(())
}

// fn decompress_xz(input_path: &Path, output_path: &Path) -> Result<(), Error> {
// Ok(())
// }

fn run_demotc(path: &Path, info: &DemoInfo, dir: &Path) -> Result<PathBuf, Error> {
    let path_display = format!("{}", path.display());
    let tmpfile = tempfile::NamedTempFile::new()?;
    let output_file_name = format!(
        "{}---{}---{}.dem.xz",
        info.map_name,
        info.time,
        get_unix_timestamp()
    );
    let output_path = dir.join(output_file_name);
    let tmpfile_path_display = format!("{}", &tmpfile.path().display());
    let timecode_start = format!("{}", info.timecode_start());
    let timecode_end = format!("{}", info.timecode_end());
    run_demotc_helper(&[
        "cut",
        &path_display,
        &tmpfile_path_display,
        &timecode_start,
        &timecode_end,
        "--capture",
    ])?;
    compress_xz(tmpfile.path(), &output_path)?;
    Ok(output_path)
}

fn move_to_trash(path: &Path, dir: &Path) -> Result<(), Error> {
    let data = std::fs::read(path)?;
    let file_name = path.file_name().unwrap();
    let target_path = dir.join(file_name);
    std::fs::write(target_path, data)?;
    Ok(())
}

fn store_demo_info_in_db(conn: &mut diesel::PgConnection, info: &DemoInfo, path: &Path) -> Result<(), Error> {
    let map = models::Map::get_by_name(conn, &info.map_name)?
        .ok_or_else(|| anyhow!("no map {}", info.map_name))?;
    let player = models::Player::get_by_key(conn, &info.crypto_idfp)?
        .ok_or_else(|| anyhow!("no player {}", info.crypto_idfp))?;
    models::DemoFile::store_new(
        conn,
        map.id,
        player.id,
        info.time,
        format!("{}", path.display()),
    )?;
    Ok(())
}

fn process_demo_file(conn: &mut diesel::PgConnection, opt: &Opt, path: &Path) -> Result<(), Error> {
    let pids = opath(path);
    if let Ok(pids) = pids {
        if pids.is_empty() {
            let maybe_demo_info = get_demo_info(path)?;
            match maybe_demo_info {
                Some((_, di)) => {
                    println!("Found demo info: {di:?}");
                    let output_path = run_demotc(path, &di, &opt.target_directory)?;
                    store_demo_info_in_db(conn, &di, &output_path)?;
                }
                None => move_to_trash(path, &opt.trash_directory)?,
            }
            std::fs::remove_file(path)?;
        } else {
            eprintln!("Demo file is being used by pids: {pids:?}, ignoring");
        }
    }
    Ok(())
}

fn process_demo_files(conn: &mut diesel::PgConnection, opt: Opt) -> Result<(), Error> {
    for maybe_entry in WalkDir::new(&opt.source_directory) {
        let entry = maybe_entry?;
        if entry.file_type().is_file() {
            let maybe_ext = entry.path().extension();
            if let Some(ext) = maybe_ext {
                if ext == "dem" {
                    match process_demo_file(conn, &opt, entry.path()) {
                        Ok(()) => {
                            println!("Demo file {} processed correctly", entry.path().display())
                        }
                        Err(e) => println!("Demo file {} errored: {}", entry.path().display(), e),
                    }
                }
            }
        }
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(|conn| {
        process_demo_files(conn, opt).expect("failed to process demos");
    })
    .await;

    Ok(())
}

static DEMOTC_SOURCE: &str = r#"use strict;
use warnings;

print "@ARGV";

sub sanitize($)
{
	my ($str) = @_;
	$str =~ y/\000-\037//d;
	return $str;
}

# opening the files

my ($in, $out, $tc0, $tc1, $pattern, $capture);

my $mode = shift @ARGV;
$mode = 'help' if not defined $mode;

if($mode eq 'grep' && @ARGV == 2)
{
	$in = $ARGV[0];
	$pattern = $ARGV[1];
}
elsif($mode eq 'uncut' && @ARGV == 2)
{
	$in = $ARGV[0];
	$out = $ARGV[1];
}
elsif($mode eq 'cut' && (@ARGV == 4 || @ARGV == 5))
{
	$in = $ARGV[0];
	$out = $ARGV[1];
	$tc0 = $ARGV[2];
	$tc1 = $ARGV[3];
	$capture = (@ARGV == 5);
}
else
{
	die "Usage: $0 cut infile outfile tc_start tc_end [--capture], or $0 uncut infile outfile, or $0 grep infile pattern\n"
}

if($mode ne 'grep')
{
	$in ne $out
		or die "Input and output file may not be the same!";
}

open my $infh, "<", $in
	or die "open $in: $!";
binmode $infh;

my $outfh;
if($mode ne 'grep') # cutting
{
	open $outfh, ">", $out
		or die "open $out: $!";
	binmode $outfh;
}

# 1. CD track

$/ = "\012";
my $cdtrack = <$infh>;
print $outfh $cdtrack if $mode ne 'grep';

# 2. packets

my $tc = undef;

my $first = 1;
my $demo_started = 0;
my $demo_stopped = 0;
my $inject_buffer = "";

use constant DEMOMSG_CLIENT_TO_SERVER => 0x80000000;
for(;;)
{
	last
		unless 4 == read $infh, my $length, 4;
	$length = unpack("V", $length);
	if($length & DEMOMSG_CLIENT_TO_SERVER)
	{
		# client-to-server packet
		$length = $length & ~DEMOMSG_CLIENT_TO_SERVER;
		die "Invalid demo packet"
			unless 12 == read $infh, my $angles, 12;
		die "Invalid demo packet"
			unless $length == read $infh, my($data), $length;

		next if $mode eq 'grep';
		print $outfh pack("V", length($data) | DEMOMSG_CLIENT_TO_SERVER);
		print $outfh $angles;
		print $outfh $data;
		next;
	}
	die "Invalid demo packet"
		unless 12 == read $infh, my $angles, 12;
	die "Invalid demo packet"
		unless $length == read $infh, my($data), $length;
	
	# remove existing cut marks
	$data =~ s{^\011\n//CUTMARK\n[^\0]*\0}{};
	
	if(substr($data, 0, 1) eq "\007") # svc_time
	{
		$tc = unpack "f", substr $data, 1, 4;
	}

	if($mode eq 'cut' && defined $tc)
	{
		if($first)
		{
			$inject_buffer = "\011\n//CUTMARK\nslowmo 100\n\000";
			$first = 0;
		}
		if($demo_started < 1 && $tc > $tc0 - 50)
		{
			$inject_buffer = "\011\n//CUTMARK\nslowmo 10\n\000";
			$demo_started = 1;
		}
		if($demo_started < 2 && $tc > $tc0 - 5)
		{
			$inject_buffer = "\011\n//CUTMARK\nslowmo 1\n\000";
			$demo_started = 2;
		}
		if($demo_started < 3 && $tc > $tc0)
		{
			if($capture)
			{
				$inject_buffer = "\011\n//CUTMARK\ncl_capturevideo 1\n\000";
			}
			else
			{
				$inject_buffer = "\011\n//CUTMARK\nslowmo 0; defer 1 \"slowmo 1\"\n\000";
			}
			$demo_started = 3;
		}
		if(!$demo_stopped && $tc > $tc1)
		{
			if($capture)
			{
				$inject_buffer = "\011\n//CUTMARK\ncl_capturevideo 0; defer 0.5 \"quit\"\n\000";
			}
			else
			{
				$inject_buffer = "\011\n//CUTMARK\ndefer 0.5 \"quit\"\n\000";
			}
			$demo_stopped = 1;
		}
	}
	elsif($mode eq 'grep')
	{
		if(my @l = ($data =~ /$pattern/))
		{
			if(defined $tc)
			{
				print "$tc:";
			}
			else
			{
				print "start:";
			}
			for(@l)
			{
				print " \"", sanitize($_), "\"";
			}
			print "\n";
		}
	}

	next if $mode eq 'grep';
	if(length($inject_buffer . $data) < 65536)
	{
		$data = $inject_buffer . $data;
		$inject_buffer = "";
	}
	print $outfh pack("V", length $data);
	print $outfh $angles;
	print $outfh $data;
}

close $outfh if $mode ne 'grep';
close $infh;
"#;

fn run_demotc_helper(args: &[&str]) -> Result<(), Error> {
    println!("Running demotc with args: {args:?}");
    let mut cmd = Command::new("perl");
    cmd.args(["--", "-"]);
    cmd.args(args);
    cmd.stdin(Stdio::piped());
    let mut child = cmd.spawn()?;
    {
        let stdin = child.stdin.as_mut().unwrap();
        stdin.write_all(DEMOTC_SOURCE.as_bytes())?;
    }
    let output = child.wait_with_output()?;
    if output.status.success() {
        Ok(())
    } else {
        bail!(
            "demotc failed with status: {}\n output: {}\n error: {}",
            output.status,
            String::from_utf8_lossy(&output.stdout),
            String::from_utf8_lossy(&output.stderr)
        )
    }
}

fn get_unix_timestamp() -> f64 {
    use std::time::SystemTime;
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs_f64()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract_map_name() {
        let file_name = "2021-07-08_08-34_char4n-caja_7_206-55-183-234-62225.dem";
        let extracted = extract_map_name(file_name).unwrap();
        assert!(dbg!(extracted) == *"char4n-caja");
    }
}
