use std::path::{Path, PathBuf};

use anyhow::{bail, Error};
use chrono::NaiveDateTime;
use clap::{Parser, Subcommand};
use diesel::prelude::*;
use diesel::PgConnection;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use rocket::fs::NamedFile;
use rocket::{get, routes, Build, Rocket};
use rust_decimal::Decimal;

use luffa::create_rocket;
use luffa::models::{
    Map, ResultsChange, ResultsChangeset, SpeedRecord, SpeedRecordChange, TimeRecord,
};
use luffa::schema::{
    map, rating_map, results_change, results_changeset, speed_record, speed_record_change,
    time_record,
};
use luffa::{admin, DbConn};
use luffa::{controllers, models};

#[derive(Parser, Debug)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Server,
    Migrate,
    FixRelTimes,
    #[command(arg_required_else_help = true)]
    DeleteRecord {
        player_id: i32,
        map: String,
    },
}

#[get("/static/<file..>")]
async fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).await.ok()
}

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations");

async fn get_single_conn(rocket_build: Rocket<Build>) -> Result<DbConn, Error> {
    let rocket = rocket_build.ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    match maybe_conn {
        Some(conn) => Ok(conn),
        None => bail!("Can't aquire database connection"),
    }
}

fn delete_record(conn: &mut PgConnection, player_id: i32, map: &str) -> Result<(), Error> {
    let map = models::Map::get_by_name(conn, map)?.expect("No such map {map}");
    let player = models::Player::get_by_id(conn, player_id)?;
    let cheater_time_record = time_record::table
        .filter(time_record::player_id.eq(player_id))
        .filter(time_record::map_id.eq(map.id))
        .first::<TimeRecord>(conn)?;
    let changesets = results_changeset::table
        .filter(results_changeset::map_id.eq(map.id))
        .order_by(results_changeset::created_at.asc())
        .load::<ResultsChangeset>(conn)?;
    let mut cheater_time: Option<Decimal> = None;
    let mut best_time: Option<Decimal> = None;
    let mut has_cheater = false;
    let mut top_speed: Option<Decimal> = None;
    let mut top_speed_holder: Option<i32> = None;
    let mut top_speed_created_at: Option<NaiveDateTime> = None;
    for changeset in changesets.iter() {
        let speed_changes = speed_record_change::table
            .filter(speed_record_change::results_changeset_id.eq(changeset.id))
            .load::<SpeedRecordChange>(conn)?;
        for speed_change in speed_changes {
            if speed_change.new_player_id != player_id {
                top_speed = Some(speed_change.new_speed);
                top_speed_holder = Some(speed_change.new_player_id);
                top_speed_created_at = Some(changeset.created_at);
            } else if speed_change.old_player_id.is_some()
                && (speed_change.old_player_id != Some(player_id))
            {
                top_speed = speed_change.old_speed;
                top_speed_holder = speed_change.old_player_id;
                top_speed_created_at = Some(changeset.created_at); // wrong, but should never happen
            }
            if speed_change.new_player_id == player_id {
                diesel::delete(
                    speed_record_change::table.filter(speed_record_change::id.eq(speed_change.id)),
                )
                .execute(conn)?;
            } else if speed_change.old_player_id == Some(player_id) {
                diesel::update(
                    speed_record_change::table.filter(speed_record_change::id.eq(speed_change.id)),
                )
                .set((
                    speed_record_change::old_player_id.eq(top_speed_holder),
                    speed_record_change::old_speed.eq(top_speed),
                ))
                .execute(conn)?;
            }
        }
        let changes = results_change::table
            .filter(results_change::results_changeset_id.eq(changeset.id))
            .load::<ResultsChange>(conn)?;
        let mut new_cheater_time = cheater_time;
        for change in changes.iter() {
            if change.player_id == player_id {
                has_cheater = true;
                new_cheater_time = change.new_time;
                println!("CURTIME {cheater_time:?}");
            }
        }
        for change in changes.iter() {
            if let (Some(t), Some(new_pos), Some(new_time)) =
                (new_cheater_time, change.new_pos, change.new_time)
            {
                if new_time > t {
                    println!(
                        "Change {change:?} -> {t} -> new_pos from {} to {}",
                        new_pos,
                        new_pos - 1
                    );
                    diesel::update(results_change::table.filter(results_change::id.eq(change.id)))
                        .set(results_change::new_pos.eq(new_pos - 1))
                        .execute(conn)?;
                }
            }
            if let (Some(t), Some(old_pos), Some(old_time)) =
                (cheater_time, change.old_pos, change.old_time)
            {
                if old_time > t {
                    println!(
                        "Change {change:?} -> {t} -> old_pos from {} to {}",
                        old_pos,
                        old_pos - 1
                    );
                    diesel::update(results_change::table.filter(results_change::id.eq(change.id)))
                        .set(results_change::old_pos.eq(old_pos - 1))
                        .execute(conn)?;
                }
            }
        }
        if has_cheater {
            diesel::update(results_changeset::table.filter(results_changeset::id.eq(changeset.id)))
                .set(results_changeset::max_pos.eq(changeset.max_pos - 1))
                .execute(conn)?;
        }
        if Some(changeset.best_time) == new_cheater_time {
            if let Some(t) = best_time {
                diesel::update(
                    results_changeset::table.filter(results_changeset::id.eq(changeset.id)),
                )
                .set(results_changeset::best_time.eq(t))
                .execute(conn)?;
            }
        } else {
            best_time = Some(changeset.best_time);
        }
        // delete cheater change
        for change in changes.iter() {
            if change.player_id == player_id {
                diesel::delete(change).execute(conn)?;
            }
        }

        let changes = results_change::table
            .filter(results_change::results_changeset_id.eq(changeset.id))
            .load::<ResultsChange>(conn)?;
        let speed_changes = speed_record_change::table
            .filter(speed_record_change::results_changeset_id.eq(changeset.id))
            .load::<SpeedRecordChange>(conn)?;
        // check if changeset is empty
        if changes.is_empty() && speed_changes.is_empty() {
            println!("deleting empty changeset {changeset:?}");
            diesel::delete(changeset).execute(conn)?;
        }
        cheater_time = new_cheater_time;
    }
    diesel::delete(rating_map::table.filter(rating_map::time_record_id.eq(cheater_time_record.id)))
        .execute(conn)?;
    diesel::delete(time_record::table.filter(time_record::id.eq(cheater_time_record.id)))
        .execute(conn)?;
    let other_records = time_record::table
        .filter(time_record::map_id.eq(map.id))
        .load::<TimeRecord>(conn)?;
    for rec in other_records {
        if rec.time > cheater_time_record.time {
            diesel::update(time_record::table.filter(time_record::id.eq(rec.id)))
                .set(time_record::pos.eq(rec.pos - 1))
                .execute(conn)?;
        }
    }

    if let Some(srec) = speed_record::table
        .filter(speed_record::map_id.eq(map.id))
        .first::<SpeedRecord>(conn)
        .optional()?
    {
        if srec.player_id == player_id {
            if let (Some(top_speed), Some(top_speed_holder), Some(ts)) =
                (top_speed, top_speed_holder, top_speed_created_at)
            {
                diesel::update(speed_record::table.filter(speed_record::id.eq(srec.id)))
                    .set((
                        speed_record::speed.eq(top_speed),
                        speed_record::player_id.eq(top_speed_holder),
                        speed_record::created_at.eq(ts),
                    ))
                    .execute(conn)?;
            } else {
                diesel::delete(speed_record::table.filter(speed_record::id.eq(srec.id)))
                    .execute(conn)?;
            }
        }
    }

    println!("Player id {player:?} {map:?}");
    Ok(())
}
#[rocket::main]
async fn main() -> Result<(), Error> {
    let rocket = create_rocket();
    let cli = Cli::parse();
    match cli.command {
        Commands::Server => {
            rocket
                .mount(
                    "/",
                    routes![
                        files,
                        controllers::index,
                        controllers::map,
                        controllers::rating,
                        controllers::rating_specific,
                        controllers::rating_history_data,
                        controllers::player,
                        controllers::player_maps,
                        controllers::player_videos,
                        controllers::maps,
                        controllers::maps_with_filter,
                        controllers::versus,
                        controllers::versus_do,
                        admin::player_index,
                        admin::player_manage,
                        admin::players_json,
                        admin::player_merge,
                        admin::player_merge_post
                    ],
                )
                .launch()
                .await?;
        }
        Commands::Migrate => {
            let conn = get_single_conn(rocket).await?;
            conn.run(|conn| match conn.run_pending_migrations(MIGRATIONS) {
                Ok(_) => {
                    println!("Successfully completed migrations");
                }
                Err(e) => {
                    println!("Error running migrations: {e}");
                }
            })
            .await;
        }
        Commands::DeleteRecord { player_id, map } => {
            let conn = get_single_conn(rocket).await?;
            conn.run(move |conn| match delete_record(conn, player_id, &map) {
                Ok(()) => {
                    println!("OK!")
                }
                Err(e) => {
                    println!("Error {e} :(")
                }
            })
            .await;
        }
        Commands::FixRelTimes => {
            let conn = get_single_conn(rocket).await?;
            conn.run(move |conn| match fix_rel_times(conn) {
                Ok(()) => {
                    println!("OK!")
                }
                Err(e) => {
                    println!("Error {e} :(")
                }
            })
            .await;
        }
    }
    Ok(())
}

fn fix_rel_times(conn: &mut PgConnection) -> Result<(), Error> {
    let maps = Map::get_all(conn, None, 1000000, 0)?;
    for map in maps {
        println!("Processing map {}", map.map.name);
        let time_records = time_record::table
            .filter(time_record::map_id.eq(map.map.id))
            .order_by(time_record::time.asc())
            .load::<TimeRecord>(conn)?;
        if time_records.is_empty() {
            continue;
        }
        let mut best_time = None;
        let mut second_best_time = None;
        for tr in time_records.iter() {
            if best_time.is_none() {
                best_time.replace(tr.time);
            } else if second_best_time.is_none() {
                second_best_time.replace(tr.time);
                break;
            }
        }
        let mut curpos = 1;
        let mut curtime = time_records[0].time;
        for tr in time_records.iter() {
            if tr.time > curtime {
                curpos += 1;
                curtime = tr.time;
            }
            if let Some(best_time) = best_time {
                let reltime = if curpos == 1 {
                    tr.time / second_best_time.unwrap_or(best_time)
                } else {
                    tr.time / best_time
                };
                diesel::update(time_record::table.filter(time_record::id.eq(tr.id)))
                    .set((
                        time_record::pos.eq(curpos),
                        time_record::reltime.eq(reltime),
                    ))
                    .execute(conn)?;
            }
        }
        println!("{} maxpos -> {}", map.map.name, curpos);
        diesel::update(map::table.filter(map::id.eq(map.map.id)))
            .set(map::max_pos.eq(curpos))
            .execute(conn)?;
    }
    Ok(())
}
