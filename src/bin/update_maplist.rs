use anyhow::{bail, Error};
use diesel::connection::Connection;
use luffa::{create_rocket, models, DbConn};
use std::collections::HashSet;
use structopt::StructOpt;
use tokio::time::sleep;
use xagy::{RconCmdItem, RconConnection};

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short, long)]
    no_news: bool,
}

async fn maplist_receiver(server: &models::Server) -> Result<HashSet<String>, Error> {
    let remote = server.get_rcon_remote();
    let (connection, mut receiver) = RconConnection::connect(remote).await?;
    connection.send("update_maps").await?;
    sleep(std::time::Duration::from_secs(3)).await;
    connection.send("g_maplist").await?;
    sleep(std::time::Duration::from_secs(3)).await;
    let mut items = Vec::new();
    loop {
        match receiver.try_recv() {
            Ok(i) => items.push(i),
            _ => break,
        }
    }
    let mut res = None;
    for i in items {
        match i {
            RconCmdItem::Cvar(n, v) => {
                if n == "g_maplist" {
                    let maplist: HashSet<String> = v
                        .split(' ')
                        .map(|x| String::from(x).replace("^7", ""))
                        .collect();
                    res.replace(maplist);
                } else {
                    continue;
                }
            }
            _ => continue,
        }
    }
    match res {
        Some(v) => Ok(v),
        None => bail!("Could not get maplist for server {}", server.name),
    }
}

fn process_update_maplist(
    conn: &mut diesel::PgConnection,
    no_news: bool,
    server: models::Server,
    server_maps: HashSet<String>,
) -> Result<(), Error> {
    if server_maps.is_empty() {
        eprintln!("Empty maplist from server {}, skipping", server.name);
        return Ok(());
    }
    println!(
        "Got maplist for server {}: {} maps",
        server.name,
        server_maps.len()
    );
    let current_maps = models::Map::get_for_server(conn, server.id)?;
    let symdiff: HashSet<_> = server_maps
        .symmetric_difference(&current_maps)
        .cloned()
        .collect();
    println!("CURRENT: {:?}", current_maps);
    println!("SYMDIFF {:?}", symdiff);
    let added_maps: HashSet<_> = symdiff.intersection(&server_maps).collect();
    let deleted_maps: HashSet<_> = symdiff.intersection(&current_maps).collect();
    println!("ADDED {:?}", added_maps);
    println!("DELETED {:?}", deleted_maps);
    for i in added_maps.into_iter() {
        let map = models::Map::get_or_create(conn, i)?;
        if !no_news {
            models::MapUpdate::insert_new(conn, map.id, server.id, false)?;
        }
        map.set_server_id(conn, Some(server.id))?;
    }
    for i in deleted_maps.into_iter() {
        let map = models::Map::get_or_create(conn, i)?;
        if !no_news {
            models::MapUpdate::insert_new(conn, map.id, server.id, true)?;
        }
        map.set_server_id(conn, None)?;
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    let servers = conn
        .run(|conn| models::Server::get_active(conn).expect("Can't get list of servers"))
        .await;
    for server in servers.into_iter() {
        let maps = maplist_receiver(&server)
            .await
            .expect("Can't get map list from server");
        let no_news = opt.no_news;
        conn.run(move |conn| {
            conn.transaction(|c| {
                process_update_maplist(c, no_news, server, maps).map_err(|e| {
                    eprintln!("Error updating maplist: {:?}", e);
                    // eprintln!("{:?}", e.backtrace());
                    e
                })
            })
        })
        .await
        .unwrap();
    }
    Ok(())
}
