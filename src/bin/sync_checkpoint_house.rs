use std::path::PathBuf;
use std::str::FromStr;

use anyhow::{bail, Error};
use chrono::NaiveDateTime;
use diesel::ExpressionMethods;
use diesel::RunQueryDsl;
use dpcolors::DPString;
use ipnetwork::IpNetwork;
use luffa::models;
use luffa::schema::time_record;
use luffa::util::utcnow;
use luffa::{create_rocket, DbConn};
use rusqlite::{Connection, OpenFlags, OptionalExtension};
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt()]
struct Opt {
    #[structopt(long = "checkpoint-house-db")]
    checkpoint_house_db: PathBuf,
}

fn main_inner(
    _opt: &Opt,
    conn: &mut diesel::PgConnection,
    sqlite_conn: Connection,
) -> Result<(), Error> {
    let records_to_process =
        models::TimeRecord::get_records_not_synced_with_checkpoint_house(conn)?;
    for (record, player, map) in records_to_process {
        let keys = player.get_keys(conn)?;
        let keys_values: Vec<_> = keys.clone().into_iter().map(|x| x.crypto_idfp).collect();
        if let Some(r) =
            get_checkpoint_house_record(&sqlite_conn, record.time, &map.name, &keys_values)?
        {
            let mut key_id = None;
            for k in keys.iter() {
                if k.crypto_idfp == r.crypto_idfp {
                    key_id = Some(k.id);
                    break;
                }
            }
            let now = utcnow();
            println!("{:?}", r);
            diesel::update(&record)
                .set((
                    time_record::checkpoints.eq(r.checkpoints),
                    time_record::nickname_nocolors.eq(DPString::parse(&r.nickname).to_none()),
                    time_record::nickname.eq(r.nickname),
                    time_record::player_key_id.eq(key_id),
                    time_record::ip_address.eq(r.ip),
                    time_record::server_id.eq(r.server_id),
                    time_record::top_speed.eq(r.top_speed),
                    time_record::average_speed.eq(r.average_speed),
                    time_record::start_speed.eq(r.start_speed),
                    time_record::strafe_percentage.eq(r.strafe_percentage),
                    time_record::checkpoint_house_timestamp.eq(r.timestamp),
                    time_record::checkpoint_house_synced_at.eq(now),
                ))
                .execute(conn)?;
        }
    }
    Ok(())
}

#[derive(Debug)]
struct CheckpointHouseRecord {
    nickname: String,
    crypto_idfp: String,
    server_id: i32,
    ip: IpNetwork,
    timestamp: NaiveDateTime,
    checkpoints: Vec<Option<Decimal>>,
    top_speed: f64,
    start_speed: f64,
    average_speed: f64,
    strafe_percentage: f64,
}

fn get_checkpoint_house_record(
    conn: &Connection,
    time: Decimal,
    map: &str,
    player_keys: &[String],
) -> Result<Option<CheckpointHouseRecord>, Error> {
    let time_i = (time * dec!(100)).to_string();
    let query = format!(
        r#"SELECT nickname,crypto_idfp,ip,server_id,timestamp,cp_times,top_speed,start_speed,average_speed,strafe_percentage
FROM record
WHERE time=? AND map=? AND crypto_idfp IN ({})"#,
        player_keys
            .iter()
            .map(|x| format!("'{}'", x))
            .collect::<Vec<_>>()
            .join(",")
    );
    let mut stmt = conn.prepare(&query)?;
    let record = stmt.query_row([time_i, String::from(map)], |r| {
        let mut checkpoints = [Decimal::ZERO; 256];
        let raw_cp = r.get::<_, String>(5)?;
        for cp in raw_cp.split(';') {
            if let Some((cp_ix, cp_time)) = cp.split_once(' ') {
                let cp_ix: usize = cp_ix.parse().unwrap();
                let cp_time = Decimal::from_str(cp_time).unwrap() / dec!(100);
                checkpoints[cp_ix - 1] = cp_time;
            }
        }
        let nickname = r.get::<_, String>(0)?.trim_end_matches('\0').to_string();
        Ok(CheckpointHouseRecord {
            nickname,
            crypto_idfp: r.get(1)?,
            ip: IpNetwork::from_str(&r.get::<_, String>(2)?).unwrap(),
            server_id: r.get(3)?,
            timestamp: NaiveDateTime::from_timestamp(r.get::<_, i64>(4)?, 0),
            top_speed: r.get(6)?,
            start_speed: r.get(7)?,
            average_speed: r.get(8)?,
            strafe_percentage: r.get(9)?,
            checkpoints: checkpoints
                .into_iter()
                .map(|c| if c == dec!(0) { None } else { Some(c) })
                .collect(),
        })
    });
    Ok(record.optional()?)
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let sqlite_conn =
        Connection::open_with_flags(&opt.checkpoint_house_db, OpenFlags::SQLITE_OPEN_READ_ONLY)?;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(move |conn| {
        main_inner(&opt, conn, sqlite_conn).unwrap();
    })
    .await;
    Ok(())
}
