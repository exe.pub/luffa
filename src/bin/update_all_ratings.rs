use anyhow::{bail, Error};
use diesel::connection::Connection;
use luffa::{create_rocket, models, DbConn};

fn process_update(conn: &mut diesel::PgConnection) -> Result<(), Error> {
    let ratings = models::Rating::get_all(conn)?;
    for rating in ratings.iter() {
        println!("RATING {:?}", rating);
        let start = std::time::Instant::now();
        rating.update_all(conn)?;
        println!("RATING {:?} TOOK {}", rating, start.elapsed().as_secs());
    }
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    // let tb = rayon::ThreadPoolBuilder::new();
    // tb.num_threads(4).build_global()?;
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    for i in 0..1 {
        println!("ITERATION {}", i);
        let start = std::time::Instant::now();
        conn.run(|conn| {
            conn.transaction(|c| {
                process_update(c).map_err(|e| {
                    eprintln!("Error processing update: {:?}", e);
                    // eprintln!("{:?}", e.backtrace());
                    e
                })
            })
        })
        .await.unwrap();
        println!("ITERATION TOOK {}", start.elapsed().as_secs())
    }
    Ok(())
}
