use anyhow::{anyhow, bail, Error};
use dpcolors::DPString;
use duct::cmd;
use luffa::watermark::*;
use luffa::{create_rocket, models, DbConn};
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use structopt::StructOpt;
use tempfile::NamedTempFile;

#[derive(StructOpt, Debug, Clone)]
struct Opt {
    #[structopt(short, long)]
    map_name: Option<String>,
    #[structopt(short, long)]
    xonoticdir: PathBuf,
    #[structopt(short, long)]
    userdir: PathBuf,
    #[structopt(short, long)]
    outro: PathBuf,
    #[structopt(short, long)]
    target_dir: PathBuf,
    #[structopt(short, long)]
    demo_path: PathBuf,
}

fn decompress_xz(demo_path: &Path, input_path: &Path, output_path: &Path) -> Result<(), Error> {
    let p = demo_path.join(input_path.file_name().unwrap());
    let data = std::fs::read(&p)?;
    let decompressed_data = lzma::decompress(&data)?;
    std::fs::write(output_path, decompressed_data)?;
    Ok(())
}

#[derive(Debug)]
struct RecordMetadata {
    map_name: String,
    nickname: String,
    youtube_title: String,
    youtube_description: String,
    time: Decimal,
    rating: Decimal,
    demo_file_path: PathBuf,
    time_record: models::TimeRecord,
}

impl RecordMetadata {
    fn from_db(conn: &mut diesel::PgConnection, opt: &Opt) -> Result<Self, Error> {
        let (d, m, p, tr) = if let Some(ref mn) = opt.map_name {
            models::DemoFile::get_for_map(conn, mn)?
        } else {
            models::DemoFile::get_oldest_first_without_vid(conn)?
        };
        let youtube_title = format!(
            "[XDF] {}: {} - {} | Xonotic",
            m.name,
            p.nickname_nocolors.replace(['>', '<'], ""),
            format_time(d.time)
        );
        let youtube_description = format!(
            "Run by {} on {}: {} (score: {})\nView all records on: https://xdf.gg/map/{}\nDownload xonotic at: https://xonotic.org/\nJoin servers Relaxed Running and Hardcore Parkour to play!\nDiscord: https://discord.gg/v447EHx\nIRC: #clanexe at quakenet.org\n",
            p.nickname_nocolors.replace(['>', '<'], ""),
            m.name,
            format_time(d.time),
            dec!(0),
            // tr.score
            //     .round_dp_with_strategy(0, rust_decimal::RoundingStrategy::MidpointAwayFromZero),
            m.name
        );
        // let youtube_description = format!(
        //     "Run by {} on {}: {} (score: {})\n",
        //     p.nickname_nocolors,
        //     m.name,
        //     format_time(t),
        //     rating.round_dp_with_strategy(0, rust_decimal::RoundingStrategy::MidpointAwayFromZero),
        // );
        Ok(Self {
            map_name: m.name,
            nickname: p.nickname,
            time: d.time,
            youtube_title,
            youtube_description,
            rating: dec!(0), // tr.score,
            demo_file_path: PathBuf::from(d.demo_file_path),
            time_record: tr,
        })
    }

    fn make_watermark(&self) -> Result<NamedTempFile, Error> {
        let wm = Watermark {
            map: &self.map_name,
            nickname: DPString::parse(&self.nickname),
            time: self.time,
            rating: self.rating,
        };
        let image = wm.gen_image()?;
        let tmpfile = tempfile::Builder::new().suffix(".png").tempfile()?;

        image.save(&tmpfile.path())?;
        Ok(tmpfile)
    }

    fn render_raw_video(&self, opt: &Opt) -> Result<PathBuf, Error> {
        let demo_file_name = self
            .demo_file_path
            .file_name()
            .ok_or_else(|| anyhow!("Missing demo file name: {}", self.demo_file_path.display()))?;
        let demopath = opt.userdir.join("data/demos");
        let demopath = demopath.join(&demo_file_name);
        println!("Before decompress");
        decompress_xz(&opt.demo_path, &self.demo_file_path, &demopath)?;
        println!("After decompress {}", opt.xonoticdir.display());
        cmd!(
            "/Users/morosophos/Xonotic/Xonotic.app/Contents/MacOS/xonotic-osx-sdl",
            "-userdir",
            &format!("{}", opt.userdir.display()),
            "+cl_capturevideo_number",
            "1",
            "-simsound",
            "+developer",
            "1",
            "+playdemo",
            &format!("demos/{}", demo_file_name.to_string_lossy())
        )
        .dir(&opt.xonoticdir)
        .run()?;
        println!("After render");
        // Command::new("./xonotic-linux64-sdl")
        //     .current_dir(&opt.xonoticdir)
        //     .stdin(Stdio::null())
        //     .args(&[
        //         "./xonotic-linux64-sdl",
        //         "-userdir",
        //         &format!("{}", opt.userdir.display()),
        //         "+cl_capturevideo_number",
        //         "1",
        //         "-simsound",
        //         "+playdemo",
        //         &format!("demos/{}", demo_file_name.to_string_lossy()),
        //     ])
        //     .status()?;
        std::fs::remove_file(&demopath)?;
        Ok(opt.userdir.join("data/video/dpvideo001.avi"))
    }

    fn transcode_video(
        &self,
        conn: &mut diesel::PgConnection,
        opt: &Opt,
        raw_video: &Path,
        watermark: &NamedTempFile,
    ) -> Result<(), Error> {
        // let tmpfile = tempfile::Builder::new().suffix(".mp4").tempfile()?;
        // let now = utcnow().format("%Y-%m-%d-%H-%M-%S").to_string();
        let demo_file_name = self
            .demo_file_path
            .file_name()
            .ok_or_else(|| anyhow!("Missing demo file name: {}", self.demo_file_path.display()))?
            .to_string_lossy()
            .to_string();
        let cut_start = if demo_file_name.starts_with("rrdemo-") {
            "2"
        } else {
            "0"
        };
        let meta_file_name = demo_file_name.clone();
        let demo_file_name = format!("{}.mp4", demo_file_name);
        let meta_file_name = format!("{}.txt", meta_file_name);
        let mut cmd = Command::new("ffmpeg");
        cmd.stdin(Stdio::null());
        cmd.args(vec![
            "-y",
            "-threads",
            "0", // "1",
            "-i",
            &format!("{}", raw_video.display()),
            "-i",
            &format!("{}", watermark.path().display()),
            "-i",
            &format!("{}", opt.outro.display()),
            "-filter_complex",
            &format!(
                "[0:v][1:v] overlay={}:{} [wm];[wm] [0:a] [2:v] [2:a] concat=n=2:v=1:a=1",
                WATERMARK_OFFSET_LEFT, WATERMARK_OFFSET_TOP
            ),
            "-ss",
            cut_start,
            "-pix_fmt",
            "yuv420p",
            "-c:v",
            "libx264",
            "-crf",
            "21",
            // "-b:v",
            // "12000k",
            "-profile:v",
            "high",
            "-r",
            "60",
            "-force_key_frames",
            "expr:gte(t,n_forced/2)",
            "-g",
            "30",
            "-s",
            "2560x1440",
            "-sws_flags",
            "lanczos+full_chroma_inp",
            "-preset",
            "slow",
            "-flags:v",
            "+cgop",
            "-bf",
            "2",
            "-coder",
            "1",
            // "-crf",
            // "18",
            "-c:a",
            "aac",
            "-q:a",
            "1",
            "-ac",
            "2",
            "-ar",
            "48000",
            "-use_editlist",
            "0",
            "-movflags",
            "+faststart",
            &format!("/tmp/{}", demo_file_name), // &format!("{}", tmpfile.path().display()),
        ]);
        println!("{:?}", cmd);
        cmd.status()?;
        let mut cmd = Command::new("mv");
        cmd.args(vec![
            format!("/tmp/{}", demo_file_name).as_str(),
            &format!("{}", opt.target_dir.join(&demo_file_name).display()),
        ]);
        println!("{:?}", cmd);
        cmd.status()?;
        let mut f = std::fs::File::create(&opt.target_dir.join(meta_file_name))?;
        writeln!(&mut f, "{}", self.youtube_title)?;
        writeln!(
            &mut f,
            "Description:\n====\n{}\n====",
            self.youtube_description
        )?;
        writeln!(&mut f, "Playlist: XDF records")?;
        writeln!(&mut f, "xdf,xonotic,cts,defrag")?;
        writeln!(&mut f, "Not for kids")?;
        writeln!(&mut f, "Category: gaming")?;
        writeln!(&mut f, "Game: xonotic 2011")?;
        writeln!(&mut f, "Privacy: public")?;
        models::MapVideo::insert_new(
            conn,
            self.time_record.map_id,
            self.time_record.player_id,
            self.time,
            self.time_record.created_at,
        )?;
        Ok(())
    }

    fn _upload_video(&self, path: &NamedTempFile) -> Result<(), Error> {
        let mut cmd = Command::new("/home/nick/.virtualenvs/luffa/bin/youtube-upload");
        cmd.args(vec![
            format!("--title={}", self.youtube_title).as_str(),
            &format!("--description={}", self.youtube_description),
            "--tags=xdf,xonotic,defrag,cts",
            "--privacy=public",
            "--playlist=XDF records",
            &format!("{}", path.path().display()),
        ]);
        println!("{:?}", cmd);
        cmd.status()?;
        Ok(())
    }
}

fn process(conn: &mut diesel::PgConnection, opt: &Opt) -> Result<(), Error> {
    let metadata = RecordMetadata::from_db(conn, opt)?;
    println!("{:?}", metadata);
    let watermark = metadata.make_watermark()?;
    println!("{:?}", watermark);
    let raw_video = metadata.render_raw_video(opt)?;
    let _transcoded_video = metadata.transcode_video(conn, opt, &raw_video, &watermark)?;
    // metadata.upload_video(&transcoded_video)?;
    println!("{:?}", metadata);
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    match maybe_conn {
        Some(conn) => {
            let count = if opt.map_name.is_some() { 1 } else { 15 };
            // let count = 1;
            for _i in 0..count {
                let opt_clone = opt.clone();
                conn.run(move |conn| {
                    process(conn, &opt_clone).unwrap_or_else(|e| println!("CANT PROCESS: {:?}", e));
                })
                .await;
            }
            Ok(())
        }
        None => bail!("Can't aquire database connection"),
    }
}
