use anyhow::{bail, Error};
use diesel::connection::Connection;
use luffa::models;
use luffa::{create_rocket, DbConn};
use xonotic_db::server_db;

fn process_merge(conn: &mut diesel::PgConnection) -> Result<(), Error> {
    println!("here");
    let dup_db = models::PlayerKey::load_dup_db(conn)?;
    println!("Dub len: {}", dup_db.len());
    let mut db = server_db::ServerDB::new("server.db")?;
    println!("{:?}", db.players);
    db.deduplicate(&dup_db);
    db.save("server.db.fixed").unwrap();
    Ok(())
}

#[rocket::main]
async fn main() -> Result<(), Error> {
    let rocket = create_rocket().ignite().await?;
    let maybe_conn = DbConn::get_one(&rocket).await;
    let conn = match maybe_conn {
        Some(conn) => conn,
        None => bail!("Can't aquire database connection"),
    };
    conn.run(|conn| {
        conn.transaction(|c| {
            process_merge(c).map_err(|e| {
                println!("Error merging players");
                // println!("{:?}", e.backtrace());
                e
            })
        })
    })
    .await?;
    Ok(())
}
