use std::fmt;
use rocket::UriDisplayQuery;

#[derive(Debug, Clone, Copy, PartialEq, UriDisplayQuery)]
pub enum OrderDirection {
    Asc,
    Desc,
}

impl fmt::Display for OrderDirection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            OrderDirection::Asc => write!(f, "asc"),
            OrderDirection::Desc => write!(f, "desc")
        }
    }
}

impl OrderDirection {
    pub fn opposite(self) -> OrderDirection {
        match self {
            OrderDirection::Asc => OrderDirection::Desc,
            OrderDirection::Desc => OrderDirection::Asc,
        }
    }
}

macro_rules! ord {
    ($query:expr, $x:expr, $y:expr) => {{
        match $y {
            OrderDirection::Asc => $query.order($x.asc()),
            OrderDirection::Desc => $query.order($x.desc()),
        }
    }};
}

macro_rules! then_ord {
    ($query:expr, $x:expr, $y:expr) => {{
        match $y {
            OrderDirection::Asc => $query.then_order_by($x.asc()),
            OrderDirection::Desc => $query.then_order_by($x.desc()),
        }
    }};
}

#[derive(Debug, Clone, Copy, PartialEq, UriDisplayQuery)]
pub enum PlayerMapsField {
    MapName,
    Timestamp,
    Pos,
    Time,
    Reltime,
    // Score,
}

impl fmt::Display for PlayerMapsField {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use PlayerMapsField::*;
        match self {
            MapName => write!(f, "map_name"),
            Timestamp => write!(f, "timestamp"),
            Pos => write!(f, "pos"),
            Time => write!(f, "time"),
            Reltime => write!(f, "reltime")
        }
    }
}

#[derive(Debug, Clone, Copy, UriDisplayQuery)]
pub struct PlayerMapsOrdering {
    pub maps_field: PlayerMapsField,
    pub order_direction: OrderDirection,
}

macro_rules! player_maps_ord {
    ($query:expr, $x:expr) => {{
        use PlayerMapsField::*;
        let bq = $query.into_boxed();
        match $x.maps_field {
            MapName => ord!(bq, map::name, $x.order_direction),
            Pos => {
                let sub_bq = ord!(bq, time_record::pos, $x.order_direction);
                then_ord!(sub_bq, map::max_pos, $x.order_direction.opposite())
            }
            Timestamp => ord!(bq, time_record::created_at, $x.order_direction),
            Time => ord!(bq, time_record::time, $x.order_direction),
            Reltime => ord!(bq, time_record::reltime, $x.order_direction),
            // Score => ord!(bq, time_record::score, $x.1),
        }
    }};
}

impl Default for PlayerMapsOrdering {
    fn default() -> Self {
        PlayerMapsOrdering {
            maps_field: PlayerMapsField::Timestamp,
            order_direction: OrderDirection::Desc,
        }
    }
}
