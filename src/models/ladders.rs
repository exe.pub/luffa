use super::constants::INTEGRATION_PRECISION;
use crate::util::f64_to_decimal;
use num_traits::cast::ToPrimitive;
use rayon::prelude::*;
use rust_decimal::prelude::*;
use rust_decimal_macros::dec;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt;
use std::ops::{Add, Div};

pub fn get_default_map_algo() -> Box<dyn MapScoring> {
    // {1.550000,5.000000,1.100000,0.900000,0.200000,100.000000,15.000000}
    Box::new(ItemsStatistical::new(1.55, 5.0, 1.1, 0.9, 0.2, 100.0, 15.0))
}

pub fn get_default_sum_algo() -> Box<dyn Aggregation> {
    Box::new(ItemsAggregation::new(15.0))
}

#[derive(Debug, Clone, Copy)]
pub enum Score {
    Float(f64),
    Fixed(Decimal),
}

impl PartialEq for Score {
    fn eq(&self, other: &Score) -> bool {
        match self {
            Score::Float(f) => match other {
                Score::Float(f_o) => f == f_o,
                Score::Fixed(_d_o) => false,
            },
            Score::Fixed(d) => match other {
                Score::Float(_f_o) => false,
                Score::Fixed(d_o) => d == d_o,
            },
        }
    }
}

impl Eq for Score {}

impl Ord for Score {
    fn cmp(&self, other: &Score) -> Ordering {
        match self {
            Score::Float(f) => match other {
                Score::Float(f_o) => {
                    if f < f_o {
                        Ordering::Less
                    } else if f > f_o {
                        Ordering::Greater
                    } else {
                        Ordering::Equal
                    }
                }
                Score::Fixed(d_o) => f64_to_decimal(*f, 6).cmp(d_o),
            },
            Score::Fixed(d) => match other {
                Score::Float(f_o) => d.cmp(&f64_to_decimal(*f_o, 6)),
                Score::Fixed(d_o) => d.cmp(d_o),
            },
        }
    }
}

impl PartialOrd for Score {
    fn partial_cmp(&self, other: &Score) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Add for Score {
    type Output = Score;

    fn add(self, other: Score) -> Score {
        match self {
            Score::Float(f) => match other {
                Score::Float(f_o) => Score::Float(f + f_o),
                Score::Fixed(d_o) => Score::Fixed(f64_to_decimal(f, 6) + d_o),
            },
            Score::Fixed(d) => match other {
                Score::Float(f_o) => Score::Fixed(d + f64_to_decimal(f_o, 6)),
                Score::Fixed(d_o) => Score::Fixed(d + d_o),
            },
        }
    }
}

impl Div<usize> for Score {
    type Output = Self;
    fn div(self, rhs: usize) -> Self {
        match self {
            Score::Float(f) => Score::Float(f / (rhs as f64)),
            Score::Fixed(d) => Score::Fixed(d / Decimal::from(rhs)),
        }
    }
}

impl Score {
    pub fn to_float(&self) -> f64 {
        match self {
            Score::Float(v) => *v,
            Score::Fixed(d) => d.to_f64().unwrap(),
        }
    }

    pub fn to_fixed(&self) -> Decimal {
        match self {
            Score::Float(v) => f64_to_decimal(*v, 6),
            Score::Fixed(d) => *d,
        }
    }
}

impl fmt::Display for Score {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            self.to_fixed()
                .round_dp_with_strategy(2, RoundingStrategy::MidpointAwayFromZero)
        )
    }
}

pub trait MapScoring {
    fn display(&self) -> String;

    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        speed_record: Option<&super::SpeedRecord>,
        overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)>;
}

pub trait Aggregation {
    fn display(&self) -> String;

    fn compute_total_score(
        &self,
        map_scores: &[Vec<(super::PlayerTimeRecord, Score)>],
    ) -> HashMap<super::Player, (Score, usize)>;
}

pub struct ClassicLadder {
    first_place_score: Decimal,
    speed_record_score: Decimal,
}

impl ClassicLadder {
    pub fn new(params: &[Decimal]) -> Self {
        let def0 = dec!(100);
        let def1 = dec!(10);
        let first_place_score = params.get(0).unwrap_or(&def0);
        let speed_record_score = params.get(1).unwrap_or(&def1);
        Self {
            first_place_score: *first_place_score,
            speed_record_score: *speed_record_score,
        }
    }
}

impl MapScoring for ClassicLadder {
    fn display(&self) -> String {
        format!(
            "Classic({}, {})",
            self.first_place_score, self.speed_record_score
        )
    }
    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        speed_record: Option<&super::SpeedRecord>,
        _overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)> {
        let mut res = Vec::new();
        for record in records.iter() {
            let mut points = dec!(0);
            if let Some(sr) = speed_record {
                if sr.player_id == record.player.id {
                    points += self.speed_record_score;
                }
            }
            points += (self.first_place_score / Decimal::from(record.time_record.pos)).floor();
            res.push((record.clone(), Score::Fixed(points)))
        }
        res
    }
}

pub struct EESystem {
    first_place_score: Decimal,
}

impl EESystem {
    pub fn new(params: &[Decimal]) -> Self {
        let def0 = dec!(1000);
        let first_place_score = params.get(0).unwrap_or(&def0);
        Self {
            first_place_score: *first_place_score,
        }
    }

    fn calculate_c2(&self, pos: i16) -> Decimal {
        let order = pos / 50;
        let subpos = Decimal::from(pos - order * 50);
        let base = Decimal::from(100) / Decimal::from(2u64.pow(order as u32));
        let mul = Decimal::from(1) / Decimal::from(2u64.pow(order as u32));
        base - subpos * mul
    }
}

impl MapScoring for EESystem {
    fn display(&self) -> String {
        format!("EESystem({})", self.first_place_score)
    }
    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        _speed_record: Option<&super::SpeedRecord>,
        _overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)> {
        let mut res = Vec::new();
        if !records.is_empty() {
            let best_time = &records[0].time_record.time;
            for record in records.iter() {
                let points = if record.time_record.pos == 1 {
                    self.first_place_score
                } else {
                    let time = &record.time_record.time;
                    let c1 = best_time / time;
                    let c2 = self.calculate_c2(record.time_record.pos);
                    c1 * (c2 / Decimal::from(100)) * self.first_place_score
                };
                res.push((record.clone(), Score::Fixed(points)))
            }
        }
        res
    }
}

pub struct EESystem2 {
    scale: f64,
    c2_coef: f64,
}

impl EESystem2 {
    pub fn new(params: &[Decimal]) -> Self {
        let scale = params.get(0).map(|x| x.to_f64().unwrap()).unwrap_or(1000.);
        let c2_coef = params.get(1).map(|x| x.to_f64().unwrap()).unwrap_or(0.988);
        Self { scale, c2_coef }
    }

    fn calculate_c2(&self, pos: i16) -> f64 {
        f64::powi(self.c2_coef, (pos - 1).into())
    }
}

impl MapScoring for EESystem2 {
    fn display(&self) -> String {
        format!(
            "EESystem2(scale={:.2}, c2_coef={:.2})",
            self.scale, self.c2_coef
        )
    }

    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        _speed_record: Option<&super::SpeedRecord>,
        _overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)> {
        let mut res = Vec::new();
        for record in records.iter() {
            let reltime = record.time_record.reltime.to_f64().unwrap();
            let points = self.scale * (1.0 / reltime) * self.calculate_c2(record.time_record.pos);
            res.push((record.clone(), Score::Float(points)))
        }
        res
    }
}

pub struct Items4 {
    k: f64,
    c1: f64,
    c2: f64,
    scale: f64,
}

impl Items4 {
    pub fn new(params: &[Decimal]) -> Self {
        let k = params.get(0).map(|x| x.to_f64().unwrap()).unwrap_or(1.);
        let c1 = params.get(1).map(|x| x.to_f64().unwrap()).unwrap_or(0.9);
        let c2 = params.get(2).map(|x| x.to_f64().unwrap()).unwrap_or(0.7);
        let scale = params.get(3).map(|x| x.to_f64().unwrap()).unwrap_or(400.);
        Self { k, c1, c2, scale }
    }
}

impl MapScoring for Items4 {
    fn display(&self) -> String {
        format!(
            "ItemsPlain({:.2}, {:.2}, {:.2}, {})",
            self.k, self.c1, self.c2, self.scale
        )
    }

    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        _speed_record: Option<&super::SpeedRecord>,
        _overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)> {
        let mut res = Vec::new();
        let total_recs = records.len() as f64;
        let mul3 = f64::log2(total_recs);
        for record in records.iter() {
            if records.len() == 1 {
                // 0 points score in case of only one record
                res.push((record.clone(), Score::Float(0.0)))
            } else {
                let t0 = records[0].time_record.time.to_f64().unwrap();
                let t1 = records[1].time_record.time.to_f64().unwrap();
                let time_player = record.time_record.time.to_f64().unwrap();

                let mul1 = if record.time_record.pos == 1 {
                    f64::powf(2. * t1 / (t1 + time_player), self.k * f64::log2(total_recs))
                } else {
                    f64::powf(2. * t0 / (t0 + time_player), self.k * f64::log2(total_recs))
                };
                let pos_player = f64::from(record.time_record.pos);
                let mul2 = f64::powf(self.c1, (pos_player - 1.0) / f64::powf(total_recs, self.c2));
                let points = self.scale * mul1 * mul2 * mul3;
                res.push((record.clone(), Score::Float(points)))
            }
        }
        res
    }
}

pub struct ItemsStatistical {
    k: f64,
    b: f64,
    d: f64,
    i_power: f64,
    c: f64,
    scale: f64,
    m: f64,
}

impl ItemsStatistical {
    #[allow(clippy::many_single_char_names)]
    pub fn new(k: f64, b: f64, d: f64, i_power: f64, c: f64, scale: f64, m: f64) -> Self {
        Self {
            k,
            b,
            d,
            i_power,
            c,
            scale,
            m,
        }
    }

    fn p_x(&self, x: f64) -> f64 {
        if x < 0.0001 {
            0.0
        } else {
            f64::exp(-1. / x) * f64::powf(x, -self.d)
        }
    }

    fn l_x(&self, x: f64) -> f64 {
        if x < 0.0001 {
            0.0
        } else {
            use statrs::function::gamma::gamma_ur;
            gamma_ur(self.d - 1., 1. / x)
        }
    }

    fn n_x(
        &self,
        overall_scores: &HashMap<super::Player, (Score, usize)>,
        player: &super::Player,
        records: &[super::PlayerTimeRecord],
        x: f64,
    ) -> f64 {
        let mut precalc_product = 1.0;
        let mut product_zeros = 0;
        let clamp_threshold = 1e-40;
        for precalc_record in records {
            if precalc_record.player.id != player.id {
                let score = if let Some((score, _)) = overall_scores.get(&precalc_record.player) {
                    score.to_float() / self.scale
                } else {
                    1.0
                };
                let i_h = f64::powf(score, self.i_power);
                let this_l = 1.0 - self.l_x(self.k * i_h * x);
                if this_l < clamp_threshold {
                    product_zeros += 1;
                    if product_zeros >= 2 {
                        return 0.0;
                    }
                    continue; //skip one zero and calculate the remaining product
                }
                precalc_product *= this_l;
            }
        }
        if precalc_product < clamp_threshold {
            return 0.0;
        }
        let mut sum = 0.0;
        for record in records {
            if record.player.id != player.id {
                let score = if let Some((score, _)) = overall_scores.get(&record.player) {
                    score.to_float() / self.scale
                } else {
                    1.0
                };
                let i_i = f64::powf(score, self.i_power);
                let mul1 = i_i * self.p_x(self.k * i_i * x);
                let mul2_divider = 1.0 - self.l_x(self.k * i_i * x);
                if product_zeros == 0 {
                    sum += mul1 * precalc_product / mul2_divider;
                } else {
                    sum += if mul2_divider < clamp_threshold {
                        mul1 * precalc_product
                    } else {
                        0.0
                    };
                }
            }
        }
        sum
    }

    fn q_x(
        &self,
        overall_scores: &HashMap<super::Player, (Score, usize)>,
        player: &super::Player,
        records: &[super::PlayerTimeRecord],
        r_y: f64,
        x: f64,
    ) -> f64 {
        self.p_x(self.b * (1. / x - 1.))
            * self.n_x(overall_scores, player, records, 1. / (x * r_y) - 1.)
    }

    fn pos_correction(
        &self,
        points: f64,
        better_records: &[&super::PlayerTimeRecord],
        overall_scores: &HashMap<super::Player, (Score, usize)>,
    ) -> f64 {
        let mut sum = 0.0;
        for i in better_records {
            let score = if let Some((score, maps)) = overall_scores.get(&i.player) {
                let mul = 1.0 + 1.0 / (f64::powf(2.0, (maps + 1) as f64 / self.m) - 1.0);
                score.to_float() * mul / self.scale
            } else {
                1.0
            };
            sum += f64::exp(-score);
        }
        let f = f64::exp(-points) + self.c * sum;
        f64::ln(f) / (f - 1.0)
    }
}

impl MapScoring for ItemsStatistical {
    fn display(&self) -> String {
        format!(
            "ItemsStat(k={:.2}, b={:.2}, d={:.2}, i_power={:.2}, c={:.2}, scale={:.2}, m={:.2})",
            self.k, self.b, self.d, self.i_power, self.c, self.scale, self.m
        )
    }

    fn compute_map_scores(
        &self,
        records: &[super::PlayerTimeRecord],
        _speed_record: Option<&super::SpeedRecord>,
        overall_scores: Option<&HashMap<super::Player, (Score, usize)>>,
    ) -> Vec<(super::PlayerTimeRecord, Score)> {
        records
            .par_iter()
            .map(|record| {
                if records.len() == 1 {
                    // 0 points score in case of only one record
                    (record.clone(), Score::Float(0.0))
                } else {
                    let points = if let Some(overalls) = overall_scores {
                        let r = record.time_record.reltime.to_f64().unwrap();
                        let nom = quadrature::double_exponential::integrate(
                            |x: f64| {
                                self.q_x(overalls, &record.player, records, r, x)
                                    * f64::powf(x / (1.0 - x), 0.5)
                            },
                            0.,
                            1.,
                            INTEGRATION_PRECISION,
                        )
                        .integral;
                        let den = quadrature::double_exponential::integrate(
                            |x: f64| self.q_x(overalls, &record.player, records, r, x),
                            0.,
                            1.,
                            INTEGRATION_PRECISION,
                        )
                        .integral;
                        let uncorrected = f64::powi(nom / den, 2);
                        let better_recs: Vec<_> = records
                            .iter()
                            .filter(|x| x.time_record.pos < record.time_record.pos)
                            .collect();
                        self.pos_correction(uncorrected, &better_recs, overalls)
                    } else {
                        1.0
                    };
                    (record.clone(), Score::Float(self.scale * points))
                }
            })
            .collect()
    }
}

pub struct SumAggregation {}

impl SumAggregation {
    pub fn new(_params: &[Decimal]) -> Self {
        Self {}
    }
}

impl Aggregation for SumAggregation {
    fn display(&self) -> String {
        "sum".to_string()
    }

    fn compute_total_score(
        &self,
        map_scores: &[Vec<(super::PlayerTimeRecord, Score)>],
    ) -> HashMap<super::Player, (Score, usize)> {
        let mut players = HashMap::new();
        for map in map_scores.iter() {
            for (record, score) in map.iter() {
                players
                    .entry(record.player.clone())
                    .and_modify(|(s, c)| {
                        *s = *s + *score;
                        *c += 1;
                    })
                    .or_insert((*score, 1));
            }
        }
        players
    }
}

pub struct AvgDroppingAggregation {
    drop_percent: usize,
    min_maps: usize,
}

impl AvgDroppingAggregation {
    pub fn new(params: &[Decimal]) -> Self {
        let drop_percent = params.get(0).map(|x| x.to_usize().unwrap()).unwrap_or(10);
        let min_maps = params.get(1).map(|x| x.to_usize().unwrap()).unwrap_or(10);
        Self {
            drop_percent,
            min_maps,
        }
    }
}

impl Aggregation for AvgDroppingAggregation {
    fn display(&self) -> String {
        format!("Avg({}, {})", self.drop_percent, self.min_maps)
    }

    fn compute_total_score(
        &self,
        map_scores: &[Vec<(super::PlayerTimeRecord, Score)>],
    ) -> HashMap<super::Player, (Score, usize)> {
        let mut players = HashMap::new();
        for map in map_scores.iter() {
            for (record, score) in map.iter() {
                let entry = players
                    .entry(record.player.clone())
                    .or_insert_with(Vec::new);
                entry.push(*score);
            }
        }
        let mut res = HashMap::new();
        for (player, mut scores) in players.drain() {
            scores.sort();
            let to_remove = scores.len() * self.drop_percent / 100;
            for _i in 0..to_remove {
                scores.remove(0);
            }
            if scores.len() >= self.min_maps {
                // Player is eligible for rankings
                if !scores.is_empty() {
                    let mut sum = scores[0];
                    for i in scores.iter().skip(1) {
                        sum = sum + *i;
                    }
                    let avg = sum / scores.len();
                    res.insert(player, (avg, scores.len()));
                }
            }
        }
        res
    }
}

pub struct ItemsAggregation {
    m: f64,
}

impl ItemsAggregation {
    pub fn new(m: f64) -> Self {
        Self { m }
    }
}

impl Aggregation for ItemsAggregation {
    fn display(&self) -> String {
        format!("ItemsAgg({})", self.m)
    }

    fn compute_total_score(
        &self,
        map_scores: &[Vec<(super::PlayerTimeRecord, Score)>],
    ) -> HashMap<super::Player, (Score, usize)> {
        let mut players = HashMap::new();
        for map in map_scores.iter() {
            for (record, score) in map.iter() {
                let entry = players
                    .entry(record.player.clone())
                    .or_insert_with(Vec::new);
                entry.push(*score);
            }
        }
        let mut res = HashMap::new();
        for (player, mut scores) in players.drain() {
            scores.sort_by(|a, b| b.cmp(a)); // WHY RUST STD HAS NO REVERSE SORT!!!!!111
            let mut points: f64 = 0.0;
            for (i, s) in scores.iter().enumerate() {
                let fscore = s.to_float();
                let mul = f64::exp2(-(i as f64) / self.m);
                points += fscore * mul;
            }
            points *= (f64::powf(2.0, 1.0 / self.m) - 1.0) / f64::powf(2.0, 1.0 / self.m);
            res.insert(player, (Score::Float(points), scores.len()));
        }
        res
    }
}
