use rust_decimal::Decimal;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ResultChange {
    New(i16, Decimal),                       // new pos, new time
    Improvement(i16, Decimal, i16, Decimal), // new pos, new time, old pos, old time
    Degradation(i16, Decimal, i16, Decimal), // new pos, new time, old pos, old time
    Deleted(i16, Decimal),                   // old pos, old time
}

impl ResultChange {
    pub fn from_db(
        new_pos: Option<i16>,
        new_time: Option<Decimal>,
        old_pos: Option<i16>,
        old_time: Option<Decimal>,
    ) -> Option<Self> {
        match (new_pos, new_time, old_pos, old_time) {
            (Some(np), Some(nt), None, None) => Some(ResultChange::New(np, nt)),
            (None, None, Some(op), Some(ot)) => Some(ResultChange::Deleted(op, ot)),
            (Some(np), Some(nt), Some(op), Some(ot)) => Some(if nt < ot {
                ResultChange::Improvement(np, nt, op, ot)
            } else {
                ResultChange::Degradation(np, nt, op, ot)
            }),
            _ => None,
        }
    }

    pub fn get_cmp_time(&self) -> Decimal {
        match self {
            ResultChange::New(_, t) => *t,
            ResultChange::Improvement(_, t, _, _) => *t,
            ResultChange::Degradation(_, t, _, _) => *t,
            ResultChange::Deleted(_, t) => *t,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PlayerResultChange {
    pub player_id: i32,
    pub change: ResultChange,
}

#[derive(Debug)]
pub struct SpeedRecordChange {
    pub new_player_id: i32,
    pub new_speed: Decimal,
    pub old_player_id: Option<i32>,
    pub old_speed: Option<Decimal>,
}

#[derive(Debug)]
pub struct ResultChangeset {
    pub changes: Vec<PlayerResultChange>,
    pub speed_record_change: Option<SpeedRecordChange>,
}

impl ResultChangeset {
    pub fn is_empty(&self) -> bool {
        self.changes.is_empty() && self.speed_record_change.is_none()
    }
}

pub type SpeedRecord = (i32, Decimal); // player_id, speed
pub type TimeRecord = (i32, i16, Decimal); // player_id, pos, time

#[derive(Debug)]
pub struct Results {
    pub speed_record: Option<SpeedRecord>,
    pub time_records: Vec<TimeRecord>,
}

fn apply_change(time_records: &mut Vec<TimeRecord>, change: PlayerResultChange) {
    if let ResultChange::Deleted(old_pos, old_time) = change.change {
        time_records.push((change.player_id, old_pos, old_time));
    } else {
        let mut to_delete = None;
        for (ix, (player_id, pos, time)) in time_records.iter_mut().enumerate() {
            if *player_id == change.player_id {
                match change.change {
                    ResultChange::New(_, _) => to_delete = Some(ix),
                    ResultChange::Improvement(_, _, old_pos, old_time) => {
                        *pos = old_pos;
                        *time = old_time;
                    }
                    ResultChange::Degradation(_, _, old_pos, old_time) => {
                        *pos = old_pos;
                        *time = old_time;
                    }
                    ResultChange::Deleted(_, _) => {
                        panic!(
                            "Change {:?} has deleted record which is present in the current record table, possibly corrupt data in DB, please check",
                            change.change
                        );
                    }
                }
            }
        }
        if let Some(ix) = to_delete {
            time_records.remove(ix);
        }
    }
}

impl Results {
    pub fn recalc_positions(&mut self) {
        self.time_records
            .sort_unstable_by(|(_, _, time1), (_, _, time2)| time1.cmp(time2));
        let mut ix = 0;
        let mut current_pos = 1;
        let mut curplus = 1;
        while ix < self.time_records.len() {
            self.time_records[ix].1 = current_pos;
            if (ix < self.time_records.len() - 1)
                && (self.time_records[ix].2 != self.time_records[ix + 1].2)
            {
                current_pos += curplus;
                curplus = 1
            } else {
                curplus += 1
            }
            ix += 1;
        }
    }
    pub fn go_back(&self, changes: &ResultChangeset) -> Results {
        let speed_record = if let Some(ref s) = changes.speed_record_change {
            if let (Some(p), Some(s)) = (s.old_player_id, s.old_speed) {
                Some((p, s))
            } else {
                None
            }
        } else {
            self.speed_record
        };
        let mut time_records = self.time_records.clone();
        for i in changes.changes.iter() {
            apply_change(&mut time_records, *i);
        }

        let mut res = Results {
            speed_record,
            time_records,
        };
        res.recalc_positions();
        res
    }

    #[allow(clippy::comparison_chain)]
    pub fn compute_changes(&self, older_results: Option<&Results>) -> ResultChangeset {
        if let Some(older_results) = older_results {
            let speed_record_change = match (self.speed_record, older_results.speed_record) {
                (Some(new), Some(old)) => {
                    if old != new {
                        Some(SpeedRecordChange {
                            new_player_id: new.0,
                            new_speed: new.1,
                            old_player_id: Some(old.0),
                            old_speed: Some(old.1),
                        })
                    } else {
                        None
                    }
                }
                (Some(new), None) => Some(SpeedRecordChange {
                    new_player_id: new.0,
                    new_speed: new.1,
                    old_player_id: None,
                    old_speed: None,
                }),
                _ => None,
            };

            let mut changes = Vec::new();
            let mut record_map = HashMap::new(); // player_id -> Option(new_pos, new_time), Option(old_pos, old_time)
            for (player_id, pos, time) in self.time_records.iter() {
                record_map.insert(player_id, (Some((pos, time)), None));
            }
            for (player_id, pos, time) in older_results.time_records.iter() {
                let entry = record_map.entry(player_id).or_insert((None, None));
                entry.1 = Some((pos, time));
            }

            for (player_id, result) in record_map.into_iter() {
                let player_id = *player_id;
                match result {
                    (None, None) => {}
                    (None, Some((old_pos, old_time))) => {
                        changes.push(PlayerResultChange {
                            player_id,
                            change: ResultChange::Deleted(*old_pos, *old_time),
                        });
                    }
                    (Some((new_pos, new_time)), None) => {
                        changes.push(PlayerResultChange {
                            player_id,
                            change: ResultChange::New(*new_pos, *new_time),
                        });
                    }
                    (Some((new_pos, new_time)), Some((old_pos, old_time))) => {
                        if new_time < old_time {
                            changes.push(PlayerResultChange {
                                player_id,
                                change: ResultChange::Improvement(
                                    *new_pos, *new_time, *old_pos, *old_time,
                                ),
                            });
                        } else if new_time > old_time {
                            changes.push(PlayerResultChange {
                                player_id,
                                change: ResultChange::Degradation(
                                    *new_pos, *new_time, *old_pos, *old_time,
                                ),
                            });
                        }
                    }
                }
            }

            ResultChangeset {
                changes,
                speed_record_change,
            }
        } else {
            let speed_record_change = self.speed_record.map(|sr| SpeedRecordChange {
                new_player_id: sr.0,
                new_speed: sr.1,
                old_player_id: None,
                old_speed: None,
            });
            let mut changes = Vec::new();
            for (player_id, pos, time) in self.time_records.iter() {
                changes.push(PlayerResultChange {
                    player_id: *player_id,
                    change: ResultChange::New(*pos, *time),
                })
            }
            ResultChangeset {
                changes,
                speed_record_change,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_decimal_macros::*;

    fn r2() -> Results {
        Results {
            speed_record: Some((1, dec!(6543.21))),
            time_records: vec![
                (1, 1, dec!(15.45)),
                (2, 2, dec!(15.80)),
                (3, 3, dec!(15.90)),
                (4, 4, dec!(16.30)),
                (5, 5, dec!(17.48)),
            ],
        }
    }

    fn r1() -> Results {
        Results {
            speed_record: Some((2, dec!(7000.03))),
            time_records: vec![
                (1, 1, dec!(15.45)),
                (3, 2, dec!(15.50)),
                (2, 3, dec!(15.80)),
                (6, 4, dec!(15.85)),
                (4, 5, dec!(16.30)),
            ],
        }
    }

    #[test]
    fn test_history() {
        let r1 = r1();
        let r2 = r2();
        let changes = r1.compute_changes(Some(&r2));
        assert!(changes.speed_record_change.is_some());
        assert!(changes.changes.len() == 3);
        let r3 = r1.go_back(&changes);
        assert!(r3.speed_record == r2.speed_record);
        assert!(r3.time_records == r2.time_records);
        let other_changes = r2.compute_changes(Some(&r1));
        let r4 = r2.go_back(&other_changes);
        assert!(r4.speed_record == r1.speed_record);
        assert!(r4.time_records == r1.time_records);
    }
}
