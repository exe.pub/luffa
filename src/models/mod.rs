pub mod history;
pub mod ladders;
#[macro_use]
mod ord;
mod constants;
mod utils;

use super::schema::*;
use super::util::utcnow;
use anyhow::{anyhow, Error};
use chrono::prelude::*;
use chrono::Duration;
use constants::*;
use diesel::expression_methods::PgArrayExpressionMethods;
use diesel::prelude::*;
use diesel::sql_types::VarChar;
use diesel::PgConnection;
use dpcolors::DPString;
use ipnetwork::IpNetwork;
use ladders::*;
use num_traits::cast::ToPrimitive;
pub use ord::*;
use pk3rs::MapTags;
use rocket::http::RawStr;
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use serde::Serialize;
use std::collections::{hash_map::Entry, HashMap, HashSet};
use std::hash::{Hash, Hasher};
use std::net::SocketAddr;
use std::str::FromStr;
use xagy::rcon::{RconRemote, RconSecure};
use xonotic_db::server_db;

pub use utils::heatmap_data;

const TARGET_PATH: &str = "./static/levelshots/";

#[derive(Debug, Queryable, Clone)]
#[diesel(table_name = server)]
pub struct Server {
    pub id: i32,
    pub name: String,
    pub ip_address: IpNetwork,
    pub rcon_password: String,
    pub port: i32,
    pub active: bool,
    pub server_db_path: String,
}

impl Server {
    pub fn get_all(conn: &mut PgConnection) -> Result<Vec<Server>, Error> {
        Ok(server::table.load::<Server>(conn)?)
    }

    pub fn get_active(conn: &mut PgConnection) -> Result<Vec<Server>, Error> {
        Ok(server::table
            .filter(server::active.eq(true))
            .load::<Server>(conn)?)
    }

    pub fn get_rcon_remote(&self) -> RconRemote {
        let ip = self.ip_address.ip();
        RconRemote {
            addr: SocketAddr::new(ip, self.port as u16),
            secure: RconSecure::Time,
            password: self.rcon_password.clone(),
        }
    }
}

#[derive(Debug, Identifiable, Queryable, PartialEq, Clone)]
#[diesel(table_name = map)]
pub struct Map {
    pub id: i32,
    pub name: String,
    pub haste: bool,
    pub slick: bool,
    pub vq3_compat: bool,
    pub nexrun: bool,
    pub rocket: bool,
    pub mortar: bool,
    pub crylink: bool,
    pub hagar: bool,
    pub hook: bool,
    pub fireball: bool,
    pub nex: bool,
    pub electro: bool,
    pub blaster: bool,
    pub mine_layer: bool,
    pub sg: bool,
    pub mg: bool,
    pub max_pos: i16,
    pub server_id: Option<i32>,
    pub rating_sum: i32,
    pub rating_votes: i32,
}

#[derive(AsChangeset, Default)]
#[diesel(table_name = map)]
struct MapTagsChangeset {
    pub haste: bool,
    pub slick: bool,
    pub vq3_compat: bool,
    pub nexrun: bool,
    pub rocket: bool,
    pub mortar: bool,
    pub crylink: bool,
    pub hagar: bool,
    pub hook: bool,
    pub fireball: bool,
    pub nex: bool,
    pub electro: bool,
    pub blaster: bool,
    pub mine_layer: bool,
    pub sg: bool,
    pub mg: bool,
}

impl MapTagsChangeset {
    fn from_tags(tags: MapTags) -> Self {
        Self {
            haste: tags.contains(MapTags::HASTE),
            slick: tags.contains(MapTags::SLICK),
            vq3_compat: tags.contains(MapTags::VQ3_COMPAT),
            nexrun: tags.contains(MapTags::NEXRUN),
            rocket: tags.contains(MapTags::ROCKET),
            mortar: tags.contains(MapTags::MORTAR),
            crylink: tags.contains(MapTags::CRYLINK),
            hagar: tags.contains(MapTags::HAGAR),
            hook: tags.contains(MapTags::HOOK),
            fireball: tags.contains(MapTags::FIREBALL),
            nex: tags.contains(MapTags::NEX),
            electro: tags.contains(MapTags::ELECTRO),
            blaster: tags.contains(MapTags::BLASTER),
            mine_layer: tags.contains(MapTags::MINE_LAYER),
            sg: tags.contains(MapTags::SG),
            mg: tags.contains(MapTags::MG),
        }
    }
}

pub struct MapWithMetadata {
    pub map: Map,
    pub images: Vec<MapImage>,
    pub records: Vec<PlayerTimeRecord>,
}

define_sql_function!(fn lower(x: VarChar) -> VarChar);

impl Map {
    pub fn get_by_name(conn: &mut PgConnection, name: &str) -> Result<Option<Self>, Error> {
        Ok(map::table
            .filter(map::name.eq(name))
            .first(conn)
            .optional()?)
    }

    pub fn get_by_name_with_server(
        conn: &mut PgConnection,
        name: &str,
    ) -> Result<Option<(Self, Server)>, Error> {
        Ok(map::table
            .inner_join(server::table)
            .filter(map::name.eq(name))
            .first(conn)
            .optional()?)
    }

    pub fn set_tags(&self, conn: &mut PgConnection, tags: MapTags) -> Result<(), Error> {
        let changeset = MapTagsChangeset::from_tags(tags);
        diesel::update(self).set(changeset).execute(conn)?;
        Ok(())
    }

    pub fn get_for_server(
        conn: &mut PgConnection,
        server_id: i32,
    ) -> Result<HashSet<String>, Error> {
        let mut res = HashSet::new();
        for i in map::table
            .filter(map::server_id.eq(server_id))
            .load::<Map>(conn)?
        {
            res.insert(i.name);
        }
        Ok(res)
    }

    pub fn get_full_for_server(
        conn: &mut PgConnection,
        server_id: i32,
    ) -> Result<HashMap<String, Self>, Error> {
        let mut res = HashMap::new();
        for i in map::table
            .filter(map::server_id.eq(server_id))
            .load::<Map>(conn)?
        {
            res.insert(i.name.clone(), i);
        }
        Ok(res)
    }

    pub fn total_maps(conn: &mut PgConnection, filter_name: Option<&str>) -> Result<i64, Error> {
        let res = match filter_name {
            Some(n) => map::table
                .filter(map::server_id.is_not_null())
                .filter(lower(map::name).like(format!("%{n}%")))
                .count()
                .first(conn)?,
            None => map::table
                .filter(map::server_id.is_not_null())
                .count()
                .first(conn)?,
        };
        Ok(res)
    }

    pub fn get_all(
        conn: &mut PgConnection,
        filter_name: Option<&str>,
        page_number: i64,
        page_size: i64,
    ) -> Result<Vec<MapWithMetadata>, Error> {
        let images_raw = map_image::table.load::<MapImage>(conn)?;
        let records_raw = time_record::table
            .inner_join(map::table)
            .inner_join(player::table)
            .filter(time_record::pos.le(3))
            .order_by(time_record::pos)
            .load::<(TimeRecord, Map, Player)>(conn)?;
        let maps = match filter_name {
            Some(n) => map::table
                .filter(map::server_id.is_not_null())
                .filter(lower(map::name).like(format!("%{n}%")))
                .order_by(map::name)
                .limit(page_size)
                .offset(page_number * page_size)
                .load::<Map>(conn)?,
            None => map::table
                .filter(map::server_id.is_not_null())
                .order_by(map::name)
                .limit(page_size)
                .offset(page_number * page_size)
                .load::<Map>(conn)?,
        };
        let mut records = HashMap::new();
        for (tr, m, p) in records_raw {
            let e: &mut Vec<PlayerTimeRecord> = records.entry(tr.map_id).or_insert_with(Vec::new);
            e.push(PlayerTimeRecord {
                map: m,
                time_record: tr,
                player: p,
            });
        }
        let mut images = HashMap::new();
        for image in images_raw {
            let e = images.entry(image.map_id).or_insert_with(Vec::new);
            e.push(image);
        }
        let mut res = Vec::new();
        for map in maps {
            let images = images.remove(&map.id).unwrap_or_default();
            let records = records.remove(&map.id).unwrap_or_default();
            res.push(MapWithMetadata {
                map,
                images,
                records,
            })
        }
        Ok(res)
    }

    pub fn set_levelshot(
        &self,
        conn: &mut PgConnection,
        levelshot: &image::DynamicImage,
        force: bool,
    ) -> Result<(), Error> {
        use std::path::Path;
        let target_fn = format!("{}.jpeg", self.name);
        let target_path = Path::new(TARGET_PATH).join(&target_fn);
        let url = format!(
            "/static/levelshots/{}",
            RawStr::new(&target_fn).percent_encode()
        );
        let type_ = MapImageType::Levelshot.to_str();

        if (!target_path.exists()) || force {
            levelshot.save(target_path)?;
            let maybe_img = map_image::table
                .filter(map_image::map_id.eq(self.id))
                .filter(map_image::type_.eq(type_))
                .first::<MapImage>(conn)
                .optional()?;
            if let Some(img) = maybe_img {
                if force {
                    diesel::update(&img)
                        .set(map_image::url.eq(&url))
                        .execute(conn)?;
                }
            } else {
                diesel::insert_into(map_image::table)
                    .values((
                        map_image::map_id.eq(self.id),
                        map_image::type_.eq(type_),
                        map_image::url.eq(&url),
                    ))
                    .execute(conn)?;
            }
        }
        Ok(())
    }

    pub fn set_server_id(
        &self,
        conn: &mut PgConnection,
        server_id: Option<i32>,
    ) -> Result<Self, Error> {
        if server_id != self.server_id {
            let res = diesel::update(self)
                .set(map::server_id.eq(server_id))
                .get_result(conn)?;
            Ok(res)
        } else {
            Ok(self.clone())
        }
    }

    pub fn update_records_positions(&self, conn: &mut PgConnection) -> Result<(), Error> {
        let recs = time_record::table
            .filter(time_record::map_id.eq(self.id))
            .order_by(time_record::time.asc())
            .load::<TimeRecord>(conn)?;
        let total = recs.len();
        if total > 0 {
            let mut curpos = 0;
            let mut curplus = 1;
            let mut curtime = dec!(0);
            let best_time = &recs[0].time;
            let second_time = recs.get(1).map(|x| &x.time).unwrap_or(best_time);

            for i in recs.iter() {
                if i.time > curtime {
                    curpos += curplus;
                    curtime = i.time;
                    curplus = 1;
                } else {
                    curplus += 1;
                }
                let w = if curpos == 1 { second_time } else { best_time };
                let reltime = i.time / w;
                diesel::update(time_record::table.filter(time_record::id.eq(i.id)))
                    .set((
                        time_record::pos.eq(curpos),
                        time_record::reltime.eq(&reltime),
                    ))
                    .execute(conn)?;
            }
        }
        Ok(())
    }

    pub fn get_or_create(conn: &mut PgConnection, name: &str) -> Result<Map, Error> {
        let maybe_map: Option<Map> = map::table
            .filter(map::name.eq(name))
            .first(conn)
            .optional()?;
        Ok(match maybe_map {
            Some(m) => m,
            None => diesel::insert_into(map::table)
                .values((map::name.eq(name), map::max_pos.eq(0)))
                .get_result(conn)?,
        })
    }
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = map_video)]
pub struct MapVideo {
    pub id: i32,
    pub map_id: i32,
    pub player_id: i32,
    pub time: Decimal,
    pub record_created_at: NaiveDateTime,
    pub video_created_at: NaiveDateTime,
    pub render_success: bool,
    pub youtube_code: Option<String>,
    pub hidden: bool,
}

impl MapVideo {
    pub fn insert_new(
        conn: &mut PgConnection,
        map_id: i32,
        player_id: i32,
        time: Decimal,
        record_created_at: NaiveDateTime,
    ) -> Result<(), Error> {
        diesel::insert_into(map_video::table)
            .values((
                map_video::map_id.eq(map_id),
                map_video::player_id.eq(player_id),
                map_video::time.eq(time),
                map_video::record_created_at.eq(record_created_at),
                map_video::video_created_at.eq(utcnow()),
                map_video::render_success.eq(true),
                map_video::youtube_code.eq::<Option<String>>(None),
                map_video::is_hidden.eq(false),
            ))
            .execute(conn)?;
        Ok(())
    }

    pub fn get_for_map(
        conn: &mut PgConnection,
        map: &Map,
    ) -> Result<Vec<(MapVideo, Player)>, Error> {
        Ok(map_video::table
            .inner_join(player::table)
            .filter(map_video::youtube_code.is_not_null())
            .filter(map_video::map_id.eq(map.id))
            .filter(map_video::is_hidden.eq(false))
            .order_by(map_video::time.asc())
            .load(conn)?)
    }

    pub fn get_for_player(
        conn: &mut PgConnection,
        player_id: i32,
    ) -> Result<Vec<(MapVideo, Map)>, Error> {
        Ok(map_video::table
            .inner_join(map::table)
            .filter(map_video::player_id.eq(player_id))
            .filter(map_video::youtube_code.is_not_null())
            .filter(map_video::is_hidden.eq(false))
            .order_by(map_video::record_created_at.desc())
            .load(conn)?)
    }

    pub fn get_video_code(&self) -> Option<&str> {
        self.youtube_code
            .as_ref()
            .map(|x| x.trim_start_matches("https://youtu.be/"))
    }
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = demo_file)]
pub struct DemoFile {
    pub id: i32,
    pub map_id: i32,
    pub player_id: i32,
    pub time: Decimal,
    pub demo_file_path: String,
}

impl DemoFile {
    pub fn store_new(
        conn: &mut PgConnection,
        map_id: i32,
        player_id: i32,
        time: Decimal,
        demo_file_path: String,
    ) -> Result<(), Error> {
        diesel::insert_into(demo_file::table)
            .values((
                demo_file::map_id.eq(map_id),
                demo_file::player_id.eq(player_id),
                demo_file::time.eq(time),
                demo_file::demo_file_path.eq(demo_file_path),
            ))
            .execute(conn)?;
        Ok(())
    }

    pub fn get_for_map(
        conn: &mut PgConnection,
        map_name: &str,
    ) -> Result<(DemoFile, Map, Player, TimeRecord), Error> {
        let map =
            Map::get_by_name(conn, map_name)?.ok_or_else(|| anyhow!("no map {}", map_name))?;
        let (d, p, tr) = demo_file::table
            .inner_join(player::table)
            .inner_join(time_record::table.on(time_record::time.eq(demo_file::time)))
            .filter(demo_file::map_id.eq(map.id))
            .filter(time_record::pos.eq(1))
            .order_by(demo_file::time.asc())
            .first(conn)?;
        Ok((d, map, p, tr))
    }

    pub fn get_oldest_first_without_vid(
        conn: &mut PgConnection,
    ) -> Result<(DemoFile, Map, Player, TimeRecord), Error> {
        let records: Vec<(TimeRecord, Player, Map, DemoFile, Option<MapVideo>)> =
            time_record::table
                .inner_join(player::table)
                .inner_join(map::table)
                .inner_join(
                    demo_file::table.on(time_record::time
                        .eq(demo_file::time)
                        .and(time_record::player_id.eq(demo_file::player_id))
                        .and(time_record::map_id.eq(demo_file::map_id))),
                )
                .left_outer_join(
                    map_video::table.on(time_record::time
                        .eq(map_video::time)
                        .and(time_record::player_id.eq(map_video::player_id))
                        .and(time_record::map_id.eq(map_video::map_id))),
                )
                .filter(map::server_id.is_not_null())
                .filter(time_record::pos.eq(1))
                .filter(time_record::time.le(dec!(2400.0)))
                .filter(time_record::created_at.le(utcnow() - chrono::Duration::days(1)))
                .order_by(time_record::created_at.asc())
                .load(conn)?;
        for (tr, p, m, df, maybe_mv) in records {
            if maybe_mv.is_none() {
                // maybe we can filter that in SQL somehow?
                // but I'm too lazy to mess with diesel that much
                return Ok((df, m, p, tr));
            }
        }
        Err(anyhow!("ALL RECORDS RENDERED HURRAH"))
    }
}

#[derive(Debug, Clone, Copy)]
enum MapImageType {
    Levelshot,
    // Wsq3df,
    // Custom,
}

impl MapImageType {
    fn to_str(self) -> &'static str {
        match self {
            MapImageType::Levelshot => "LEVELSHOT",
            // MapImageType::Wsq3df => "WSQ3DF",
            // MapImageType::Custom => "CUSTOM",
        }
    }
}

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = map_image)]
pub struct MapImage {
    pub id: i32,
    pub map_id: i32,
    pub type_: String,
    pub url: String,
}

impl MapImage {
    pub fn get_for_map(conn: &mut PgConnection, map: &Map) -> Result<Vec<Self>, Error> {
        Ok(map_image::table
            .filter(map_image::map_id.eq(map.id))
            .order_by(map_image::type_)
            .load(conn)?)
    }
}

#[derive(Debug, Identifiable, Queryable, Clone)]
#[diesel(table_name = player)]
pub struct Player {
    pub id: i32,
    pub stats_id: Option<i32>,
    pub nickname: String,
    pub nickname_nocolors: String,
    pub created_at: NaiveDateTime,
}

impl PartialEq for Player {
    fn eq(&self, other: &Player) -> bool {
        self.id == other.id
    }
}

impl Eq for Player {}

impl Hash for Player {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

pub struct PlayerProfile {
    pub ratings: Vec<(Rating, RatingTotal)>,
    pub recent_results: Vec<(Map, ResultsChangeset, history::ResultChange)>,
    pub time_records: Vec<PlayerTimeRecord>,
    pub best_maps: Vec<(i32, Vec<(PlayerTimeRecord, Decimal)>)>,
    pub videos_number: i64,
}

pub struct PlayerAdminProfile {
    pub player: Player,
    pub keys: Vec<PlayerKey>,
    pub nicks: Vec<PlayerNickname>,
}

pub struct PlayerComparison {
    pub map: Map,
    pub left: TimeRecord,
    pub right: TimeRecord,
}

impl Player {
    pub fn get_by_id(conn: &mut PgConnection, id: i32) -> Result<Option<Self>, Error> {
        Ok(player::table
            .filter(player::id.eq(id))
            .first::<Self>(conn)
            .optional()?)
    }
    pub fn get_by_key(conn: &mut PgConnection, key: &str) -> Result<Option<Self>, Error> {
        let interim = player::table
            .inner_join(player_key::table)
            .filter(player_key::crypto_idfp.eq(key))
            .first::<(Player, PlayerKey)>(conn)
            .optional()?;
        Ok(interim.map(|x| x.0))
    }

    pub fn compare(
        &self,
        conn: &mut PgConnection,
        other: &Self,
    ) -> Result<Vec<PlayerComparison>, Error> {
        let left_maps: Vec<PlayerTimeRecord> = self.get_maps(conn, None)?;
        let right_maps = other.get_maps(conn, None)?;
        let mut right_maps_hm: HashMap<_, _> = right_maps
            .into_iter()
            .map(|ptr| (ptr.map.id, ptr.time_record))
            .collect();
        let mut res = Vec::new();
        for ptr in left_maps {
            if let Some(other_tr) = right_maps_hm.remove(&ptr.map.id) {
                res.push(PlayerComparison {
                    map: ptr.map,
                    left: ptr.time_record,
                    right: other_tr,
                })
            }
        }
        res.sort_by_key(|x| {
            (x.right.time - x.left.time) * dec!(100) / std::cmp::min(x.right.time, x.left.time)
        });
        Ok(res)
    }

    pub fn get_keys(&self, conn: &mut PgConnection) -> Result<Vec<PlayerKey>, Error> {
        Ok(player_key::table
            .filter(player_key::player_id.eq(self.id))
            .order_by(player_key::last_used_at.desc())
            .load(conn)?)
    }

    pub fn get_nicks(&self, conn: &mut PgConnection) -> Result<Vec<PlayerNickname>, Error> {
        Ok(player_nickname::table
            .filter(player_nickname::player_id.eq(self.id))
            .order_by(player_nickname::last_used_at.desc())
            .load(conn)?)
    }

    pub fn get_admin_profile(
        conn: &mut PgConnection,
        id: i32,
    ) -> Result<Option<PlayerAdminProfile>, Error> {
        if let Some(player) = Player::get_by_id(conn, id)? {
            let keys = player.get_keys(conn)?;
            let nicks = player.get_nicks(conn)?;
            Ok(Some(PlayerAdminProfile {
                player,
                keys,
                nicks,
            }))
        } else {
            Ok(None)
        }
    }

    pub fn merge_tran(&self, conn: &mut PgConnection, other: &Player) -> Result<(), Error> {
        conn.transaction::<(), Error, _>(|c| {
            self.merge(c, other)?;
            Ok(())
        })
    }

    pub fn merge(&self, conn: &mut PgConnection, other: &Player) -> Result<Player, Error> {
        if self.id == other.id {
            return Ok(self.clone());
        }
        let (target, dup) = if self.id < other.id {
            (self, other)
        } else {
            (other, self)
        };
        // copy keys and nicknames from dup to target
        diesel::update(player_key::table.filter(player_key::player_id.eq(dup.id)))
            .set(player_key::player_id.eq(target.id))
            .execute(conn)?;

        let target_nicks = target.get_nicks(conn)?;
        let dup_nicks = dup.get_nicks(conn)?;
        for dup_nick in dup_nicks {
            let mut contains = false;
            for target_nick in &target_nicks {
                if dup_nick.nickname == target_nick.nickname {
                    contains = true;
                    break;
                }
            }
            if !contains {
                diesel::update(&dup_nick)
                    .set(player_nickname::player_id.eq(target.id))
                    .execute(conn)?;
            }
        }

        let nicks = target.get_nicks(conn)?;
        let latest_nick = nicks.get(0);
        if let Some(n) = latest_nick {
            diesel::update(target)
                .set((
                    player::nickname.eq(n.nickname.clone()),
                    player::nickname_nocolors.eq(n.nickname_nocolors.clone()),
                ))
                .execute(conn)?;
        }

        // fix time record
        let target_records = target.get_all_maps_with_dead(conn)?;
        let dup_records = dup.get_all_maps_with_dead(conn)?;

        for (m, dup_r) in dup_records.into_iter() {
            if let Some(target_r) = target_records.get(&m) {
                if target_r.time > dup_r.time {
                    diesel::delete(rating_map::table)
                        .filter(rating_map::time_record_id.eq(target_r.id))
                        .execute(conn)?;
                    diesel::delete(target_r).execute(conn)?;
                    diesel::update(&dup_r)
                        .set(time_record::player_id.eq(target.id))
                        .execute(conn)?;
                } else {
                    diesel::delete(rating_map::table)
                        .filter(rating_map::time_record_id.eq(dup_r.id))
                        .execute(conn)?;
                    diesel::delete(&dup_r).execute(conn)?;
                }
            } else {
                diesel::update(&dup_r)
                    .set(time_record::player_id.eq(target.id))
                    .execute(conn)?;
            }
        }

        // reassign owned by dup to target
        diesel::delete(rating_total::table.filter(rating_total::player_id.eq(dup.id)))
            .execute(conn)?;
        diesel::update(speed_record::table.filter(speed_record::player_id.eq(dup.id)))
            .set(speed_record::player_id.eq(target.id))
            .execute(conn)?;
        diesel::update(
            speed_record_change::table.filter(speed_record_change::old_player_id.eq(dup.id)),
        )
        .set(speed_record_change::old_player_id.eq(target.id))
        .execute(conn)?;
        diesel::update(
            speed_record_change::table.filter(speed_record_change::new_player_id.eq(dup.id)),
        )
        .set(speed_record_change::new_player_id.eq(target.id))
        .execute(conn)?;
        diesel::update(results_change::table.filter(results_change::player_id.eq(dup.id)))
            .set(results_change::player_id.eq(target.id))
            .execute(conn)?;
        diesel::update(map_video::table.filter(map_video::player_id.eq(dup.id)))
            .set(map_video::player_id.eq(target.id))
            .execute(conn)?;
        diesel::update(demo_file::table.filter(demo_file::player_id.eq(dup.id)))
            .set(demo_file::player_id.eq(target.id))
            .execute(conn)?;
        // delete dup
        diesel::delete(player::table.filter(player::id.eq(dup.id))).execute(conn)?;
        Ok(target.clone())
    }

    pub fn admin_list(
        conn: &mut PgConnection,
        _search: Option<&str>,
    ) -> Result<Vec<(Player, Vec<PlayerKey>, Vec<PlayerNickname>)>, Error> {
        let rows_keys = player::table
            .inner_join(player_key::table)
            .load::<(Player, PlayerKey)>(conn)?;
        let rows_nicks = player::table
            .inner_join(player_nickname::table)
            .load::<(Player, PlayerNickname)>(conn)?;
        let mut hm_players = HashMap::new();
        let mut hm = HashMap::new();
        for (p, k) in rows_keys {
            let (keys, _nicks) = hm.entry(p.id).or_insert_with(|| (Vec::new(), Vec::new()));
            keys.push(k);
            hm_players.insert(p.id, p);
        }
        for (p, n) in rows_nicks {
            let (_keys, nicks) = hm.entry(p.id).or_insert_with(|| (Vec::new(), Vec::new()));
            nicks.push(n);
            hm_players.insert(p.id, p);
        }
        let mut res = Vec::new();
        for (p_id, (mut keys, mut nicks)) in hm.into_iter() {
            if let Some(p) = hm_players.remove(&p_id) {
                keys.sort_by(|a, b| b.last_used_at.cmp(&a.last_used_at));
                nicks.sort_by(|a, b| b.last_used_at.cmp(&a.last_used_at));
                res.push((p, keys, nicks))
            }
        }
        res.sort_by(|a, b| a.0.id.cmp(&b.0.id));
        Ok(res)
    }

    fn get_all_maps_with_dead(
        &self,
        conn: &mut PgConnection,
    ) -> Result<HashMap<String, TimeRecord>, Error> {
        let q = time_record::table
            .inner_join(map::table)
            .filter(time_record::player_id.eq(self.id));
        Ok(q.load::<(TimeRecord, Map)>(conn)?
            .into_iter()
            .map(|(tr, m)| (m.name, tr))
            .collect())
    }

    pub fn get_maps(
        &self,
        conn: &mut PgConnection,
        ordering: Option<PlayerMapsOrdering>,
    ) -> Result<Vec<PlayerTimeRecord>, Error> {
        let ordering = ordering.unwrap_or_default();
        let q = time_record::table
            .inner_join(map::table)
            .filter(map::server_id.is_not_null())
            .filter(time_record::player_id.eq(self.id));

        let sorted_q = player_maps_ord!(q, ordering);

        let ladder_maps = sorted_q.load::<(TimeRecord, Map)>(conn)?;
        let mut res = Vec::new();
        for (tr, m) in ladder_maps.into_iter() {
            res.push(PlayerTimeRecord {
                map: m,
                time_record: tr,
                player: self.clone(),
            });
        }
        Ok(res)
    }

    pub fn get_profile(
        &self,
        conn: &mut PgConnection,
        max_rows: i64,
        ratings: &[Rating],
    ) -> Result<PlayerProfile, Error> {
        let time_records_raw = time_record::table
            .inner_join(map::table)
            .inner_join(player::table)
            .filter(time_record::player_id.eq(self.id))
            .filter(map::server_id.is_not_null())
            .load::<(TimeRecord, Map, Player)>(conn)?;
        let time_records = utils::make_player_time_records(time_records_raw);

        let mut best_maps = Vec::new();

        for r in ratings {
            let best_records_raw = rating_map::table
                .inner_join(time_record::table.inner_join(map::table))
                .filter(rating_map::rating_id.eq(r.id))
                .filter(time_record::player_id.eq(self.id))
                .filter(map::server_id.is_not_null())
                .order_by(rating_map::score.desc())
                .limit(max_rows)
                .load::<(RatingMap, (TimeRecord, Map))>(conn)?;
            let best_records_for_this_r: Vec<_> = best_records_raw
                .into_iter()
                .map(|(rm, (tr, m))| {
                    (
                        PlayerTimeRecord {
                            map: m,
                            time_record: tr,
                            player: self.clone(),
                        },
                        rm.score,
                    )
                })
                .collect();
            best_maps.push((r.id, best_records_for_this_r))
        }

        let ratings = rating::table
            .inner_join(rating_total::table)
            .filter(rating_total::player_id.eq(self.id))
            .load(conn)?;
        let videos_number = map_video::table
            .filter(map_video::player_id.eq(self.id))
            .filter(map_video::youtube_code.is_not_null())
            .count()
            .get_result(conn)?;

        let recents = results_change::table
            .inner_join(results_changeset::table.inner_join(map::table))
            .filter(results_change::player_id.eq(self.id))
            .order_by(results_changeset::created_at.desc())
            .limit(20)
            .load::<(ResultsChange, (ResultsChangeset, Map))>(conn)?;
        Ok(PlayerProfile {
            ratings,
            time_records,
            best_maps,
            videos_number,
            recent_results: recents
                .into_iter()
                .map(|(rc, (rcset, m))| {
                    (
                        m,
                        rcset,
                        history::ResultChange::from_db(
                            rc.new_pos,
                            rc.new_time,
                            rc.old_pos,
                            rc.old_time,
                        )
                        .expect("invalid results change db data..."),
                    )
                })
                .collect(),
        })
    }

    pub fn mark_last_used(
        conn: &mut PgConnection,
        player_id: i32,
        crypto_idfp: &str,
        nickname: Option<&str>,
    ) -> Result<(), Error> {
        diesel::update(
            player_key::table
                .filter(player_key::player_id.eq(player_id))
                .filter(player_key::crypto_idfp.eq(crypto_idfp)),
        )
        .set(player_key::last_used_at.eq(utcnow()))
        .execute(conn)?;
        if let Some(n) = nickname {
            let nickname_nocolors = DPString::parse(n).to_none();
            diesel::update(player::table)
                .filter(player::id.eq(player_id))
                .set((
                    player::nickname.eq(n),
                    player::nickname_nocolors.eq(nickname_nocolors),
                ))
                .execute(conn)?;
            diesel::update(
                player_nickname::table
                    .filter(player_nickname::player_id.eq(player_id))
                    .filter(player_nickname::nickname.eq(n)),
            )
            .set(player_nickname::last_used_at.eq(utcnow()))
            .execute(conn)?;
        }
        Ok(())
    }
    pub fn load_all_players(
        conn: &mut PgConnection,
    ) -> Result<HashMap<String, (Self, HashSet<String>, HashSet<String>)>, Error> {
        let mut res = HashMap::new();
        let q = player::table
            .inner_join(player_key::table)
            .inner_join(player_nickname::table)
            .load::<(Player, PlayerKey, PlayerNickname)>(conn)?;
        for (p, k, n) in q {
            let entry =
                res.entry(k.crypto_idfp.clone())
                    .or_insert((p, HashSet::new(), HashSet::new()));
            entry.1.insert(k.crypto_idfp);
            entry.2.insert(n.nickname);
        }
        Ok(res)
    }

    pub fn load_all_players_brief(conn: &mut PgConnection) -> Result<HashMap<i32, Self>, Error> {
        let rows = player::table.load::<Player>(conn)?;
        let res: HashMap<_, _> = rows.into_iter().map(|x| (x.id, x)).collect();
        Ok(res)
    }

    fn insert_new_nickname(
        &self,
        conn: &mut PgConnection,
        nicknames: &mut HashSet<String>,
        nickname: &str,
        nickname_nocolors: &str,
    ) -> Result<(), Error> {
        diesel::insert_into(player_nickname::table)
            .values((
                player_nickname::player_id.eq(self.id),
                player_nickname::nickname.eq(nickname),
                player_nickname::nickname_nocolors.eq(&nickname_nocolors),
                player_nickname::created_at.eq(utcnow()),
                player_nickname::last_used_at.eq(utcnow()),
            ))
            .execute(conn)?;
        nicknames.insert(nickname.to_string());
        Ok(())
    }

    fn insert_new_key(
        &self,
        conn: &mut PgConnection,
        keys: &mut HashSet<String>,
        key: &str,
    ) -> Result<(), Error> {
        diesel::insert_into(player_key::table)
            .values((
                player_key::player_id.eq(self.id),
                player_key::crypto_idfp.eq(key),
                player_key::created_at.eq(utcnow()),
                player_key::last_used_at.eq(utcnow()),
            ))
            .execute(conn)?;
        keys.insert(key.to_string());
        Ok(())
    }

    fn update(
        &self,
        conn: &mut PgConnection,
        nicknames: &mut HashSet<String>,
        new_nickname: &str,
    ) -> Result<Self, Error> {
        let nickname_nocolors = DPString::parse(new_nickname).to_none();
        if new_nickname != self.nickname {
            if !nicknames.contains(new_nickname) {
                self.insert_new_nickname(conn, nicknames, new_nickname, &nickname_nocolors)?;
            }
            Ok(self.clone())
        } else {
            Ok(self.clone())
        }
    }

    pub fn get_or_create<'a>(
        conn: &mut PgConnection,
        all_players: &'a mut HashMap<String, (Self, HashSet<String>, HashSet<String>)>,
        crypto_idfp: &str,
        maybe_nickname: Option<&str>,
    ) -> Result<&'a (Self, HashSet<String>, HashSet<String>), Error> {
        let maybe_player = all_players.remove_entry(crypto_idfp);
        let (key, val) = match maybe_player {
            Some((key, (p, keys, mut nicks))) => {
                if let Some(new_nick) = maybe_nickname {
                    p.update(conn, &mut nicks, new_nick)?;
                }
                (key, (p, keys, nicks))
            }
            None => {
                let nickname = maybe_nickname.unwrap_or("^1Unregistered Player^7");
                let nickname_nocolors = DPString::parse(nickname).to_none();
                let mut keys = HashSet::new();
                let mut nicks = HashSet::new();
                let p = diesel::insert_into(player::table)
                    .values((
                        player::nickname.eq(nickname),
                        player::nickname_nocolors.eq(&nickname_nocolors),
                        player::created_at.eq(utcnow()),
                    ))
                    .get_result::<Self>(conn)?;
                p.insert_new_nickname(conn, &mut nicks, nickname, &nickname_nocolors)?;
                p.insert_new_key(conn, &mut keys, crypto_idfp)?;
                (crypto_idfp.to_string(), (p, keys, nicks))
            }
        };
        all_players.insert(key, val);
        Ok(all_players.get(crypto_idfp).unwrap())
    }
}

#[derive(Debug, Identifiable, Queryable, PartialEq, Clone)]
#[diesel(table_name = player_key)]
pub struct PlayerKey {
    pub id: i32,
    pub player_id: i32,
    pub crypto_idfp: String,
    pub created_at: NaiveDateTime,
    pub last_used_at: NaiveDateTime,
}

impl PlayerKey {
    pub fn load_dup_db(
        conn: &mut PgConnection,
    ) -> Result<HashMap<server_db::CryptoIDFP, server_db::CryptoIDFP>, Error> {
        let mut res = HashMap::new();
        let rows = player_key::table
            .order((player_key::player_id, player_key::last_used_at.desc()))
            .load::<PlayerKey>(conn)?;
        println!("{}", rows.len());
        let mut current_player_id: Option<i32> = None;
        let mut current_key: Option<String> = None;
        for row in rows {
            if current_player_id != Some(row.player_id) {
                // We encountered a new player
                current_player_id.replace(row.player_id);
                current_key.replace(row.crypto_idfp.clone());
            } else if let Some(k) = current_key.take() {
                // Same player, dump keys
                if k != row.crypto_idfp {
                    res.insert(
                        server_db::CryptoIDFP(row.crypto_idfp),
                        server_db::CryptoIDFP(k),
                    );
                }
            }
        }
        Ok(res)
    }
}

#[derive(Debug, Identifiable, Queryable, PartialEq)]
#[diesel(table_name = player_nickname)]
pub struct PlayerNickname {
    pub id: i32,
    pub player_id: i32,
    pub nickname: String,
    pub nickname_nocolors: String,
    pub created_at: NaiveDateTime,
    pub last_used_at: NaiveDateTime,
}

#[derive(Debug, Identifiable, Queryable, PartialEq, Clone)]
#[diesel(table_name = time_record)]
pub struct TimeRecord {
    pub id: i32,
    pub player_id: i32,
    pub time: Decimal,
    pub created_at: NaiveDateTime,
    pub map_id: i32,
    pub pos: i16,
    pub reltime: Decimal,
    pub checkpoints: Option<Vec<Option<Decimal>>>,
    pub nickname: Option<String>,
    pub nickname_nocolors: Option<String>,
    pub player_key_id: Option<i32>,
    pub server_id: Option<i32>,
    pub ip_address: Option<IpNetwork>,
    pub top_speed: Option<f64>,
    pub average_speed: Option<f64>,
    pub start_speed: Option<f64>,
    pub strafe_percentage: Option<f64>,
    pub checkpoint_house_synced_at: Option<NaiveDateTime>,
    pub checkpoint_house_timestamp: Option<NaiveDateTime>,
}

#[derive(Debug, Clone)]
pub struct PlayerTimeRecord {
    pub time_record: TimeRecord,
    pub map: Map,
    pub player: Player,
}

impl TimeRecord {
    pub fn get_records_not_synced_with_checkpoint_house(
        conn: &mut PgConnection,
    ) -> Result<Vec<(TimeRecord, Player, Map)>, Error> {
        Ok(time_record::table
            .inner_join(player::table)
            .inner_join(map::table)
            .filter(time_record::checkpoint_house_synced_at.is_null())
            .load::<(TimeRecord, Player, Map)>(conn)?)
    }
    pub fn load_all_records_into_results(
        conn: &mut PgConnection,
    ) -> Result<
        (
            HashMap<String, history::Results>,
            HashMap<String, HashMap<i32, TimeRecord>>,
        ),
        Error,
    > {
        let mut hm: HashMap<String, history::Results> = HashMap::new();
        let mut hm_recs = HashMap::new();
        let recs = time_record::table
            .inner_join(map::table)
            .filter(map::server_id.is_not_null())
            .order_by(time_record::time.asc())
            .load::<(TimeRecord, Map)>(conn)?;
        let mut speed_recs = SpeedRecord::load_all_records(conn)?;
        for (rec, map) in recs {
            let r = (rec.player_id, rec.pos, rec.time);
            match hm.entry(map.name.clone()) {
                Entry::Occupied(mut e) => {
                    e.get_mut().time_records.push(r);
                }
                Entry::Vacant(e) => {
                    let speed_record = speed_recs.remove(&map.id);
                    let time_records = vec![r];
                    e.insert(history::Results {
                        speed_record: speed_record.map(|x| (x.player_id, x.speed)),
                        time_records,
                    });
                }
            }
            hm_recs
                .entry(map.name)
                .or_insert_with(HashMap::new)
                .insert(rec.player_id, rec);
        }
        Ok((hm, hm_recs))
    }

    pub fn load_time_record_for_render(
        conn: &mut PgConnection,
        id: i32,
    ) -> Result<(Self, Player, Map), Error> {
        Ok(time_record::table
            .inner_join(player::table)
            .inner_join(map::table)
            .filter(time_record::id.eq(id))
            .first(conn)?)
    }

    pub fn save_results(
        conn: &mut PgConnection,
        map: &Map,
        results: &history::Results,
        mut current_time_records: HashMap<i32, TimeRecord>,
        server_db: &xonotic_db::ServerDB,
        speed_to_crypto_idfp: &HashMap<(i32, Decimal), server_db::CryptoIDFP>,
        time_to_crypto_idfp: &HashMap<(i32, Decimal), server_db::CryptoIDFP>,
    ) -> Result<Map, Error> {
        // get old speedrecord
        if let Some(sr) = results.speed_record {
            let maybe_new_speed_rec = SpeedRecord::update(conn, map.id, sr.0, sr.1)?;
            if maybe_new_speed_rec.is_some() {
                if let Some(crypto_idfp) = speed_to_crypto_idfp.get(&(sr.0, sr.1)) {
                    let nickname = server_db.players.get(crypto_idfp).map(|x| x.0.as_str());
                    Player::mark_last_used(conn, sr.0, &crypto_idfp.0, nickname)?;
                }
            }
        }
        let mut max_pos = None;
        let mut best_time = None;
        let mut second_best_time = None;
        for (_player_id, _pos, time) in results.time_records.iter() {
            if best_time.is_none() {
                best_time.replace(time);
            } else if second_best_time.is_none() {
                second_best_time.replace(time);
                break;
            }
        }
        for (player_id, pos, time) in results.time_records.iter() {
            max_pos.replace(*pos);
            if let Some(best_time) = best_time {
                let reltime = if *pos == 1 {
                    time / second_best_time.unwrap_or(best_time)
                } else {
                    time / best_time
                };
                if let Some(existing_r) = current_time_records.remove(player_id) {
                    if *time != existing_r.time {
                        diesel::update(&existing_r)
                            .set((time_record::checkpoint_house_synced_at
                                .eq(None::<chrono::NaiveDateTime>),))
                            .execute(conn)?;
                    }
                    if *time < existing_r.time {
                        diesel::update(&existing_r)
                            .set((
                                time_record::time.eq(time),
                                time_record::pos.eq(pos),
                                time_record::created_at.eq(utcnow()),
                                time_record::reltime.eq(reltime),
                            ))
                            .execute(conn)?;
                        if let Some(crypto_idfp) = time_to_crypto_idfp.get(&(*player_id, *time)) {
                            let nickname = server_db.players.get(crypto_idfp).map(|x| x.0.as_str());
                            Player::mark_last_used(conn, *player_id, &crypto_idfp.0, nickname)?;
                        }
                    } else if *time > existing_r.time {
                        diesel::update(&existing_r)
                            .set((
                                time_record::time.eq(time),
                                time_record::pos.eq(pos),
                                time_record::created_at.eq(utcnow()),
                                time_record::reltime.eq(reltime),
                            ))
                            .execute(conn)?;
                    } else {
                        diesel::update(&existing_r)
                            .set((time_record::pos.eq(pos), time_record::reltime.eq(reltime)))
                            .execute(conn)?;
                    }
                } else {
                    diesel::insert_into(time_record::table)
                        .values((
                            time_record::map_id.eq(map.id),
                            time_record::pos.eq(pos),
                            time_record::time.eq(time),
                            time_record::player_id.eq(*player_id),
                            time_record::created_at.eq(utcnow()),
                            time_record::reltime.eq(reltime),
                        ))
                        .execute(conn)?;
                    if let Some(crypto_idfp) = time_to_crypto_idfp.get(&(*player_id, *time)) {
                        let nickname = server_db.players.get(crypto_idfp).map(|x| x.0.as_str());
                        Player::mark_last_used(conn, *player_id, &crypto_idfp.0, nickname)?;
                    }
                }
            }
        }

        let recs_to_delete: Vec<_> = current_time_records.into_values().map(|x| x.id).collect();
        println!("TO DELETE {}: {:?}", map.name, recs_to_delete);

        diesel::delete(rating_map::table)
            .filter(rating_map::time_record_id.eq_any(&recs_to_delete))
            .execute(conn)?;
        diesel::delete(time_record::table)
            .filter(time_record::id.eq_any(&recs_to_delete))
            .execute(conn)?;
        let max_pos = max_pos.unwrap_or(0);

        Ok(diesel::update(map)
            .set(map::max_pos.eq(max_pos))
            .get_result::<Map>(conn)?)
    }

    pub fn load_all_records_for_rating(
        conn: &mut PgConnection,
        rating: &Rating,
    ) -> Result<Vec<Vec<PlayerTimeRecord>>, Error> {
        let records = time_record::table
            .inner_join(player::table)
            .inner_join(map::table)
            .filter(map::server_id.eq_any(&rating.servers))
            .load::<(Self, Player, Map)>(conn)?;
        let mut maps = HashMap::new();
        for (tr, p, m) in records {
            let map_records = maps.entry(m.id).or_insert_with(Vec::new);
            map_records.push(PlayerTimeRecord {
                map: m,
                time_record: tr,
                player: p,
            });
        }
        let maps_ordered: Vec<_> = maps
            .values_mut()
            .map(|results| {
                results.sort_by(|a, b| a.time_record.time.cmp(&b.time_record.time));
                results.to_vec()
            })
            .collect();
        Ok(maps_ordered)
    }

    pub fn get_for_map(conn: &mut PgConnection, map: &Map) -> Result<Vec<PlayerTimeRecord>, Error> {
        let rows = time_record::table
            .inner_join(player::table)
            .filter(time_record::map_id.eq(map.id))
            .order((time_record::time.asc(), time_record::id))
            .load::<(TimeRecord, Player)>(conn)?;
        let mut res: Vec<PlayerTimeRecord> = Vec::new();
        for (tr, p) in rows {
            res.push(PlayerTimeRecord {
                map: map.clone(),
                time_record: tr,
                player: p,
            });
        }
        Ok(res)
    }

    // pub fn cleanup_records_all(
    //     conn: &mut PgConnection,
    //     map_records: &mut Vec<Self>,
    //     map_players: &HashSet<i32>,
    // ) -> Result<(), Error> {
    //     let mut to_remove = Vec::new();
    //     for (i, rec) in map_records.iter().enumerate() {
    //         if !map_players.contains(&rec.player_id) {
    //             to_remove.push(i);
    //             diesel::delete(
    //                 time_record_position::table
    //                     .filter(time_record_position::time_record_id.eq(rec.id)),
    //             )
    //             .execute(conn)?;
    //             diesel::delete(rec).execute(conn)?;
    //         }
    //     }
    //     for (num_removed, i) in to_remove.iter().enumerate() {
    //         map_records.remove(*i - num_removed);
    //     }
    //     Ok(())
    // }

    // pub fn cleanup_records(
    //     conn: &mut PgConnection,
    //     map_records: &mut Vec<Self>,
    //     map_players: &HashSet<i32>,
    //     time: rust_decimal::Decimal,
    // ) -> Result<(), Error> {
    //     // Delete records in the site DB which are better than the new time.
    //     // This should trigger in two cases:
    //     // 1. player accounts were merged in the site DB but weren't merged in the server DB
    //     // 2. record was deleted in the server DB for whatever reason
    //     // Both events should be VERY rare on a properly managed server-site pair.
    //     let new_time = decimal_to_bigdecimal(time);
    //     let mut to_remove = Vec::new();
    //     for (i, rec) in map_records.iter().enumerate() {
    //         if (!map_players.contains(&rec.player_id)) && (rec.time < new_time) {
    //             to_remove.push(i);
    //             diesel::delete(
    //                 time_record_position::table
    //                     .filter(time_record_position::time_record_id.eq(rec.id)),
    //             )
    //             .execute(conn)?;
    //             diesel::delete(rec).execute(conn)?;
    //         }
    //     }
    //     for (num_removed, i) in to_remove.iter().enumerate() {
    //         map_records.remove(*i - num_removed);
    //     }
    //     Ok(())
    // }

    pub fn delete(&self, conn: &mut PgConnection) -> Result<(), Error> {
        diesel::delete(time_record::table.filter(time_record::id.eq(self.id))).execute(conn)?;
        Ok(())
    }
}

#[derive(Debug, Identifiable, Queryable, PartialEq)]
#[diesel(table_name = speed_record)]
pub struct SpeedRecord {
    pub id: i32,
    pub player_id: i32,
    pub speed: Decimal,
    pub created_at: NaiveDateTime,
    pub map_id: i32,
}

impl SpeedRecord {
    pub fn get_for_map(
        conn: &mut PgConnection,
        map_id: i32,
    ) -> Result<Option<(Self, Player)>, Error> {
        let rec = speed_record::table
            .inner_join(player::table)
            .filter(speed_record::map_id.eq(map_id))
            .first::<(Self, Player)>(conn)
            .optional()?;
        Ok(rec.map(|(sr, p)| (sr, p)))
    }

    pub fn load_all_records(conn: &mut PgConnection) -> Result<HashMap<i32, Self>, Error> {
        let mut res = HashMap::new();
        let recs = speed_record::table
            .inner_join(map::table)
            .filter(map::server_id.is_not_null())
            .load::<(Self, Map)>(conn)?;
        for (sr, m) in recs {
            res.insert(m.id, sr);
        }
        Ok(res)
    }

    pub fn load_all_records_for_rating(
        conn: &mut PgConnection,
        rating: &Rating,
    ) -> Result<HashMap<i32, Self>, Error> {
        let mut res = HashMap::new();
        let recs = speed_record::table
            .inner_join(map::table)
            .filter(map::server_id.eq_any(&rating.servers))
            .filter(map::server_id.is_not_null())
            .load::<(Self, Map)>(conn)?;
        for (sr, m) in recs {
            res.insert(m.id, sr);
        }
        Ok(res)
    }

    pub fn update(
        conn: &mut PgConnection,
        map_id: i32,
        player_id: i32,
        speed: rust_decimal::Decimal,
    ) -> Result<Option<Self>, Error> {
        let maybe_prev_record = SpeedRecord::get_for_map(conn, map_id)?;
        let mut new_record = None;
        match maybe_prev_record {
            Some((prev_record, _)) => {
                if (prev_record.player_id != player_id) || (prev_record.speed != speed) {
                    new_record = Some(
                        diesel::update(&prev_record)
                            .set((
                                speed_record::player_id.eq(player_id),
                                speed_record::speed.eq(speed),
                                speed_record::created_at.eq(utcnow()),
                            ))
                            .get_result(conn)?,
                    );
                }
            }
            None => {
                new_record = Some(
                    diesel::insert_into(speed_record::table)
                        .values((
                            speed_record::map_id.eq(map_id),
                            speed_record::player_id.eq(player_id),
                            speed_record::speed.eq(speed),
                            speed_record::created_at.eq(utcnow()),
                        ))
                        .get_result(conn)?,
                );
            }
        }
        Ok(new_record)
    }
}

#[derive(Debug, Queryable, PartialEq, Identifiable, Clone)]
#[diesel(table_name = rating)]
pub struct Rating {
    pub id: i32,
    pub name: String,
    pub servers: Vec<Option<i32>>,
    pub max_pos: i32,
}

#[derive(Debug, Queryable, PartialEq, Clone)]
#[diesel(table_name = rating_map)]
pub struct RatingMap {
    pub rating_id: i32,
    pub time_record_id: i32,
    pub score: Decimal,
    pub last_updated: NaiveDateTime,
}

#[derive(Debug, Queryable, PartialEq, Clone)]
#[diesel(table_name = rating_total)]
pub struct RatingTotal {
    pub rating_id: i32,
    pub player_id: i32,
    pub score: Decimal,
    pub map_count: i32,
    pub pos: i32,
}

impl Rating {
    pub fn get_by_id(conn: &mut PgConnection, id: i32) -> Result<Rating, Error> {
        Ok(rating::table.filter(rating::id.eq(id)).first(conn)?)
    }

    pub fn get_for_server(conn: &mut PgConnection, server_id: i32) -> Result<Vec<Rating>, Error> {
        Ok(rating::table
            .filter(rating::servers.contains(vec![server_id]))
            .load::<Rating>(conn)?)
    }

    pub fn get_points_for_time_records(
        conn: &mut PgConnection,
        time_record_ids: &[i32],
    ) -> Result<HashMap<(i32, i32), Decimal>, Error> {
        let rows = rating_map::table
            .filter(rating_map::time_record_id.eq_any(time_record_ids))
            .load::<RatingMap>(conn)?;
        Ok(rows
            .into_iter()
            .map(|rm| ((rm.rating_id, rm.time_record_id), rm.score))
            .collect())
    }

    pub fn get_points_for_all_time_records(
        &self,
        conn: &mut PgConnection,
    ) -> Result<HashMap<(i32, i32), Decimal>, Error> {
        let rows = rating_map::table
            .filter(rating_map::rating_id.eq(self.id))
            .load::<RatingMap>(conn)?;
        Ok(rows
            .into_iter()
            .map(|rm| ((rm.rating_id, rm.time_record_id), rm.score))
            .collect())
    }

    pub fn get_points(
        conn: &mut PgConnection,
        id: i32,
    ) -> Result<Vec<(Player, RatingTotal)>, Error> {
        let rows = rating_total::table
            .inner_join(rating::table)
            .inner_join(player::table)
            .filter(rating::id.eq(id))
            .order_by(rating_total::score.desc())
            .load::<(RatingTotal, Rating, Player)>(conn)?;
        Ok(rows.into_iter().map(|(rt, _r, p)| (p, rt)).collect())
    }
    pub fn get_all(conn: &mut PgConnection) -> Result<Vec<Rating>, Error> {
        Ok(rating::table.order_by(rating::id.asc()).load(conn)?)
    }

    fn load_current_overalls(
        &self,
        conn: &mut PgConnection,
    ) -> Result<HashMap<Player, (ladders::Score, usize)>, Error> {
        let rows = rating_total::table
            .inner_join(player::table)
            .filter(rating_total::rating_id.eq(self.id))
            .order_by(rating_total::score.desc())
            .load::<(RatingTotal, Player)>(conn)?;
        Ok(rows
            .into_iter()
            .map(|(t, p)| {
                (
                    p,
                    (
                        ladders::Score::Float(t.score.to_f64().unwrap()),
                        t.map_count as usize,
                    ),
                )
            })
            .collect())
    }

    pub fn get_outdated_maps(conn: &mut PgConnection) -> Result<Vec<i32>, Error> {
        let rows = rating_map::table
            .inner_join(time_record::table)
            .order(rating_map::last_updated.asc())
            .limit(200)
            .load::<(RatingMap, TimeRecord)>(conn)?;
        let map_ids: HashSet<i32> = rows.into_iter().map(|(_, tr)| tr.map_id).collect();
        Ok(map_ids.into_iter().collect())
    }

    pub fn update(&self, conn: &mut PgConnection, include_maps: &[i32]) -> Result<(), Error> {
        let time_records = TimeRecord::load_all_records_for_rating(conn, self)?;
        let speed_records = SpeedRecord::load_all_records_for_rating(conn, self)?;
        self.update_inner(conn, include_maps, &time_records, &speed_records)
    }

    pub fn update_all(&self, conn: &mut PgConnection) -> Result<(), Error> {
        let maps = map::table
            .filter(map::server_id.eq_any(&self.servers))
            .load::<Map>(conn)?;
        let time_records = TimeRecord::load_all_records_for_rating(conn, self)?;
        let speed_records = SpeedRecord::load_all_records_for_rating(conn, self)?;
        self.update_inner(
            conn,
            &maps.into_iter().map(|x| x.id).collect::<Vec<i32>>(),
            &time_records,
            &speed_records,
        )
    }

    fn update_inner(
        &self,
        conn: &mut PgConnection,
        include_maps: &[i32],
        time_records: &[Vec<PlayerTimeRecord>],
        speed_records: &HashMap<i32, SpeedRecord>,
    ) -> Result<(), Error> {
        let overalls = self.load_current_overalls(conn)?;
        let mut map_scores = Vec::new();
        let map_algo = get_default_map_algo();
        let sum_algo = get_default_sum_algo();
        let points = self.get_points_for_all_time_records(conn)?;
        for map_records in time_records.iter() {
            if !map_records.is_empty() {
                let speed_record = speed_records.get(&map_records[0].time_record.map_id);
                let mut scores;
                if include_maps.contains(&map_records[0].time_record.map_id) {
                    scores =
                        map_algo.compute_map_scores(map_records, speed_record, Some(&overalls));
                    let now = utcnow();
                    for (ptr, score) in &scores {
                        diesel::insert_into(rating_map::table)
                            .values((
                                rating_map::rating_id.eq(self.id),
                                rating_map::time_record_id.eq(ptr.time_record.id),
                                rating_map::score.eq(score.to_fixed()),
                                rating_map::last_updated.eq(now),
                            ))
                            .on_conflict((rating_map::rating_id, rating_map::time_record_id))
                            .do_update()
                            .set((
                                rating_map::score.eq(score.to_fixed()),
                                rating_map::last_updated.eq(now),
                            ))
                            .execute(conn)?;
                    }
                } else {
                    scores = Vec::new();
                    for i in map_records.iter() {
                        let score = points.get(&(self.id, i.time_record.id));
                        if let Some(score) = score {
                            scores.push((i.clone(), Score::Fixed(*score)))
                        }
                    }
                }
                map_scores.push(scores);
            }
        }
        let new_overalls = sum_algo.compute_total_score(&map_scores);
        let mut sorted_overalls: Vec<_> = new_overalls.into_iter().collect();
        sorted_overalls.sort_by(|(_, a), (_, b)| b.0.cmp(&a.0));
        let total = sorted_overalls.len();
        for (pos, (player, (score, map_count))) in sorted_overalls.iter().enumerate() {
            diesel::insert_into(rating_total::table)
                .values((
                    rating_total::rating_id.eq(self.id),
                    rating_total::player_id.eq(player.id),
                    rating_total::score.eq(score.to_fixed()),
                    rating_total::map_count.eq((*map_count) as i32),
                    rating_total::pos.eq(pos as i32 + 1),
                ))
                .on_conflict((rating_total::rating_id, rating_total::player_id))
                .do_update()
                .set((
                    rating_total::score.eq(score.to_fixed()),
                    rating_total::map_count.eq((*map_count) as i32),
                    rating_total::pos.eq(pos as i32 + 1),
                ))
                .execute(conn)?;
        }
        diesel::update(rating::table.filter(rating::id.eq(self.id)))
            .set(rating::max_pos.eq(total as i32))
            .execute(conn)?;
        Ok(())
    }
}

#[derive(Debug, Identifiable, Queryable, PartialEq)]
#[diesel(table_name = user)]
pub struct User {
    id: i32,
    username: String,
    email: Option<String>,
    player_id: i32,
    is_admin: bool,
}

#[derive(Debug, Identifiable, Queryable, PartialEq)]
#[diesel(table_name = heatmap_history)]
pub struct HeatmapHistory {
    pub id: i32,
    pub player_id: i32,
    pub created_at: NaiveDateTime,
    pub data: serde_json::Value,
}

impl HeatmapHistory {
    pub fn save(conn: &mut PgConnection) -> Result<(), Error> {
        let now = utcnow();
        let players = player::table.load::<Player>(conn)?;
        let mut inserts = Vec::new();
        let ratings = Rating::get_all(conn)?;
        for p in players {
            let profile = p.get_profile(conn, 0, &ratings)?;
            let data = heatmap_data(&profile.time_records);
            inserts.push((
                heatmap_history::player_id.eq(p.id),
                heatmap_history::created_at.eq(now),
                heatmap_history::data.eq(serde_json::json!(data)),
            ))
        }
        for chunk in inserts.chunks(MAX_PG_INSERTS / 10) {
            diesel::insert_into(heatmap_history::table)
                .values(chunk)
                .execute(conn)?;
        }
        Ok(())
    }
}

#[derive(Debug, Serialize)]
pub struct RatingHistoryChartDataSet {
    label: String,
    data_points: Vec<f64>,
    data_pos: Vec<i32>,
}

#[derive(Debug, Serialize)]
pub struct RatingHistoryChartData {
    labels: Vec<String>,
    datasets: Vec<RatingHistoryChartDataSet>,
}

#[derive(serde::Deserialize, Serialize, Debug, PartialEq)]
pub struct RatingHistoryMap {
    m: i32,
    s: serde_json::Number,
}

#[derive(Debug, Queryable, PartialEq)]
#[diesel(table_name = rating_history)]
pub struct RatingHistory {
    pub player_id: i32,
    pub created_at: NaiveDateTime,
    pub score: Decimal,
    pub map_count: i32,
    pub pos: i32,
    pub max_pos: i32,
    pub maps: Option<serde_json::Value>,
    pub rating_id: i32,
}

impl RatingHistory {
    pub fn save(conn: &mut PgConnection) -> Result<(), Error> {
        let now = utcnow();
        let mut inserts = Vec::new();
        let rows = rating::table
            .inner_join(rating_total::table)
            .load::<(Rating, RatingTotal)>(conn)?;
        let mut map_scores: HashMap<i32, Vec<serde_json::Value>> = HashMap::new();
        for (tr, m, (rm, r)) in time_record::table
            .inner_join(map::table)
            .inner_join(rating_map::table.inner_join(rating::table))
            .filter(map::server_id.is_not_null())
            .load::<(TimeRecord, Map, (RatingMap, Rating))>(conn)?
        {
            let mut map = serde_json::map::Map::new();
            map.insert(
                String::from("m"),
                serde_json::Value::Number(serde_json::Number::from(m.id)),
            );
            map.insert(
                String::from("r"),
                serde_json::Value::Number(serde_json::Number::from(r.id)),
            );
            map.insert(
                String::from("s"),
                serde_json::Value::Number(
                    serde_json::Number::from_str(&rm.score.to_string()).unwrap(),
                ),
            );
            map_scores
                .entry(tr.player_id)
                .or_insert_with(Vec::new)
                .push(serde_json::Value::Object(map))
        }
        for (r, rt) in rows {
            inserts.push((
                rating_history::player_id.eq(rt.player_id),
                rating_history::created_at.eq(now),
                rating_history::score.eq(rt.score),
                rating_history::map_count.eq(rt.map_count),
                rating_history::pos.eq(rt.pos),
                rating_history::max_pos.eq(r.max_pos),
                rating_history::rating_id.eq(r.id),
                rating_history::maps.eq(map_scores
                    .remove(&rt.player_id)
                    .map(serde_json::Value::Array)),
            ));
        }
        for chunk in inserts.chunks(MAX_PG_INSERTS / 15) {
            diesel::insert_into(rating_history::table)
                .values(chunk)
                .execute(conn)?;
        }
        Ok(())
    }

    pub fn get_short(
        conn: &mut PgConnection,
        rating_id: i32,
    ) -> Result<HashMap<Player, Vec<RatingHistory>>, Error> {
        let mut res = HashMap::new();
        let now = utcnow();
        let durations = vec![Duration::days(1), Duration::days(7), Duration::days(30)];
        let one_day = Duration::days(1);
        for d in durations {
            let rows = rating_history::table
                .inner_join(player::table)
                .filter(rating_history::rating_id.eq(rating_id))
                .filter(rating_history::created_at.gt(now - d))
                .filter(rating_history::created_at.lt(now - d + one_day))
                .distinct_on(rating_history::player_id)
                .load::<(RatingHistory, Player)>(conn)?;
            for (rating, player) in rows {
                let e = res.entry(player).or_insert_with(Vec::new);
                e.push(rating);
            }
        }
        Ok(res)
    }

    pub fn get_chart_data(
        conn: &mut PgConnection,
        player_ids: &[i32],
        from: Option<NaiveDateTime>,
        to: Option<NaiveDateTime>,
    ) -> Result<Option<RatingHistoryChartData>, Error> {
        let mut datasets = Vec::new();
        let mut labels = Vec::new();
        let ratings = Rating::get_all(conn)?;
        for id in player_ids {
            let from = from.unwrap_or_else(|| {
                NaiveDateTime::new(
                    NaiveDate::from_ymd_opt(1970, 1, 1).unwrap(),
                    NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
                )
            });
            let to = to.unwrap_or_else(utcnow);
            for rating in ratings.iter() {
                let rows = rating_history::table
                    .filter(rating_history::player_id.eq(id))
                    .filter(rating_history::rating_id.eq(rating.id))
                    .filter(rating_history::created_at.ge(from))
                    .filter(rating_history::created_at.le(to))
                    .order_by(rating_history::created_at)
                    .load::<RatingHistory>(conn)?;
                if rows.is_empty() {
                    continue;
                }
                let mut dataset = RatingHistoryChartDataSet {
                    label: format!("Rating: {}", rating.name),
                    data_points: Vec::with_capacity(rows.len()),
                    data_pos: Vec::with_capacity(rows.len()),
                };
                let need_to_update_labels = labels.len() < rows.len();
                if need_to_update_labels {
                    labels.clear();
                }
                for r in rows {
                    dataset.data_points.push(r.score.to_f64().unwrap());
                    dataset.data_pos.push(r.pos);
                    if need_to_update_labels {
                        labels.push(format!("{}", r.created_at.format("%Y-%m-%d")))
                    }
                }
                datasets.push(dataset);
            }
        }
        if datasets.is_empty() {
            Ok(None)
        } else {
            Ok(Some(RatingHistoryChartData { labels, datasets }))
        }
    }

    pub fn compare(&self, other: &RatingTotal) -> (Decimal, i32) {
        (other.score - self.score, self.pos - other.pos)
    }
}

#[derive(Debug, Identifiable, Queryable, Clone)]
#[diesel(table_name = map_update)]
pub struct MapUpdate {
    pub id: i32,
    pub map_id: i32,
    pub server_id: i32,
    pub deleted: bool,
    pub created_at: NaiveDateTime,
}

impl MapUpdate {
    pub fn get_news_feed(conn: &mut PgConnection) -> Result<Vec<(MapUpdate, Map, Server)>, Error> {
        let rows = map_update::table
            .inner_join(map::table)
            .inner_join(server::table)
            .order_by(map_update::created_at.desc())
            .load::<(MapUpdate, Map, Server)>(conn)?;
        Ok(rows)
    }
    pub fn insert_new(
        conn: &mut PgConnection,
        map_id: i32,
        server_id: i32,
        deleted: bool,
    ) -> Result<(), Error> {
        diesel::insert_into(map_update::table)
            .values((
                map_update::map_id.eq(map_id),
                map_update::server_id.eq(server_id),
                map_update::deleted.eq(deleted),
                map_update::created_at.eq(utcnow()),
            ))
            .execute(conn)?;
        Ok(())
    }
}

#[derive(Debug, Identifiable, Queryable, Clone)]
#[diesel(table_name = results_changeset)]
pub struct ResultsChangeset {
    pub id: i32,
    pub map_id: i32,
    pub created_at: NaiveDateTime,
    pub max_pos: i16,
    pub best_time: Decimal,
    pub server_id: i32,
}

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = results_change)]
pub struct ResultsChange {
    pub id: i32,
    pub results_changeset_id: i32,
    pub player_id: i32,
    pub new_time: Option<Decimal>,
    pub old_time: Option<Decimal>,
    pub new_pos: Option<i16>,
    pub old_pos: Option<i16>,
}

#[derive(Debug, Identifiable, Queryable)]
#[diesel(table_name = speed_record_change)]
pub struct SpeedRecordChange {
    pub id: i32,
    pub results_changeset_id: i32,
    pub new_player_id: i32,
    pub old_player_id: Option<i32>,
    pub new_speed: Decimal,
    pub old_speed: Option<Decimal>,
}

impl ResultsChangeset {
    fn delete_if_empty(conn: &mut PgConnection, changeset_id: i32) -> Result<(), Error> {
        let changes = results_change::table
            .filter(results_change::results_changeset_id.eq(changeset_id))
            .count()
            .get_result::<i64>(conn)?;
        let s_changes = speed_record_change::table
            .filter(speed_record_change::results_changeset_id.eq(changeset_id))
            .count()
            .get_result::<i64>(conn)?;
        if (changes == 0) && (s_changes == 0) {
            diesel::delete(results_changeset::table.filter(results_changeset::id.eq(changeset_id)))
                .execute(conn)?;
        }
        Ok(())
    }

    pub fn revert_speed_change(conn: &mut PgConnection, change_id: i32) -> Result<(), Error> {
        let (change, changeset) = speed_record_change::table
            .inner_join(results_changeset::table)
            .filter(speed_record_change::id.eq(change_id))
            .first::<(SpeedRecordChange, ResultsChangeset)>(conn)?;
        let current = speed_record::table
            .filter(speed_record::map_id.eq(changeset.map_id))
            .first::<SpeedRecord>(conn)?;
        if (current.speed == change.new_speed) && (current.player_id == change.new_player_id) {
            if let (Some(old_player_id), Some(old_speed)) = (change.old_player_id, change.old_speed)
            {
                let prev = results_changeset::table
                    .inner_join(speed_record_change::table)
                    .filter(results_changeset::map_id.eq(changeset.map_id))
                    .filter(speed_record_change::new_player_id.eq(old_player_id))
                    .filter(speed_record_change::new_speed.eq(old_speed))
                    .first::<(ResultsChangeset, SpeedRecordChange)>(conn)
                    .optional()?;
                let prev_date = match prev {
                    Some((cs, _)) => cs.created_at,
                    _ => utcnow(),
                };
                diesel::update(speed_record::table.filter(speed_record::id.eq(current.id)))
                    .set((
                        speed_record::player_id.eq(old_player_id),
                        speed_record::speed.eq(old_speed),
                        speed_record::created_at.eq(prev_date),
                    ))
                    .execute(conn)?;
            } else {
                diesel::delete(speed_record::table.filter(speed_record::id.eq(current.id)))
                    .execute(conn)?;
            }
        }
        diesel::delete(speed_record_change::table.filter(speed_record_change::id.eq(change_id)))
            .execute(conn)?;
        Self::delete_if_empty(conn, change.results_changeset_id)?;
        Ok(())
    }

    pub fn revert_change(conn: &mut PgConnection, change_id: i32) -> Result<(), Error> {
        let (change, (_changeset, map)) = results_change::table
            .inner_join(results_changeset::table.inner_join(map::table))
            .filter(results_change::id.eq(change_id))
            .first::<(ResultsChange, (ResultsChangeset, Map))>(conn)?;
        let current = TimeRecord::get_for_map(conn, &map)?;
        // no old time -> if new_time in current, then remove

        // new_time > old_time
        // new_time < old_time if new_time in current, then set old_time

        // no new time -> if no time in current, insert old_time
        match (change.new_time, change.old_time) {
            (Some(new_time), Some(old_time)) => {
                let mut old_time_in_db = false;
                for i in current.iter() {
                    if (i.player.id == change.player_id) && (i.time_record.time == old_time) {
                        old_time_in_db = true;
                    }
                }
                // find the change when old_time was set
                let prev = results_changeset::table
                    .inner_join(results_change::table)
                    .filter(results_changeset::map_id.eq(map.id))
                    .filter(results_change::player_id.eq(change.player_id))
                    .filter(results_change::new_time.eq(old_time))
                    .order_by(results_changeset::created_at.desc())
                    .first::<(ResultsChangeset, ResultsChange)>(conn)
                    .optional()?;
                let prev_date = match prev {
                    Some((cs, _c)) => cs.created_at,
                    None => utcnow(),
                };

                for i in current.iter() {
                    if (i.player.id == change.player_id) && (i.time_record.time == new_time) {
                        if old_time_in_db {
                            diesel::delete(rating_map::table)
                                .filter(rating_map::time_record_id.eq(i.time_record.id))
                                .execute(conn)?;
                            diesel::delete(time_record::table)
                                .filter(time_record::id.eq(i.time_record.id))
                                .execute(conn)?;
                        } else {
                            diesel::update(
                                time_record::table.filter(time_record::id.eq(i.time_record.id)),
                            )
                            .set((
                                time_record::time.eq(old_time),
                                time_record::created_at.eq(prev_date),
                            ))
                            .execute(conn)?;
                        }
                    }
                }
            }
            (None, Some(old_time)) => {
                let mut has_time = false;
                for i in current.iter() {
                    if (i.player.id == change.player_id) && (i.time_record.time == old_time) {
                        has_time = true;
                    }
                }
                if !has_time {
                    let prev = results_changeset::table
                        .inner_join(results_change::table)
                        .filter(results_changeset::map_id.eq(map.id))
                        .filter(results_change::player_id.eq(change.player_id))
                        .filter(results_change::new_time.eq(old_time))
                        .order_by(results_changeset::created_at.desc())
                        .first::<(ResultsChangeset, ResultsChange)>(conn)
                        .optional()?;
                    let prev_date = match prev {
                        Some((cs, _c)) => cs.created_at,
                        None => utcnow(),
                    };
                    diesel::insert_into(time_record::table)
                        .values((
                            time_record::map_id.eq(map.id),
                            time_record::pos.eq(555),
                            time_record::time.eq(old_time),
                            time_record::player_id.eq(change.player_id),
                            time_record::created_at.eq(prev_date),
                            time_record::reltime.eq(dec!(555)),
                        ))
                        .execute(conn)?;
                }
            }
            (Some(new_time), None) => {
                for i in current.iter() {
                    if (i.player.id == change.player_id) && (i.time_record.time == new_time) {
                        diesel::delete(rating_map::table)
                            .filter(rating_map::time_record_id.eq(i.time_record.id))
                            .execute(conn)?;
                        diesel::delete(time_record::table)
                            .filter(time_record::id.eq(i.time_record.id))
                            .execute(conn)?;
                    }
                }
            }
            (None, None) => {}
        }
        map.update_records_positions(conn)?;
        diesel::delete(results_change::table.filter(results_change::id.eq(change_id)))
            .execute(conn)?;
        Self::delete_if_empty(conn, change.results_changeset_id)?;
        Ok(())
    }

    pub fn get_news_feed(
        conn: &mut PgConnection,
        offset: i64,
        maybe_map_id: Option<i32>,
    ) -> Result<Vec<(Map, Server, Self, history::ResultChangeset)>, Error> {
        let changeset_rows = match maybe_map_id {
            Some(map_id) => results_changeset::table
                .inner_join(map::table)
                .inner_join(server::table)
                .filter(map::id.eq(map_id))
                .order_by(results_changeset::created_at.desc())
                .offset(offset)
                .limit(100)
                .load::<(ResultsChangeset, Map, Server)>(conn)?,
            None => results_changeset::table
                .inner_join(map::table)
                .inner_join(server::table)
                .order_by(results_changeset::created_at.desc())
                .offset(offset)
                .limit(100)
                .load::<(ResultsChangeset, Map, Server)>(conn)?,
        };
        let changeset_ids: Vec<_> = changeset_rows.iter().map(|x| x.0.id).collect();
        let result_rows = results_change::table
            .filter(results_change::results_changeset_id.eq_any(&changeset_ids))
            .load::<ResultsChange>(conn)?;
        let speed_rows = speed_record_change::table
            .filter(speed_record_change::results_changeset_id.eq_any(&changeset_ids))
            .load::<SpeedRecordChange>(conn)?;
        let speeds: HashMap<_, _> = speed_rows
            .into_iter()
            .map(|x| (x.results_changeset_id, x))
            .collect();
        let mut results = HashMap::new();
        for i in result_rows {
            results
                .entry(i.results_changeset_id)
                .or_insert_with(Vec::new)
                .push(i);
        }
        let mut changesets = Vec::new();
        for (c, m, s) in changeset_rows {
            let speed_record_change = speeds.get(&c.id).map(|x| history::SpeedRecordChange {
                new_player_id: x.new_player_id,
                new_speed: x.new_speed,
                old_player_id: x.old_player_id,
                old_speed: x.old_speed,
            });
            let mut changes: Vec<_> = results
                .remove(&c.id)
                .unwrap_or_default()
                .into_iter()
                .filter_map(|x| {
                    if let Some(c) =
                        history::ResultChange::from_db(x.new_pos, x.new_time, x.old_pos, x.old_time)
                    {
                        Some(history::PlayerResultChange {
                            player_id: x.player_id,
                            change: c,
                        })
                    } else {
                        None
                    }
                })
                .collect();
            changes.sort_by(|x, y| x.change.get_cmp_time().cmp(&y.change.get_cmp_time()));
            changesets.push((
                m,
                s,
                c,
                history::ResultChangeset {
                    speed_record_change,
                    changes,
                },
            ))
        }

        Ok(changesets)
    }

    pub fn save_changeset(
        conn: &mut PgConnection,
        server_id: i32,
        map: &Map,
        c: &history::ResultChangeset,
        best_time: Decimal,
    ) -> Result<(), Error> {
        let changeset: ResultsChangeset = diesel::insert_into(results_changeset::table)
            .values((
                results_changeset::map_id.eq(map.id),
                results_changeset::created_at.eq(utcnow()),
                results_changeset::max_pos.eq(map.max_pos),
                results_changeset::best_time.eq(best_time),
                results_changeset::server_id.eq(server_id),
            ))
            .get_result(conn)?;
        if let Some(sr_change) = &c.speed_record_change {
            if let (Some(op), Some(os)) = (sr_change.old_player_id, sr_change.old_speed) {
                diesel::insert_into(speed_record_change::table)
                    .values((
                        speed_record_change::results_changeset_id.eq(changeset.id),
                        speed_record_change::new_player_id.eq(sr_change.new_player_id),
                        speed_record_change::new_speed.eq(sr_change.new_speed),
                        speed_record_change::old_player_id.eq(op),
                        speed_record_change::old_speed.eq(os),
                    ))
                    .execute(conn)?;
            } else {
                diesel::insert_into(speed_record_change::table)
                    .values((
                        speed_record_change::results_changeset_id.eq(changeset.id),
                        speed_record_change::new_player_id.eq(sr_change.new_player_id),
                        speed_record_change::new_speed.eq(sr_change.new_speed),
                    ))
                    .execute(conn)?;
            }
        }
        for i in c.changes.iter() {
            match i.change {
                history::ResultChange::New(new_pos, new_time) => {
                    diesel::insert_into(results_change::table)
                        .values((
                            results_change::results_changeset_id.eq(changeset.id),
                            results_change::player_id.eq(i.player_id),
                            results_change::new_time.eq(new_time),
                            results_change::new_pos.eq(new_pos),
                        ))
                        .execute(conn)?;
                }
                history::ResultChange::Improvement(new_pos, new_time, old_pos, old_time) => {
                    diesel::insert_into(results_change::table)
                        .values((
                            results_change::results_changeset_id.eq(changeset.id),
                            results_change::player_id.eq(i.player_id),
                            results_change::new_time.eq(new_time),
                            results_change::new_pos.eq(new_pos),
                            results_change::old_time.eq(old_time),
                            results_change::old_pos.eq(old_pos),
                        ))
                        .execute(conn)?;
                }
                history::ResultChange::Degradation(new_pos, new_time, old_pos, old_time) => {
                    diesel::insert_into(results_change::table)
                        .values((
                            results_change::results_changeset_id.eq(changeset.id),
                            results_change::player_id.eq(i.player_id),
                            results_change::new_time.eq(new_time),
                            results_change::new_pos.eq(new_pos),
                            results_change::old_time.eq(old_time),
                            results_change::old_pos.eq(old_pos),
                        ))
                        .execute(conn)?;
                }
                history::ResultChange::Deleted(old_pos, old_time) => {
                    diesel::insert_into(results_change::table)
                        .values((
                            results_change::results_changeset_id.eq(changeset.id),
                            results_change::player_id.eq(i.player_id),
                            results_change::old_time.eq(old_time),
                            results_change::old_pos.eq(old_pos),
                        ))
                        .execute(conn)?;
                }
            }
        }
        Ok(())
    }
}
