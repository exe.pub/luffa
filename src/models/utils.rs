use num_traits::cast::ToPrimitive;

const MAX_RELTIME: f64 = 1.98;
const MAX_NEG_RELTIME: f64 = 0.71;

pub fn heatmap_data(time_records: &[super::PlayerTimeRecord]) -> Vec<(f64, f64, String)> {
    let mut res = Vec::new();
    for ptr in time_records {
        let normalized_pos =
            f64::from(ptr.time_record.pos - 1) / f64::from(ptr.map.max_pos - 1) * 100.;
        let mut reltime = ptr.time_record.reltime.to_f64().unwrap().clamp(MAX_NEG_RELTIME, MAX_RELTIME);
        reltime = f64::ln(reltime) * 100. / f64::ln(MAX_RELTIME);
        // reltime = (reltime - 1.) * 100. / MAX_RELTIME;
        res.push((reltime, normalized_pos, ptr.map.name.clone()));
    }
    res
}

pub fn make_player_time_records(
    data: Vec<(super::TimeRecord, super::Map, super::Player)>,
) -> Vec<super::PlayerTimeRecord> {
    let mut records = Vec::new();
    for (tr, m, p) in data {
        records.push(super::PlayerTimeRecord {
            map: m,
            time_record: tr,
            player: p,
        });
    }
    records
}
