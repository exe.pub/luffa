#!/usr/bin/python3

import psycopg2

source_conn = psycopg2.connect('dbname=xanmel')
source_cursor = source_conn.cursor()
target_conn = psycopg2.connect('dbname=luffa')
target_cursor = target_conn.cursor()

SERVERS = (4, 4321)

source_cursor.execute("select count(map_rating.id),sum(map_rating.vote),map.name FROM map_rating INNER JOIN map ON map.id=map_rating.map_id WHERE map.server_id IN (4, 4321) GROUP BY map.name ORDER BY map.name")

for row in source_cursor.fetchall():
    target_cursor.execute('UPDATE map SET rating_sum=%s rating_votes=%s WHERE name=%s', (
        row[1],
        row[0],
        row[2]
    ))
target_conn.commit()
