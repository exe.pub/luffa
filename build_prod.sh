#!/bin/bash

docker run --platform linux/amd64 \
  -v ~/tmp/luffa-cargo/git:/root/.cargo/git \
  -v ~/tmp/luffa-cargo/registry:/root/.cargo/registry \
  -v ~/src/luffa:/root/app/ \
  -v ~/src/luffa/target/production:/root/app/target/release \
  -v ~/src/maud:/root/maud/ \
  -v ~/src/morosoxon:/root/morosoxon luffa-builder
