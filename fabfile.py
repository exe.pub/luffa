import os
from fabric.api import *


env.hosts = ['xon.teichisma.info']
env.user = 'root'
local_user = 'luffa'
local_home = '/home/luffa'
app_path = '/home/luffa/luffa'
release_dir = './target/production'

bins = [i[:-3] for i in os.listdir('src/bin') if i.endswith('.rs')]

def compile_app():
    local('./build_prod.sh')
    # for i in bins:
    #     local('strip -s {}/{}'.format(release_dir, i))


def deploy():
    compile_app()
    run('mkdir -p {}/upload'.format(app_path))
    for i in bins:
        put('{}/{}'.format(release_dir, i), '{}/upload/'.format(app_path))
        run('chmod +x {}/upload/{}'.format(app_path, i))
    run('systemctl stop luffa')
    run('mv {}/upload/* {}'.format(app_path, app_path))
    with cd(app_path):
        sudo('./migrate', user=local_user)
    run('systemctl start luffa')
    local('tar czf /tmp/archive.tar.gz --exclude \'levelshots/*\' static/')
    put('/tmp/archive.tar.gz', '/tmp')
    with cd(app_path):
        sudo('tar xzf /tmp/archive.tar.gz', user=local_user)
