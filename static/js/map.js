$('#stats1').click(function (arg) {
    $('.stat2').hide();
    $('.stat3').hide();
    $('.stat1').show();
    $('.stat4').hide();
    $('.stats-link').removeClass('active');
    $(this).addClass('active');
});

$('#stats2').click(function () {
    $('.stat1').hide();
    $('.stat3').hide();
    $('.stat2').show();
    $('.stat4').hide();
    $('.stats-link').removeClass('active');
    $(this).addClass('active');
});

$('#stats3').click(function () {
    $('.stat1').hide();
    $('.stat2').hide();
    $('.stat3').show();
    $('.stat4').hide();
    $('.stats-link').removeClass('active');
    $(this).addClass('active');
});

$('#stats4').click(function () {
    $('.stat1').hide();
    $('.stat2').hide();
    $('.stat3').hide();
    $('.stat4').show();
    $('.stats-link').removeClass('active');
    $(this).addClass('active');
});
