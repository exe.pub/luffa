function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}

function isSubstrCaseI(query, target) {
    return target.toLowerCase().indexOf(query.toLowerCase()) > -1;
}

$(function() {
    var url;
    if (window.currentPlayerId) {
        url = '/players.json?exclude_ids=' + window.currentPlayerId;
    } else {
        url = '/players.json';
    }
    $.get(url, function(data) {
        $(".player-select-widget").each(function() {
            var that = $(this);
            var current_val = $(that.data('target-field')).val();
            if (current_val) {
                for (var i=0; i < data.length; i++) {
                    var item = data[i];
                    if (item.id == current_val) {
                        that.val(item.nickname_nocolors);
                        break;
                    }
                }
            }
            that.typeahead({
                source: data,
                autoSelect: true,
                matcher: function(item) {
                    var q = this.query.trim();
                    if ((item.id.toString() == q) || (isSubstrCaseI(q, item.nickname_nocolors))) {
                        return true;
                    }
                    var i;
                    for (i=0; i < item.nicks.length; i++) {
                        if (isSubstrCaseI(q, item.nicks[i])) {
                            return true;
                        }
                    }
                    for (i=0; i < item.keys.length; i++) {
                        if (q == item.keys[i]) {
                            return true;
                        }
                    }
                    return false;
                },
                displayText: function(item) {
                    return item.nickname;
                },
                afterSelect: function(item) {
                    $(that.data('target-field')).val(item.id);
                    that.val(item.nickname_nocolors);
                },
                autoSelect: true
            });
        });
    });
});
