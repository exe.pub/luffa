var zeroOffset = 0.33333333333333;

function map_url(map_name) {
    return "/map/" + encodeURIComponent(map_name);
}

function map_link(map_name) {
    return '<a href="' + map_url(map_name) + '">' + map_name + "</a>";
}

function drawHeatmap() {
    var heatmap_container = document.getElementById("heatmap");
    $(heatmap_container).empty();
    var radius = Math.round(Math.pow(heatmap_container.offsetWidth, 0.7) * 1.1 / Math.pow(window.heatmap_data.length, 0.3));
    var heatmap = h337.create({
        container: heatmap_container,
        radius: radius
    });
    var canvas = document.querySelector('#heatmap canvas');
    var maxX = canvas.width * (1 - zeroOffset);
    var offsetY = 0;
    var maxY = canvas.height - offsetY;
    var offsetX = canvas.width * zeroOffset;
    var processed_data = [];
    for (var i=0; i < window.heatmap_data.length; i++ ) {
        var elem = window.heatmap_data[i];
        processed_data.push({
            x: Math.round(elem.x * maxX / 100 + offsetX),
            y: Math.round(elem.y * maxY / 100 + offsetY),
            value: 1
        });
    }

    heatmap.setData({
        data: processed_data,
        max: 5
    });

    var ctx = canvas.getContext("2d");
    ctx.fillStyle = '#222222';
    ctx.fillRect(0, 0, canvas.width + 10, offsetY);

    ctx.moveTo(Math.round(offsetX) + 0.5, offsetY);
    ctx.lineTo(Math.round(offsetX) + 0.5, canvas.height);
    ctx.stroke();
    var reltime = $("#reltime-description");
    var x_left = $("#better-than-others");
    var x_right = $("#worse-than-the-best");
    reltime.css("left", Math.round(offsetX - 5) + "px");
    x_left.css("left", Math.round(offsetX - 200) + "px");
    x_right.css("left", Math.round(offsetX + 85) + "px");

    // ctx.font = '18px xolonium, sans-serif';
    // ctx.fillStyle = '#ccc';
    // ctx.fillText('← better than others', offsetX - 220, 15);
    // ctx.fillText('worse than the best →', offsetX + 30, 15);
    // ctx.fillStyle = '#333';
    // ctx.translate(20,250);
    // ctx.rotate(3 * Math.PI / 2);
    // ctx.translate(-20,-250);
    // ctx.fillText('← relative position', 20, 250);
    var hoverY, hoverX, maps;
    var hovermode = true;
    $(canvas).mousemove(function(event) {
        if (!hovermode) {
            return;
        }
        $(".heatmap-hint").html("Hint: Click on the heatmap to \"freeze\" the maplist at the current point");
        var offset = $(canvas).offset();
        var rawX = event.pageX - offset.left;
        var rawY = event.pageY - offset.top;
        hoverX = (rawX - offsetX) / maxX;
        hoverY = (rawY - offsetY) * 100 / maxY;
        maps = [];
        var radius = 2;
        for (var i=0; i < window.heatmap_data.length; i++) {
            var elem = window.heatmap_data[i];
            if ((Math.abs(elem.x - hoverX * 100) < radius) && (Math.abs(elem.y - hoverY) < radius)) {
                maps.push(map_link(elem.map));
            }
        }
        $('.heatmap-legend > pre').html("Relative position: " +
                                        Math.round(hoverY) +
                                        "%\nRelative Time: " +
                                        Math.pow(Math.E, hoverX * Math.log(2)) +
                                        "\nMaps: " + maps.join(', '));

        // console.log("Relative ranking:", Math.round(trueY));
        // console.log("Reltime:", Math.pow(Math.E, trueX));
    });

    $(canvas).on('click', function() {
        hovermode = !hovermode;
        if (!hovermode) {
            $(".heatmap-hint").html("Hint: Click on the heatmap to re-enable browsing");
        }
    });
}

function drawRatingHistoryChart() {
    var ctx = document.getElementById('rating-history-chart');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
		mode: 'index',
		intersect: false,
	    },
	    hover: {
		mode: 'nearest',
		intersect: true
	    },
            scales: {
		xAxes: [{
		    display: true,
		    scaleLabel: {
			display: true,
			labelString: 'Date'
		    }
		}],
		yAxes: [{
		    display: true,
		    scaleLabel: {
			display: true,
			labelString: 'Rating Points'
		    }
		}]
	    }
        }
    });
    $.get('/rating-history.json?player_ids=' + window.player_id, function (data) {
        myLineChart.data.labels = data.labels;
        var colors = ['#f00', '#0f0', '#00f', '#ff0', '#f0f', '#0ff'];
        var max_length = 0;
        var i;
        for (i=0; i < data.datasets.length; i++) {
            if (data.datasets[i].data_points.length > max_length) {
                max_length = data.datasets[i].data_points.length;
            }
        }
        for (i=0; i < data.datasets.length; i++) {
            var color = colors[i % colors.length];
            var source_dataset = data.datasets[i];
            var dataset = {
                label: source_dataset.label,
                backgroundColor: color,
                borderColor: color,
                data: [],
                fill: false
            };
            var j;
            for (j=0; j < source_dataset.data_points.length - max_length; j++) {
                dataset.data.push(0);
            }
            for (j=source_dataset.data_points.length - max_length; j < source_dataset.data_points.length; j++) {
                dataset.data.push(source_dataset.data_points[j]);
            }
            myLineChart.data.datasets.push(dataset);
            myLineChart.update();
        }
    });
}


$(document).ready(drawHeatmap);
$(document).ready(drawRatingHistoryChart);
$(window).resize(function() {
    setTimeout(drawHeatmap, 100);
});
