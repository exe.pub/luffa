import csv
import psycopg2
import decimal
import datetime

old_records = []
with open('old-records.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        old_records.append({
            'map_name': row[0],
            'time': decimal.Decimal(row[1]),
            'timestamp': datetime.datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S.%f'),
            'nickname': row[3],
            'crypto_idfp': row[4]
        })

conn = psycopg2.connect('dbname=luffa')
cursor = conn.cursor()

for i, record in enumerate(old_records):
    if i % 100 == 0:
        print(i, len(old_records))
    cursor.execute('SELECT time_record.id FROM time_record INNER JOIN player on player.id=time_record.player_id INNER JOIN player_key on player_key.player_id=player.id INNER JOIN map on map.id=time_record.map_id WHERE player_key.crypto_idfp=%s AND time_record.time=%s AND map.name=%s',
                   (record['crypto_idfp'],
                    record['time'],
                    record['map_name']))
    rows = cursor.fetchall()
    assert len(rows) <= 1
    for i in rows:
        cursor.execute('UPDATE time_record SET created_at=%s WHERE id=%s', (record['timestamp'], i[0]))
conn.commit()
