SELECT REC.nickname_nocolors, REC.time, SRV.name, REC.cp_times, REC.start_speed, REC.average_speed, REC.top_speed, REC.strafe_percentage
FROM (SELECT crypto_idfp, min(time) as t
      FROM record
      where map = 'xdwc2023-5'
      GROUP BY crypto_idfp) Q
         LEFT JOIN record REC ON Q.crypto_idfp = REC.crypto_idfp AND Q.t = REC.time AND REC.map='xdwc2023-5'
        INNER JOIN server SRV ON REC.server_id = SRV.id
-- WHERE REC.nickname_nocolors LIKE '[BOT]%'
ORDER BY REC.time;
