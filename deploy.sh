#!/bin/bash

REMHOST="root@xon.teichisma.info"
UNPRIV_USER="luffa"
REM_HOME="/home/luffa"
APP_PATH="$REM_HOME/luffa"


scp ./target/production/manage "$REMHOST:$APP_PATH/upload"
scp ./target/production/import_server_db "$REMHOST:$APP_PATH/upload"
ssh "$REMHOST" "systemctl stop luffa"
ssh "$REMHOST" "mv $APP_PATH/upload/* $APP_PATH"
ssh "$REMHOST" "cd $APP_PATH && ./manage migrate"
ssh "$REMHOST" "systemctl start luffa"
