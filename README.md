# Luffa - website for browsing Xonotic Defrag records

This is work in progress, but an instance is already working on https://xdf.teichisma.info/

## How it works

The `import_server_db` binary parses server database and stores the data in a PostgreSQL db. It also determines which records are new (to display them on the news page).

The `import_map_thumbs` binary extracts map screenshots (aka levelshots) from .pk3 files, resizes them and converts to jpeg.

The `server` binary runs the site itself.

The `youtube_scrape` binary is intended for searching videos of runs for each map on youtube. Not done yet.

## Goals

 * Convenient browsing of all records
 * Monitoring of recent activity
 * Rating system to determine who's the best defrag player (currently multiple rating system are being tested, see https://xdf.teichisma.info/rating)
 * Advanced query builder
 * support of multiple servers and possibly multiple physics types
 