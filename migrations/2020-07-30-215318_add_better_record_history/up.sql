CREATE TABLE results_changeset (
       id serial primary key,
       map_id integer not null references map(id),
       created_at timestamp not null,
       max_pos smallint not null,
       best_time numeric(20,6) not null
);

CREATE TABLE results_change (
       id serial primary key,
       results_changeset_id integer not null references results_changeset(id),
       player_id integer not null references player(id),
       new_time numeric(20,6) null,
       old_time numeric(20,6) null,
       new_pos smallint null,
       old_pos smallint null
);

CREATE TABLE speed_record_change (
       id serial primary key,
       results_changeset_id integer not null references results_changeset(id),
       new_player_id integer not null references player(id),
       old_player_id integer not null references player(id),
       new_speed numeric(20,6) not null,
       old_speed numeric(20,6) not null
);
