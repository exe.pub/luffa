ALTER TABLE ladder drop column map_algo_id;
ALTER TABLE ladder drop column sum_algo_id;
ALTER TABLE ladder_map drop column map_algo_id;

ALTER TABLE ladder ADD COLUMN map_algo character varying(32) NOT NULL;
ALTER TABLE ladder ADD COLUMN sum_algo character varying(32) NOT NULL;
ALTER TABLE ladder_map ADD COLUMN ladder_id integer NOT NULL;

DROP TABLE map_algo;
DROP TABLE sum_algo;
