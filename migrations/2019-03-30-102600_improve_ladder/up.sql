CREATE TABLE map_algo (
       id serial primary key,
       algo character varying(16) not null,
       params numeric(20,6)[] not null
);

CREATE TABLE sum_algo (
       id serial primary key,
       algo character varying(16) not null,
       params numeric(20,6)[] not null
);

ALTER TABLE ladder_map DROP COLUMN ladder_id;
ALTER TABLE ladder DROP COLUMN map_algo;
ALTER TABLE ladder DROP COLUMN sum_algo;

ALTER TABLE ladder ADD column map_algo_id integer not null references map_algo(id);
ALTER TABLE ladder ADD column sum_algo_id integer not null references sum_algo(id);

ALTER TABLE ladder_map ADD column map_algo_id integer not null references map_algo(id);
