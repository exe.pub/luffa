ALTER TABLE "map" ADD COLUMN sg bool not null default false;
ALTER TABLE "map" ADD COLUMN mg bool not null default false;

CREATE INDEX map_sg on "map" (sg);
CREATE INDEX map_mg on "map" (mg);
