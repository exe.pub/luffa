DELETE FROM ladder_map;

ALTER TABLE ladder_map DROP COLUMN time_record_id;
ALTER TABLE ladder_map ADD COLUMN time_record_id integer NOT NULL references time_record(id);
