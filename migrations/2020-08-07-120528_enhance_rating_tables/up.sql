ALTER TABLE ladder DROP CONSTRAINT ladder_pkey;
ALTER TABLE ladder ADD CONSTRAINT ladder_player_id_pkey PRIMARY KEY (player_id);
ALTER TABLE ladder DROP COLUMN id;

ALTER TABLE rating_history DROP CONSTRAINT rating_history_pkey;
ALTER TABLE rating_history ADD CONSTRAINT rating_history_pkey PRIMARY KEY (player_id, created_at);
ALTER TABLE rating_history DROP COLUMN id;

ALTER TABLE rating_history ADD COLUMN maps jsonb null;
