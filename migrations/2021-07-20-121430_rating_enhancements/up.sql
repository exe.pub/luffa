-- ALTER TABLE time_record DROP COLUMN score;

CREATE TABLE rating (
       id serial primary key,
       name character varying(64) not null,
       servers integer[] not null,
       max_pos integer not null default 0
);

CREATE TABLE rating_map (
       rating_id integer not null REFERENCES rating(id) ON DELETE RESTRICT,
       time_record_id integer not null REFERENCES time_record(id) on DELETE RESTRICT,
       score numeric(20,6) NOT NULL,
       last_updated timestamp not null,
       PRIMARY KEY (rating_id, time_record_id)
);

CREATE TABLE rating_total (
       rating_id integer not null REFERENCES rating(id) ON DELETE RESTRICT,
       player_id integer not null REFERENCES player(id) on delete restrict,
       score numeric(20,6) NOT NULL,
       map_count integer not null,
       pos integer not null,
       PRIMARY KEY (rating_id, player_id)
);
