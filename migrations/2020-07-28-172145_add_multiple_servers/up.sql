CREATE TABLE server (
       id serial primary key,
       name character varying(256) not null,
       ip_address inet not null,
       rcon_password character varying(256) not null,
       port integer not null,
       active boolean not null,
       server_db_path character varying(512) not null
);

ALTER TABLE map ADD COLUMN server_id integer null REFERENCES server(id) on DELETE RESTRICT;
