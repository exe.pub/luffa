ALTER TABLE time_record ADD column map_id integer null references map(id);
UPDATE time_record SET map_id=server_map.map_id from server_map where server_map.map_id=time_record.server_map_id;
ALTER TABLE time_record ALTER column map_id set not null;
ALTER TABLE time_record DROP COLUMN server_map_id;

ALTER TABLE speed_record ADD column map_id integer null references map(id);
UPDATE speed_record SET map_id=server_map.map_id from server_map where server_map.map_id=speed_record.server_map_id;
ALTER TABLE speed_record ALTER column map_id set not null;
ALTER TABLE speed_record DROP COLUMN server_map_id;

ALTER TABLE news_map_update ADD column map_id integer null references map(id);
UPDATE news_map_update SET map_id=server_map.map_id from server_map where server_map.map_id=news_map_update.server_map_id;
ALTER TABLE news_map_update ALTER column map_id set not null;
ALTER TABLE news_map_update DROP COLUMN server_map_id;

ALTER TABLE news_activity ADD column map_id integer null references map(id);
UPDATE news_activity SET map_id=server_map.map_id from server_map where server_map.map_id=news_activity.server_map_id;
ALTER TABLE news_activity ALTER column map_id set not null;
ALTER TABLE news_activity DROP COLUMN server_map_id;

DELETE FROM map_rating;
ALTER TABLE map_rating DROP column server_map_id;
ALTER TABLE map_rating ADD column map_id integer not null references map(id);

ALTER TABLE "map" ADD column active boolean NOT NULL default true;
UPDATE "map" SET active=server_map.active FROM server_map where "map".id=server_map.id;

DROP TABLE server_map;
DROP TABLE server;
