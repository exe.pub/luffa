ALTER TABLE ladder DROP COLUMN algo;
ALTER TABLE ladder ADD COLUMN map_algo character varying(32) NOT NULL;
ALTER TABLE ladder ADD COLUMN sum_algo character varying(32) NOT NULL;
