ALTER TABLE ladder DROP column rating_id;
ALTER TABLE ladder_map DROP COLUMN rating_id;
ALTER TABLE rating_history DROP COLUMN rating_id;

DROP TABLE rating;
DROP TABLE map_algo;
DROP TABLE sum_algo;
