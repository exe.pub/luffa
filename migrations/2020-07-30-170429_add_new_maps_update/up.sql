CREATE TABLE map_update (
       id serial primary key,
       map_id integer not null references map(id) on delete cascade,
       server_id integer not null references server(id) on delete cascade,
       deleted boolean not null,
       created_at timestamp not null
);

INSERT INTO server (name, ip_address, rcon_password, port, active, server_db_path) values ('Relaxed Running', '88.198.227.103', '', 26000, true, '');
UPDATE map set server_id=1 WHERE active = true;

DO $$
DECLARE
server_id integer;
old_rec record;
BEGIN
SELECT id INTO server_id FROM server WHERE id=1;
FOR old_rec in SELECT * FROM news_map_update INNER JOIN news on news.id = news_map_update.news_id
LOOP
        INSERT INTO map_update (map_id, server_id, deleted, created_at) VALUES (
               old_rec.map_id,
               server_id,
               old_rec.deleted,
               old_rec.created_at
        );
END LOOP;
END;
$$ LANGUAGE plpgsql;

DELETE from news_map_update;
DELETE from news where type='map';
DROP TABLE news_map_update;
