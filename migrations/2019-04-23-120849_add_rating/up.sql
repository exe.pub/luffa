CREATE TABLE rating (
       id serial primary key,
       map_algo_id integer references map_algo(id) not null,
       sum_algo_id integer references sum_algo(id) not null,
       is_default boolean not null,
       iterations integer not null
);

DELETE FROM ladder;
DELETE FROM ladder_map;

ALTER TABLE ladder_map drop column map_algo_id;
ALTER TABLE ladder drop column map_algo_id;
ALTER TABLE ladder drop column sum_algo_id;
ALTER TABLE map_algo drop column is_default;
ALTER TABLE sum_algo drop column is_default;

ALTER TABLE ladder_map add column rating_id integer references rating(id) not null;
ALTER TABLE ladder add column rating_id integer references rating(id) not null;
