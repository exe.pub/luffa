DELETE FROM ladder;
DELETE FROM ladder_map;

ALTER TABLE ladder_map drop column rating_id;
ALTER TABLE ladder drop column rating_id;
DROP TABLE rating;

ALTER TABLE ladder_map add column map_algo_id integer references map_algo(id) not null;
ALTER TABLE ladder add column map_algo_id integer references map_algo(id) not null;
ALTER TABLE ladder add column sum_algo_id integer references sum_algo(id) not null;

ALTER TABLE map_algo add column is_default boolean not null default false;
ALTER TABLE sum_algo add column is_default boolean not null default false;
