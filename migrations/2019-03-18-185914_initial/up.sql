CREATE TABLE server (
    id serial primary key,
    name character varying(256) not null,
    address character varying(512) not null
);


CREATE TABLE map (
    id serial primary key,
    name character varying(256) not null,
    thumbnail_url character varying(255)
);

CREATE UNIQUE INDEX map_name ON map USING btree (name);

CREATE TABLE server_map (
    id serial primary key,
    map_id integer not null REFERENCES map(id) ON DELETE RESTRICT,
    server_id integer not null REFERENCES server(id) ON DELETE RESTRICT,
    active boolean NOT NULL
);



CREATE TABLE player (
  id SERIAL PRIMARY KEY,
  stats_id integer,
  nickname character varying(256) NOT NULL,
  nickname_nocolors character varying(256) NOT NULL,
  created_at timestamp not null
);

CREATE INDEX player_stats_id ON player USING btree (stats_id);
CREATE INDEX player_nickname_ix ON player USING btree (nickname_nocolors);

CREATE TABLE player_key (
    id serial primary key,
    player_id integer not null references player(id) on delete cascade,
    crypto_idfp character varying(256) not null,
    created_at timestamp not null,
    last_used_at timestamp not null
);

CREATE UNIQUE INDEX player_key_crypto_idfp ON player_key USING btree (crypto_idfp);

CREATE TABLE player_nickname (
    id serial primary key,
    player_id integer not null references player(id) on delete cascade,
    nickname character varying(256) NOT NULL,
    nickname_nocolors character varying(256) NOT NULL,
    created_at timestamp not null,
    last_used_at timestamp not null
);

CREATE INDEX player_nickname_nickname ON player_nickname USING btree (nickname_nocolors);

CREATE TABLE map_rating (
    id serial primary key,
    server_map_id integer not null references server_map(id) on delete restrict,
    player_id integer not null references player(id) on delete restrict,
    vote integer not null,
    message character varying(512) not null,
    created_at timestamp not null
);

CREATE TABLE news (
       id serial primary key,
       created_at timestamp not null,
       "type" character varying(16) not null
);

CREATE INDEX news_type on news using btree ("type");

CREATE TABLE time_record (
    id serial primary key,
    server_map_id integer not null references server_map(id) on delete restrict,
    player_id integer not null references player(id) on delete restrict,
    "time" numeric(20,6) NOT NULL,
    created_at timestamp not null,
    UNIQUE(server_map_id, player_id)
);

CREATE TABLE time_record_position (
       time_record_id integer not null references time_record(id) on delete cascade primary key,
       pos smallint not null,
       max_pos smallint not null
);

CREATE TABLE speed_record (
    id serial primary key,
    server_map_id integer not null references server_map(id) on delete restrict,
    player_id integer not null references player(id) on delete restrict,
    "speed" numeric(20,6) NOT NULL,
    created_at timestamp not null,
    UNIQUE(server_map_id)
);

CREATE TABLE ladder (
       id serial primary key,
       algo character varying(16) not null,
       player_id integer not null references player(id) on delete restrict,
       score numeric(20,6) NOT NULL
);

CREATE INDEX ladder_algo on ladder using btree (algo);


CREATE TABLE ladder_map (
       id serial primary key,
       ladder_id integer not null references ladder(id) on delete restrict,
       server_map_id integer not null references server_map(id) on delete restrict,
       score numeric(20,6) NOT NULL
);

CREATE INDEX ladder_map_algo on ladder using btree (algo);

CREATE TABLE news_map_update (
       news_id integer not null references news(id) primary key,
       server_map_id integer not null references server_map(id),
       "deleted" boolean not null
);

CREATE TABLE news_activity (
       news_id integer not null references news(id) primary key,
       server_map_id integer not null references server_map(id) on delete restrict,
       player_id integer not null references player(id) on delete restrict,
       rec numeric(20, 6) not null,
       beaten_player_id integer references player(id) on delete restrict,
       beaten_rec numeric(20, 6)
);

CREATE INDEX news_activity_rec on news_activity using btree (rec);
CREATE INDEX news_activity_beaten_rec on news_activity using btree (beaten_rec);

CREATE TABLE news_time_record (
       news_id integer not null references news(id) primary key,
       pos smallint not null,
       max_pos smallint not null
);
