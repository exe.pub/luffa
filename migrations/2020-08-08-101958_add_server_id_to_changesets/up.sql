ALTER TABLE results_changeset ADD COLUMN server_id integer references server (id);

DO $$
DECLARE
rec record;
map_server_id integer;
BEGIN
FOR rec in SELECT * FROM results_changeset
LOOP
        SELECT server_id into map_server_id FROM map where map.id=rec.map_id;
        UPDATE results_changeset SET server_id=map_server_id WHERE id=rec.id;
END LOOP;
END;
$$ LANGUAGE plpgsql;

ALTER TABLE results_changeset ALTER COLUMN server_id SET NOT NULL;
