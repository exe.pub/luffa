DROP TABLE map_rating;

ALTER TABLE map ADD COLUMN rating_sum integer NOT NULL DEFAULT 0;
ALTER TABLE map ADD COLUMN rating_votes integer NOT NULL DEFAULT 0;
