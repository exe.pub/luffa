ALTER TABLE "map" ADD COLUMN haste bool not null default false;
ALTER TABLE "map" ADD COLUMN slick bool not null default false;
ALTER TABLE "map" ADD COLUMN vq3_compat bool not null default false;
ALTER TABLE "map" ADD COLUMN nexrun bool not null default false;
ALTER TABLE "map" ADD COLUMN rocket bool not null default false;
ALTER TABLE "map" ADD COLUMN mortar bool not null default false;
ALTER TABLE "map" ADD COLUMN crylink bool not null default false;
ALTER TABLE "map" ADD COLUMN hagar bool not null default false;
ALTER TABLE "map" ADD COLUMN hook bool not null default false;
ALTER TABLE "map" ADD COLUMN fireball bool not null default false;
ALTER TABLE "map" ADD COLUMN nex bool not null default false;
ALTER TABLE "map" ADD COLUMN electro bool not null default false;
ALTER TABLE "map" ADD COLUMN blaster bool not null default false;
ALTER TABLE "map" ADD COLUMN mine_layer bool not null default false;

CREATE INDEX map_haste on "map" (haste);
CREATE INDEX map_slick on "map" (slick);
CREATE INDEX map_vq3_compat on "map" (vq3_compat);
CREATE INDEX map_nexrun on "map" (nexrun);
CREATE INDEX map_rocket on "map" (rocket);
CREATE INDEX map_mortar on "map" (mortar);
CREATE INDEX map_crylink on "map" (crylink);
CREATE INDEX map_hagar on "map" (hagar);
CREATE INDEX map_hook on "map" (hook);
CREATE INDEX map_fireball on "map" (fireball);
CREATE INDEX map_nex on "map" (nex);
CREATE INDEX map_electro on "map" (electro);
CREATE INDEX map_blaster on "map" (blaster);
CREATE INDEX map_mine_layer on "map" (mine_layer);

ALTER TABLE server_map ADD COLUMN popularity_1d integer not null default 0;
ALTER TABLE server_map ADD COLUMN popularity_7d integer not null default 0;
ALTER TABLE server_map ADD COLUMN popularity_30d integer not null default 0;

CREATE INDEX server_map_popularity_1d on "server_map" (popularity_1d);
CREATE INDEX server_map_popularity_7d on "server_map" (popularity_7d);
CREATE INDEX server_map_popularity_30d on "server_map" (popularity_30d);

ALTER TABLE "map" DROP COLUMN thumbnail_url;

CREATE TABLE map_video (
       id serial primary key,
       map_id integer not null references map(id),
       url character varying(1024) not null,
       embed_code text
);

CREATE TABLE map_image (
       id serial primary key,
       map_id integer not null references map(id),
       -- type should be either of LEVELSHOT, WSQ3DF, CUSTOM
       -- TODO: use enum (when it's supported in diesel)
       type character varying(16) not null,
       url character varying(1024) not null
);
