ALTER TABLE "map" DROP COLUMN haste;
ALTER TABLE "map" DROP COLUMN slick;
ALTER TABLE "map" DROP COLUMN vq3_compat;
ALTER TABLE "map" DROP COLUMN nexrun;
ALTER TABLE "map" DROP COLUMN rocket;
ALTER TABLE "map" DROP COLUMN mortar;
ALTER TABLE "map" DROP COLUMN crylink;
ALTER TABLE "map" DROP COLUMN hagar;
ALTER TABLE "map" DROP COLUMN hook;
ALTER TABLE "map" DROP COLUMN fireball;
ALTER TABLE "map" DROP COLUMN nex;
ALTER TABLE "map" DROP COLUMN electro;
ALTER TABLE "map" DROP COLUMN blaster;
ALTER TABLE "map" DROP COLUMN mine_layer;

ALTER TABLE server_map DROP COLUMN popularity_1d;
ALTER TABLE server_map DROP COLUMN popularity_7d;
ALTER TABLE server_map DROP COLUMN popularity_30d;
ALTER TABLE "map" ADD COLUMN thumbnail_url character varying(255);

DROP TABLE map_video;

DROP TABLE map_image;
