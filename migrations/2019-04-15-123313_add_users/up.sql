CREATE TABLE "user" (
       id serial primary key,
       username character varying(128) not null,
       email character varying(512),
       player_id integer not null references player(id),
       is_admin boolean not null default false
);

CREATE INDEX user_username ON "user" USING btree (username);
CREATE INDEX user_email ON "user" USING btree (email);


CREATE TABLE "admin_log" (
       id serial primary key,
       created_at timestamp not null,
       op character varying(64) not null,
       user_id integer not null references "user"(id),
       description text not null
);

CREATE INDEX admin_log_op ON admin_log USING btree (op);
