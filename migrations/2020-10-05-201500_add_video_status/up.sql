DROP TABLE map_video;

CREATE TABLE map_video (
       id serial PRIMARY KEY NOT NULL,
       map_id integer not null references map(id),
       player_id integer not null references player(id),
       time numeric(10,5) not null,
       record_created_at timestamp not null,
       video_created_at timestamp not null,
       render_success boolean not null,
       youtube_code varchar(64) not null
);
