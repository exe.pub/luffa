CREATE TABLE rating_history (
       id serial primary key,
       rating_id integer not null references rating(id) ON DELETE CASCADE,
       player_id integer not null references player(id) ON DELETE CASCADE, 
       created_at timestamp not null,
       score numeric(20,6) not null,
       map_count integer not null,
       pos integer not null,
       max_pos integer not null
);

CREATE INDEX rating_history_created_at on rating_history (created_at);

CREATE TABLE heatmap_history (
       id serial primary key,
       player_id integer not null references player(id) ON DELETE CASCADE,
       created_at timestamp not null,
       data jsonb not null
);

CREATE INDEX heatmap_history_created_at on heatmap_history (created_at);
