CREATE TABLE demo_file (
       id serial PRIMARY KEY NOT NULL,
       map_id integer not null references map(id),
       player_id integer not null references player(id),
       time numeric(10,5) not null,
       demo_file_path varchar(512) not null
);
