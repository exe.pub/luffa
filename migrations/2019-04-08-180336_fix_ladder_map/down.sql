DELETE FROM ladder_map;
ALTER TABLE ladder_map ADD COLUMN player_id integer;
ALTER TABLE ladder_map ADD COLUMN server_map_id integer;

ALTER TABLE ladder_map DROP COLUMN time_record_id;
