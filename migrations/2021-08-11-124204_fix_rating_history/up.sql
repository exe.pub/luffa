ALTER TABLE rating_history DROP CONSTRAINT rating_history_pkey;
ALTER TABLE rating_history ADD PRIMARY KEY (rating_id, player_id, created_at);
