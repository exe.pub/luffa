ALTER TABLE time_record add column pos smallint null;
ALTER TABLE time_record add column reltime numeric(20,6) null;
ALTER TABLE time_record add column score numeric(20,6) null;
ALTER TABLE map add column max_pos smallint null;

DO $$
DECLARE
rec record;
old_time_rec record;
created timestamp;
new_snapshot_id integer;
old_max_pos integer;
old_speed_record_id integer;
old_pos integer;
old_reltime numeric(20,6);
old_score numeric(20,6);
BEGIN
RAISE notice 'starting procedure';
FOR rec in SELECT * FROM map
LOOP
        RAISE notice 'processing map %s', rec.name;
        SELECT max(created_at) INTO created from time_record where time_record.map_id = rec.id;
        IF created is null THEN
           SELECT max(created_at) INTO created from speed_record where speed_record.map_id = rec.id;
        END IF;
        IF created is not null THEN
           SELECT time_record_position.max_pos INTO old_max_pos FROM time_record_position INNER JOIN time_record on time_record.id=time_record_position.time_record_id WHERE time_record.map_id=rec.id;
           UPDATE map SET max_pos=old_max_pos WHERE map.id=rec.id;
           FOR old_time_rec in SELECT * FROM time_record WHERE map_id = rec.id
           LOOP
                SELECT pos INTO old_pos FROM time_record_position where time_record_position.time_record_id = old_time_rec.id;
                SELECT reltime INTO old_reltime FROM time_record_position where time_record_position.time_record_id = old_time_rec.id;
                SELECT score INTO old_score FROM ladder_map where ladder_map.time_record_id = old_time_rec.id;
                UPDATE time_record SET pos=old_pos where time_record.id=old_time_rec.id;
                UPDATE time_record SET reltime=old_reltime where time_record.id=old_time_rec.id;
                UPDATE time_record SET score=old_score where time_record.id=old_time_rec.id;
           END LOOP;
        END IF;
END LOOP;
END;
$$ LANGUAGE plpgsql;

DROP TABLE time_record_position;
DROP TABLE ladder_map;
ALTER TABLE time_record ALTER COLUMN pos SET NOT NULL;
ALTER TABLE time_record ALTER COLUMN reltime SET NOT NULL;
UPDATE time_record SET score=0 where score IS NULL;
ALTER TABLE time_record ALTER COLUMN score SET NOT NULL;
UPDATE map SET max_pos=0 where max_pos IS NULL;
ALTER TABLE map ALTER COLUMN max_pos SET NOT NULL;
