ALTER TABLE speed_record_change ALTER COLUMN old_player_id DROP NOT NULL;
ALTER TABLE speed_record_change ALTER COLUMN old_speed DROP NOT NULL;
