DELETE FROM ladder_map;
DELETE FROM ladder;

ALTER TABLE ladder DROP COLUMN map_count;
ALTER TABLE ladder DROP COLUMN pos;
ALTER TABLE ladder DROP COLUMN max_pos;
