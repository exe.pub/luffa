DELETE FROM ladder_map;
DELETE FROM ladder;

ALTER TABLE ladder ADD COLUMN map_count integer not null;
ALTER TABLE ladder ADD COLUMN pos integer not null;
ALTER TABLE ladder ADD COLUMN max_pos integer not null;
