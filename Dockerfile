FROM ubuntu:24.04

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    wget libsqlite3-dev \
    ca-certificates curl file \
    build-essential \
        liblzma-dev \
        liblzma5 \
    pkg-config autoconf automake autotools-dev libtool xutils-dev libgsl-dev libclang-dev clang libpq-dev && \
        rm -rf /var/lib/apt/lists/*

WORKDIR /root

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile minimal -y

ENV PATH=/root/.cargo/bin:$PATH

RUN mkdir /root/app
WORKDIR /root/app
VOLUME /root/.cargo

CMD cargo build --release
